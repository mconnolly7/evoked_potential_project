load('evoked_potential_project/data/processed_data/auto_extracted_processed/p1_all.mat')

rand_idx    = randperm(40);
burn_in     = 5;

for c1 = 1:40
    [~, max_idx] = max(p1_all(rand_idx(c1),:));
    max_list(c1) = max_idx;
    
    if c1 >= burn_in
        med_list(c1) = median(max_list(c1-burn_in+1:c1));
    end
end