% zach scratch bulk 03: find ep mean 
% ep mean


%% hygiene
close all
clear
clc


%% load ep_data, find ep_mean, append to ep_data

file_count = numel(dir('ep_data*'));
window_ms   = 10;               
window_s    = window_ms/1000;

ep_matrix = [];

for c1 = 1:file_count
    
    file_name = sprintf('ep_data_%02d',c1);
    load(file_name)
    tr_spt = stim_pulse_time;
    window_idx  = window_s * sampling_rate_ecog;
    
    for c2 = 1:size(tr_spt,2)
        pulse_start_idx     = floor((tr_spt(c2)-stim_start_time) * sampling_rate_ecog);
        pulse_end_idx       = pulse_start_idx + window_idx;
        if c2 < size(tr_spt,2)
            ep_matrix(:,:,c2)   = bpref_ecog(:,pulse_start_idx:pulse_end_idx);
        end
    end
    
    ep_matrix_norm  = ep_matrix - mean(ep_matrix,2);
    t               = (0:window_idx) / sampling_rate_ecog * 1000;
    
    ep_mean         = mean(ep_matrix_norm,3);
    
    save(file_name, 'ep_mean', '-append')
    
end


%%

