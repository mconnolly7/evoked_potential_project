close all
clear
clc


%% 
load('C:\Users\me\Dropbox\zach_share\f19_RT4D-5.501F0006_R_STN_evoked_part2_preprocessed.mat')
idx_shift = 0;

%%
load('C:\Users\me\Dropbox\zach_share\f18_RT4D-5.501F0005_R_STN_evoked_part1_preprocessed.mat')
idx_shift = 12;

%%
load('/Users/mconn24/Dropbox/share/zach_share/f19_RT4D-5.501F0006_R_STN_evoked_part2_preprocessed.mat')
idx_shift = 0;

%%
load('/Users/mconn24/Dropbox/share/zach_share/f18_RT4D-5.501F0005_R_STN_evoked_part1_preprocessed.mat')
idx_shift = 12;

%% inputs: raw ep data, pulse start times, stim parameters

% sampling_rate_ecog    - to be extracted
% ecog                  - raw unfiltered 28 channel ecog to be extracted
% stim_start_times      - start time of a single stimulation epoch to be extracted
% stim_stop_times;
% stim_pulse_times      - time of a single stimulation within an epoch to be extracted
% stparam = stimulation_params_legend;             % to be extracted and compared 
t       = (1:1:length(ecog))/sampling_rate_ecog;     % to be extracted




% segment raw ecog by pulse start times into stim epochs, 36 + 4 

for c1 = 1:length(stim_start_times)                                  % this for loop separates the ecog of each stim epoch
    stim_start_idx      = floor(stim_start_times(c1) * sampling_rate_ecog);
    stim_end_idx        = floor(stim_stop_times(c1) * sampling_rate_ecog);

    epoch_ecog          = ecog(:,stim_start_idx:stim_end_idx);    % epoch_ecog should be 28xN matrix, where N is length of stim epoch 
    save_path           = sprintf('ep_data_%02d',c1 + idx_shift);
    stim_start_time     = stim_start_times(c1);
    stim_stop_time      = stim_stop_times(c1);
    stim_pulse_time     = stim_pulse_times(c1);
    stim_params         = stimulation_params_legend(c1);
    save(save_path, 'epoch_ecog', 'stim_start_times', 'stim_stop_times', 'stim_pulse_times', 'stim_params')  % saves data to new file ep_results0'x'
end

% additional processing

for c1 = 1:length(stim_start_times)
    
    save_path   = sprintf('ep_data_%02d',c1 + idx_shift);
    load(save_path)           % loads each ep_results0'x'
    
    % Bipolar rereferencing
    for c2 = 1:size(epoch_ecog,1)-2
        if c2 < 14
            bpref_ecog(c2,:) = epoch_ecog(c2+1,:) - epoch_ecog(c2,:);
        else
            bpref_ecog(c2,:) = epoch_ecog(c2+2,:) - epoch_ecog(c2+1,:);
        end
    end
    
    % now bpref_ecog should be 26xN matrix, with each channel referenced to
    % an adjacent one
   
    % for each epoch average eps after stim
    
    window_ms   = 10;               % not sure what these are to be yet
    window_s    = window_ms/1000;
    window_idx  = window_s * sampling_rate_ecog;
    
    ep_matrix   = [];               % below is essentially your code
    
    tr_spt = stim_pulse_times{c1};
    offset = stim_start_times(c1);
    
    for c2 = 1:size(tr_spt,2)
        pulse_start_idx     = floor((tr_spt(c2)-stim_start_times(c1)) * sampling_rate_ecog);
        pulse_end_idx       = pulse_start_idx + window_idx;
        if c2 < size(tr_spt,2)
            ep_matrix(:,:,c2)   = bpref_ecog(:,pulse_start_idx:pulse_end_idx);
        end
    end
    
    % here ep_matrix should be 26xMxP where M is (window_s * fs) and P is  
    % size/length of spt, or how many pulses were delivered in this stim epoch 
    
    ep_matrix_norm      = ep_matrix - mean(ep_matrix,2);    % keeping per your code
    t                   = (0:window_idx) / sampling_rate_ecog * 1000;
    
    ep_mean             = mean(ep_matrix_norm,3);           % average along dimension 3 to get 26xM post-stim EP waveform
    

end




