data_dir            = 'evoked_potential_project/data/raw_data/subject_EP002/';

for file_idx = 1:40
    data_path           = sprintf('%sep_results%d.mat', data_dir, file_idx)

    load(data_path, 'mean_diff_stim_epoch_ecog')
    sampling_rate_ms    = 22; % Samples per ms
    artifact_end_ms     = 11.5;

    data_window_ms      = [artifact_end_ms 20];
    data_window_idx     = data_window_ms * sampling_rate_ms;
    data_range          = data_window_idx(1):data_window_idx(2);

    p1_window_ms        = [.5 2.5];
    p1_window_idx       = p1_window_ms*sampling_rate_ms;
    p1_range            = p1_window_idx(1):p1_window_idx(2);


    data_ecog           = squeeze(mean_diff_stim_epoch_ecog(:,data_range,:));
    t                   = (1:size(data_ecog,1))/sampling_rate_ms  ; 


    %%
    % close all
    % hold on

    pcg         = 1:26;
%     pcg         = [5:8 18:22];
    % pcg         = 18;
    peak_vec    = [];
    y_all       = [];
    cc          = [];

    for c1 = 1:size(pcg,2)
        gpep            = gp_object();
        gpep.initialize_data(t', data_ecog(:,pcg(c1)));
        gpep.minimize(5);
        y               = gpep.predict(t');
    %     y_all(c1,:)     = y;
        hold on
    %     plot(y(p1_range) - min(y(p1_range)));
        ep_data     = y(p1_range);
        fp          = findpeaks(ep_data);
        p1_peaks    = fp.loc;

        % Find location of p1
        p1 = nan;
        if p1_peaks(1) == 1 
            if size(p1_peaks,1) > 1          
                p1 = p1_peaks(2);
            else
                p1 = 1;
            end
        else
            p1 = p1_peaks(1);
        end
        p1_amp = ep_data(p1);

        % If there is a p1, then find v1
        fp          = findpeaks(-1 * ep_data);
        v1_peaks    = fp.loc;
        v1          = v1_peaks(find(v1_peaks > p1, 1));

        if ~isempty(v1)
            v1_amp =ep_data(v1);
        end

        p1_magnitude(c1) = p1_amp - v1_amp;
    end
%     scatter(pcg,p1_magnitude)
    p1_all(file_idx,:) = p1_magnitude;
end
%%
clear cc
for c1 = 2:size(pcg,2)/2
    cc(c1-1,1) = corr(y_all(c1-1,:)', y_all(c1,:)');
    cc(c1-1,2) = corr(y_all(c1-1+13,:)', y_all(c1+13,:)');
    map(c1-1,:) = [c1-1, c1];
end
%%
%     peak_list(c1)   = findpeaks(y);
%     peak_vec        = [peak_vec; peak_list(c1).loc];
%     y_all(c1,:)     = y;
%     plot(t, y-min(y))
%     hold on

%%
% m       = repmat(mean(data_ecog, 1), size(data_ecog,1),1);
% s       = repmat(std(data_ecog, [], 1), size(data_ecog, 1),1);
% data_ecog_z = (data_ecog - m) ./ s;
% data_ecog_z(:,18) = data_ecog_z(:,18) - min(data_ecog_z(:,18));
% for c1 = 1:size(data_ecog_z,2)-1
%     subplot(5,5,c1)
%     plot(t,data_ecog_z(:,c1))
%     ylim([-2 2])
% end