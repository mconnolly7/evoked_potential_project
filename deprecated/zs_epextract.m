% function [out1, out2, out3, ...] = zs_epextract(in1, in2, in3, ...)

function [] = zs_epextract('input of raw data structure "data_x"') % not sure if an output is required if the function takes input "data_x" and just saves a new procesesd data"

load('... ... \data_x\') % this should be the equivalent of one of the f18/f19 data structures


% inputs: raw ep data, pulse start times, stim parameters

fs      = [];                                   % to be extracted
t       = [(1:1:length('data_x.ecog'))/fs];     % to be extracted
ecog    = 'data_x.ecog';                        % raw unfiltered 28 channel ecog to be extracted
pst     = 'data_x.pulse_start_times';           % start time of a single stimulation epoch to be extracted
spt     = 'data_x.stim_pulse_times';            % time of a single stimulation within an epoch to be extracted
stp     = 'data_x.stim_parameters';             % to be extracted and compared 


% segment raw ecog by pulse start times into stim epochs, 36 + 4 

for c1 = 1:length(pst)                                  % this for loop separates the ecog of each stim epoch
    stim_start_idx      = floor(pst(c1) * fs);
    if c1 < length(pst)
        stim_end_idx    = floor(pst(c1+1) * fs);
    else
        stim_end_idx    = floor(pst(end) * fs); 
    end
    epoch_ecog = ecog(stim_start_idx:stim_end_idx);    % epoch_ecog should be 28xN matrix, where N is length of stim epoch 
    save(['ep_results0' num2str(c1)], 'epoch_ecog', 'pst', 'stp')  % saves data to new file ep_results0'x'
end


% additional processing

for c1 = 1:length(pst)
    
    load(['ep_results0' num2str(c1)])           % loads each ep_results0'x'
    
    % bpref = bipolar referencing of ecog leads    
    
    for c2 - 1:size(epoch_ecog,1)-2
        if c2 < 14
            bpref_ecog(c2,:) = epoch_ecog(c2+1,:) - epoch_ecog(c2,:);
        else
            bpref_ecog(c2,:) = epoch_ecog(c2+2,:) - epoch_ecog(c2+1,:);
        end
    end
    
    % now bpref_ecog should be 26xN matrix, with each channel referenced to
    % an adjacent one
    
    % for each epoch average eps after stim
    
    window_ms   = [];               % not sure what these are to be yet
    window_s    = [];
    window_idx  = window_s * fs;
    
    ep_matrix   = [];               % below is essentially your code
    
    for c2 = 1:size(spt,2)
        pulse_start_idx     = floor(spt * fs);
        pulse_end_idx       = pulse_start_idx + window_idx;
        ep_matrix(:,:,c2)   = bpref_ecog(:,pulse_start_idx:pulse_end_idx);
    end
    
    % here ep_matrix should be 26xMxP where M is (window_s * fs) and P is size/length of spt, or how many pulses were delivered in this stim epoch 
    
    ep_matrix_norm      = ep_matrix - mean(ep_matrix,2);    % keeping per your code
    t                   = (0:window_idx) / fs * 1000;
    
    ep_mean             = mean(ep_matrix_norm,3);           % average along dimension 3 to get 26xM post-stim EP waveform
    
    % btw I figured out why my plots look silly; though we're not plotting
    % here in this code, after much ado I saw that we had to plot the
    % transponse, and what I was plotting was nonsense, which was ch1 ch2 ch3 ...
    % rather than the time series of each channel.... so silly me defeated
    % by a single apostrophe
    
        
    % find peak and idx of ep
    
    ep_window_start = [];       % I need to figure out how to index and size this
    ep_window_size  = [];
    ep_window_end   = ep_window_start + ep_window_size;
    
    [M,I] = max(ep_mean(ep_window_start:ep_window_end),[],2)    % not sure if indexing here will be correct, since we're taking a window of the ep_mean
    
    % appends all to ep_results
    
    save(['ep_results0' num2str(c1)], 'M', 'I', '-append')
    
end





