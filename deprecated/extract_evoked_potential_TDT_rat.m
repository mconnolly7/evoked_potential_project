function extract_evoked_potential_TDT(file_name)
% close all
figure
fs                          = 24414;

post_window_length_ms       = 20;
pre_window_length_ms        = 5;

post_window_length_idx      = floor(post_window_length_ms * fs / 1e3);
pre_window_length_idx       = floor(pre_window_length_ms * fs / 1e3);
recording_channels          = [8 6 4 2 9 11 13 15];

load(file_name)

stim_sig                    = sum(signal_data);
stim_flip                   = -1*flip(stim_sig);
pulse_start                 = zeros(size(stim_sig));
pulse_end                   = pulse_start;
% data                        = bandpass(data', [20 500], 24414)';
% data                        = highpass(data', 70 , 24414, 'stopbandattenuation', 300)';

% Get all pulse termination times
search_idx = 1;
while search_idx < size(stim_sig,2)
    next_pulse_end              = find(stim_flip(search_idx:end) > 0, 1) + search_idx - 1;    
    pulse_end(next_pulse_end)   = 1;    
    search_idx                  = next_pulse_end + 15;
end

pulse_end       = flip(pulse_end);
pulse_end_idx   = find(pulse_end > 0);

% Collect all the post-stimulation data
for c1 = 1:size(pulse_end_idx,2)
    window_start    = pulse_end_idx(c1) - pre_window_length_idx;
    window_end      = pulse_end_idx(c1) + post_window_length_idx;
    
    window_start    = window_start;
    window_end      = window_end;
    
    ep_mat(c1,:,:)  = lfp_data(:,window_start:window_end);
end

t   = -pre_window_length_ms:1e3/fs:post_window_length_ms;
tt  = [t flip(t)];

% Plot each channel
for c1 = 1:size(ep_mat,2)
    channel_ep  = squeeze(ep_mat(:,c1,:));
    ep_z        = channel_ep - mean(channel_ep, 2);
    ep_mean     = mean(ep_z);
    ep_std      = std(channel_ep);
    ep_se       = ep_std / sqrt(size(channel_ep,1));
    
    yy          = [ep_mean - ep_se ,flip(ep_mean + ep_se)];
    
    subplot(3,4,c1)
    hold on
    patch(tt, yy, .5*ones(1,3), 'FaceAlpha', .5, 'EdgeColor', 'k')
    plot(t, ep_mean, 'LineWidth', 2, 'Color', 'r')
    
    title(sprintf('Channel: %d', recording_channels(c1)))
    xlabel('milliseconds')
    ylabel('Potential (V)')
    set(gca,'FontSize', 12)
    xlim([min(t) max(t)]);
end

subplot(3,4,[9 12])
yyaxis right
plot(stim_data')

yyaxis left
plot(lfp_data(5,:))
set(gca,'FontSize', 12)

end