% zach scratch bulk 02: bipolar rereferencing 
% rereferences bipolarly


%% hygiene
close all
clear
clc


%% load ep_data, bipolar reference, append to ep_data

file_count = numel(dir('ep_data*'));
bpref_ecog = zeros(size(epoch_ecog,1)-2,size(epoch_ecog,2);

for c1 = 1:file_count
    
    file_name = sprintf('ep_data_%02d',c1);
    load(file_name)
    
    for c2 = 1:size(epoch_ecog,1)-2
        if c2 <14
            bpref_ecog(c2,:) = epoch_ecog(c2+1,:) - epoch_ecog(c2,:);
        else
            bpref_ecog(c2,:) = epoch_ecog(c2+2,:) - epoch_ecog(c2+1,:);
        end
    end
    
    save(file_name, 'bpref_ecog', '-append')
    
end


%%

