close all
clear ep_matrix
hold on

load('evoked_potential_project/data/test_data/ecog_data.mat')
load('/Users/mconn24/Box Sync/EP_data_for_Mark/ephys002/bipolar_next_NOmega_trigger_reorganized/ep_results1.mat');
channel_1 = 4;
channel_2 = channel_1 + 1;

d1 = data.ecog_1(channel_1,:);
d2 = data.ecog_1(channel_2,:);

d1_norm = (d1 - mean(d1)) / std(d1);
d2_norm = (d2 - mean(d2)) / std(d2);

% plot(d1)
% plot(d1 - d2)

fs          = 22000;
window_ms   = 10;
window_s    = window_ms/100;
window_idx  = window_s * fs;


% d = d1_norm - d2_norm;
d = d1 - d2;


for c1 = 1:size(data.stim_pulse_times_1,2)
    pulse_start_idx = floor(data.stim_pulse_times_1(c1) * fs);
    pulse_end_idx   = pulse_start_idx + window_idx;
    
    ep_matrix(c1,:) = d(pulse_start_idx:pulse_end_idx);
end


ep_matrix_norm      = ep_matrix - mean(ep_matrix,2);
t                   = (0:window_idx)/fs * 1000;

ep_mean             = mean(ep_matrix_norm,1);

[~, p_start_idx]    = max(abs(ep_mean));
p_start             = p_start_idx/fs*1000;

m                   = squeeze(mean_diff_stim_epoch_ecog(1,:,1));
tm                  = (1:size(m,2))/fs*1000;

[~, p_start_tm_idx] = max(abs(m));
p_start_tm          =   p_start_tm_idx/fs*1000;


subplot(2,2,1)

hold on
plot(t,ep_matrix_norm', 'color', .9 * ones(1,3));
plot(t-p_start,ep_mean, 'color', 'k', 'LineWidth', 2);
ylim([-100 100])
set(gca, 'FontSize', 16)


subplot(2,2,2)
plot(tm, m)
set(gca, 'FontSize', 16)

subplot(2,2,3)
hold on
plot(t-p_start,ep_matrix_norm', 'color', .9 * ones(1,3));
plot(t-p_start,ep_mean, 'color', 'k', 'LineWidth', 2);
xlim([-1 15])
ylim([-100 100])
xlabel('Milliseconds')
set(gca, 'FontSize', 16)

subplot(2,2,4)

plot(tm-p_start_tm, m)
xlim([-1 15])
set(gca, 'FontSize', 16)




