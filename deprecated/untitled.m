

%%
clc
close all
load('data/human/EPdata_for_Mark.mat');

subject     = 6;
Y_all       = EP(subject).EP1ramp;
data_rate   = mean(~isnan(Y_all));
Y           = EP(subject).EP1ramp(:,24);
cathode     = EP(subject).StimCathode;
amplitude   = EP(subject).StimAmplitude;
frequency   = EP(subject).StimFrequency;
pulse_width = EP(subject).StimPulsewidth;
X = [cathode; amplitude]';

nan_idx     = ~isnan(Y);
X           = X(nan_idx,:);
Y           = Y(nan_idx,:);

outlier_idx = detect_outlier(Y, 5);
X           = X(~outlier_idx,:);
Y           = Y(~outlier_idx,:);

gp_model    = gp_object();
gp_model.initialize_data(X, Y, min(X), max(X));
            
% gp_model.covariance_function    = {@covSEiso};
% gp_model.hyperparameters.cov    = [1; 1];
gp_model.mean_function          = {@meanPoly,2};
gp_model.hyperparameters.mean   = [mean(Y); mean(Y); mean(Y); mean(Y)]; 
gp_model.minimize(100)

gp_model.plot_mean
% gp_model.plot_standard_deviation

%%
lower           = gp_model.lower_bound;
upper           = gp_model.upper_bound;

N               = 5;
t_amplitude     = uniform_range(gp_model.lower_bound(2), gp_model.upper_bound(2),N);
t_cathode       = randi(5,N, 1);
t               = [t_cathode t_amplitude];
ep_amplitude    = gp_model.sample(t);

for c2 = 5:100
    ep_power        = t(:,2);
    ep_efficiency   = 1 - ep_power/max(ep_power);
    
    % Create a GP for each objective function
    gp_f1           = gp_object();
    gp_f2           = gp_object();
    
    gp_f1.initialize_data(t, ep_efficiency, lower, upper)
    gp_f2.initialize_data(t, ep_amplitude, lower, upper)
    
    gp_f1.minimize(10)
    gp_f2.minimize(10);
    
    % subplot(1,2,1)
    % gp_f1.plot_mean
    % subplot(1,2,2)
    % gp_f2.plot_mean
    
    % close all
    clear is_efficient
    costs = [ep_efficiency ep_amplitude];
    is_efficient = ones(size(ep_power));
    
    rand_idx = randperm(size(is_efficient,1));
    
    for c1 = 1:size(is_efficient,1)
        idx = rand_idx(c1);
        c = costs(idx,:);
        
        a = c >= costs; % Compare to all other costs
        b = any(a,2);   % Check if c is greater than each cost for any one obj
        d = all(b);     % non-Dominated if c is greater than all others on any one obj
        
        is_efficient(idx) = all(any(c >= costs,2));
    end
    
    % hold on
    % scatter(ep_efficiency(is_efficient==0), ep_amplitude(is_efficient==0))
    % scatter(ep_efficiency(is_efficient==1), ep_amplitude(is_efficient==1))
    % xlabel('Efficiency (a.u.)')
    % ylabel('EP Magnitude (mV)')
    % set(gca, 'FontSize', 18)
    
    t_cand = gp_f1.t;
    for c1 = 1:10:size(t_cand,1)
        
        f1 = gp_f1.predict(t_cand(c1,:));
        f2 = gp_f2.predict(t_cand(c1,:));
        evhi(c1) = exi2d(costs(is_efficient ==1,:),[f1,f2],[0,0],[0.1,0.1]);
        
    end
    
    %
    [max_ehvi max_ehvi_idx] = max(evhi);
    new_t = t_cand(max_ehvi_idx,:);
        
    t               = [t; t_new];
    new_Y           = gp_model.sample(t_new);

end

