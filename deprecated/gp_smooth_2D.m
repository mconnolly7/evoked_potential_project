close all

for c1 = 9:36
    ep_path         = sprintf('ep_data_%02d',c1);
    load(ep_path)
    
    ep_envelope     = mean(abs(ep_mean),1);
    [~, pulse_idx]  = max(ep_envelope(1:110));
    
    tt              = (1:size(ep_mean,2))/22000 * 1000;
    tt              = tt - tt(pulse_idx);
    idx             = pulse_idx + 25:221;
    EP_strip_1      = ep_mean(1:13,idx);
    
    EP_strip_1      = EP_strip_1 - mean(EP_strip_1,2);
    
    channels        = (1:13)';
    channel_mat     = repmat(channels,1, size(EP_strip_1,2));
    
    t               = tt(idx);
    t_mat           = repmat(t,size(EP_strip_1,1),1);
    
    ep_reshape      = reshape(EP_strip_1',[],1);
    channel_reshape = reshape(channel_mat', [], 1);
    t_reshape       = reshape(t_mat',[],1);
    
    X               = [channel_reshape, t_reshape];
    Y               = ep_reshape;
    
    ep_model                        = gp_object;
    ep_model.covariance_function    = {@covSEard};
    ep_model.mean_function          = {@meanZero};
    ep_model.initialize_data(X, Y);
    
    ep_model.minimize(1)
    
    stim_params_all(c1)             = stim_params;
    [~, y_max(c1), ~, y_min(c1)]    = ep_model.discrete_extrema(2);
    ep_model_all(c1) = ep_model;
    
%     subplot(6, 6, c1)
    ep_model.plot_mean

    title(stim_params);
    xlabel('Channels')
    ylabel('milliseconds');
    set(gca,'fontSize', 10);
    view(90,90)
    zlim([-10 10])
    caxis([-10 10]);
    drawnow

end





%%
for c1 = 1:36
    subplot(6, 6, c1)
    ep_model_all(c1).plot_mean
    title(stim_params_all(c1));
    xlabel('Channels')
    ylabel('milliseconds');
    set(gca,'fontSize', 10);
    view(90,90)
    
    y_limits = [min(y_min) max(y_max)];
    zlim(y_limits)
    caxis(y_limits);
    xlim([1 13])
    ylim([min(t) max(t)])
    drawnow
    colormap('jet')
end


%%
clear frequency amplitude contact pulse_width
for c1 = 1:36
    param_tokens        = strsplit(stim_params_all{c1}, '/');
    
    contact(c1,:)       = param_tokens{1};
    amplitude(c1,:)     = str2num(param_tokens{2}(1));
    pulse_width(c1,:)   = str2num(param_tokens{3}(1:2));    
    frequency(c1,:)     = str2num(param_tokens{4}(1:2));

end

contact_names = unique(contact, 'rows');
for c1 = 1:size(contact_names,1)
    
    for c2 = 1:size(contact,1)
        if strcmp(contact(c2,:), contact_names(c1,:))
           contact_num(c2,:) = c1; 
        end
    end
end

%%
X   = [contact_num amplitude pulse_width frequency];
idx = pulse_width == 60 & frequency == 10;
X   = X(idx,:);
Y   = y_max(idx)';
ep_models_t = ep_model_all(idx);

for c1 = 1:max(contact_num)
    contact_idx     = X(:,1) == c1;
    y_mean(c1)      = mean(Y(contact_idx));
    
end

[y_sort, y_sort_idx] = sort(y_mean);

plot(sort(y_mean))
xticklabels(contact_names(y_sort_idx,:))

%%
for c1 = 1:size(y_sort_idx,2)
    contact_idx  = X(:,1) == y_sort_idx(c1);
   
    subplot(3,10,c1)
    plot_idx     = contact_idx & X(:,2) == 1;
   
    ep_models_t(plot_idx).plot_mean
    title(contact_names(y_sort_idx(c1),:));
    xlabel('Channels')
    ylabel('milliseconds');
    set(gca,'fontSize', 10);
    view(90,90)

    y_limits = [min(y_min) max(y_max)];
    zlim(y_limits)
    caxis(y_limits);
    xlim([1 13])
    ylim([min(t) max(t)])
    drawnow
    colormap('jet')
    
    %%%%%%
    subplot(3,10,c1+10)
    plot_idx     = contact_idx & X(:,2) == 3;
   
    ep_models_t(plot_idx).plot_mean
    title(contact_names(y_sort_idx(c1),:));
    xlabel('Channels')
    ylabel('milliseconds');
    set(gca,'fontSize', 10);
    view(90,90)

    y_limits = [min(y_min) max(y_max)];
    zlim(y_limits)
    caxis(y_limits);
    xlim([1 13])
    ylim([min(t) max(t)])
    drawnow
    colormap('jet')
    
    %%%%%%
    subplot(3,10,c1+20)
    plot_idx     = contact_idx & X(:,2) == 5;
   
    ep_models_t(plot_idx).plot_mean
    title(contact_names(y_sort_idx(c1),:));
    xlabel('Channels')
    ylabel('milliseconds');
    set(gca,'fontSize', 10);
    view(90,90)

    y_limits = [min(y_min) max(y_max)];
    zlim(y_limits)
    caxis(y_limits);
    xlim([1 13])
    ylim([min(t) max(t)])
    drawnow
    colormap('jet')
    
   
end
