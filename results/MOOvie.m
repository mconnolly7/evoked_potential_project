function MOOvie 
close all

load('simulation_data/evoked_potential/pareto_history_ep006_4.mat')


v = VideoWriter('MOO.avi');
v.FrameRate = 10;
open(v);

STOP_SAMPLE = 100;

lower = [0 .73];
upper = [3 4];
figure('Position', [0 0 1400 700])
axis tight manual 
set(gca,'nextplot','replacechildren'); 

clear ep_amplitude
for c1 = 11:STOP_SAMPLE-1
%     subplot(1,3,3)

    scatter(gt_cost(gtp == 0,1),gt_cost(gtp == 0,2),100, 'MarkerEdgeColor', .5*ones(1,3))
    hold on
    scatter(gt_cost(gtp == 1,1),gt_cost(gtp == 1,2),100, 'MarkerEdgeColor', [.5 0 0])

    start_point = max(c1-10, 11);
    
    for c2 = start_point:c1-1
        plot(pareto_history{c2}(:,1), pareto_history{c2}(:,2), ...
                'color', [.7 .7 .7], 'LineWidth', 3);
    end
    
    plot(pareto_history{c1}(:,1), pareto_history{c1}(:,2), ...
                'color', [.0 .0 .0], 'LineWidth', 3);
    
    title(sprintf('Sample: %d', c1));
    xlabel('Efficiency (a.u.)')
    ylabel('EP Magnitude (�V)')
    set(gca, 'FontSize', 18)
    hold off
    
    state           = X(1:c1,:);
    ep_amplitude    = objective_2.y_data(1:c1);
    ep_power        = state(:,2);
    ep_efficiency   = 1 - ep_power/max(ep_power) + randn(size(ep_power,1),1)*.01;
    
    % Adding jitter to data otherwise GP doesn't behave. Could be replace 
    % with a linear/less sophisticated model, but this could probably 
    % generalize to other situations better
    ep_efficiency   = ep_efficiency + randn(size(ep_efficiency,1),1)*.01;
    
    % Construct GP model of objectives
    o_1           = gp_object();
    o_2           = gp_object();
     
    o_1.initialize_data(state, ep_efficiency, lower, upper)
    o_2.initialize_data(state, ep_amplitude, lower, upper)
    
    o_2.covariance_function    = {@covSEard};
    o_2.hyperparameters.cov    = [0; 0; log(std(ep_amplitude)/sqrt(2))];
    
    prior.cov = { {}; {}; {@priorDelta}};
    inf = {@infPrior,@infExact,prior};
    o_2.inference_method = inf;
    
    % Fit models
    o_1.minimize(100)
    o_2.minimize(100);
      
    subplot(1,3,1)
    o_1.plot_mean()
    scatter3(o_1.x_data(end,1),o_1.x_data(end,2), o_1.y_data(end,1), 100, 'r', 'Filled')
    view(-125, 30);
    xlabel('Cathode')
    ylabel('Stimulation Amplitude (mA)')
    zlabel('Efficiency')
    set(gca, 'FontSize', 18)
    hold off
    
    subplot(1,3,2)
    o_2.plot_mean()
    scatter3(o_2.x_data(end,1),o_2.x_data(end,2), o_2.y_data(end,1), 100, 'r', 'Filled')
    xlabel('Cathode')
    ylabel('Stimulation Amplitude (mA)')
    zlabel('EP Magnitude (�V)')
    set(gca, 'FontSize', 18)

    hold off

    
    drawnow
    
    frame = getframe(gcf);
    writeVideo(v,frame);
%     pause(.1)
    
end
close(v);

    
end

