function ep_struct = proc_set_parameter_table(ep_struct)


for c1 = 1:length(ep_struct)
    param_tokens        = strsplit(ep_struct(c1).stim_params, '/');
    
    anode             = param_tokens{1}(1);
    if strcmp(anode, 'C')
        anode = -1;
    else
        anode = str2double(anode);
    end
    
    cathode             = str2double(param_tokens{1}(3));
    amplitude           = str2double(param_tokens{2}(1));
    pulse_width         = str2double(param_tokens{3}(1:2));    
    frequency           = str2double(param_tokens{4}(1:2));
    region              = -1;
    param_tokens{1}
    
    switch param_tokens{1}
        case 'C+0-'
            region = 1;
        case 'C+1-'
            region = 2;
        case 'C+2-'
            region = 3;
        case 'C+3-'
            region = 4;
        case '1+0-'
            region = 5;
        case '0+1-'
            region = 6;
        case '2+1-'
            region = 7;
        case '1+2-'
            region = 8;
        case '3+2-'
            region = 9;
        case '2+3-'
            region = 10;
    end

    data_table                              = table(region, cathode, anode, amplitude, pulse_width, frequency);
    ep_struct(c1).data_table                = data_table;
    
end

end

