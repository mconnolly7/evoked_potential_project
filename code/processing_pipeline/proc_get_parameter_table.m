function data_table_all = p2_get_parameter_table(ep_struct)
% construct data table of stimulation parameters

data_table_all = [];

for c1 = 1:length(ep_struct)    
    data_table_all      = [data_table_all; ep_struct(c1).data_table];

end

end

