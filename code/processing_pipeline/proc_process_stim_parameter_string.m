function data_table  = proc_process_stim_parameter_string(parameter_string)

parameter_table = [];
variable_names  = {'configuration_string','monopolar_coordinates', 'bipolar_coordinates',...
    'amplitude','frequency','pulse_width_a','pulse_width_b','pulse_width_c','polarity','electrode'};
 
for c1 = 1:size(data_table,1)
    
    param_tokens            = parameter_string{c1}, '/');
    configuration_string    = param_tokens{1};
    amplitude_string        = param_tokens{2};
    pulse_shape             = param_tokens{3};
    pulse_frequency_string  = param_tokens{4};
    
    amplitude               = str2double(regexp(amplitude_string, '([0-9]*\.*[0-9]*)','match'));
    
    pulse_width_tokens      = regexp(pulse_shape, '(\d*)','match');
    
    pulse_width_a           = str2double(pulse_width_tokens{1});
    pulse_width_b           = str2double(pulse_width_tokens{2});
    pulse_width_c           = str2double(pulse_width_tokens{3});
    
    frequency               = str2double(regexp(pulse_frequency_string, '(\d*)','match'));
       
    monopolar_coordinates   = [nan];
    bipolar_coordinates     = [nan nan];
    
    switch configuration_string
        %%%
        % Monpolar Ring
        %%%
        case 'C+0-'
            polarity                = 'monopolar';
            electrode              	= 'ring';
            monopolar_coordinates   = 0;
        	bipolar_coordinates     = [0 0];

        case 'C+1-'
            polarity                = 'monopolar';
            electrode               = 'ring';
            monopolar_coordinates   = 1;
            bipolar_coordinates     = [1 1];
            
        case 'C+2-'
            polarity                = 'monopolar';
            electrode               = 'ring';
            monopolar_coordinates   = 2;
            bipolar_coordinates     = [2 2];
            
        case 'C+3-'
            polarity                = 'monopolar';
            electrode               = 'ring';
            monopolar_coordinates   = 3;
            bipolar_coordinates     = [3 3];
            
        case 'C+1_GUI-'
            polarity                = 'monopolar';
            electrode               = 'ring';
            monopolar_coordinates   = 0;
            bipolar_coordinates     = [0 0];
            
        case 'C+2A_GUI2B_GUI2C_GUI-'
            polarity                = 'monopolar';
            electrode               = 'ring';   
            monopolar_coordinates   = 1;
            bipolar_coordinates     = [1 1];
            
        case 'C+3A_GUI3B_GUI3C_GUI-'
            polarity                = 'monopolar';
            electrode              	= 'ring';            
            monopolar_coordinates   = 2;
            bipolar_coordinates     = [2 2];
            
        case 'C+4_GUI-'
            polarity                = 'monopolar';
            electrode              	= 'ring';
            monopolar_coordinates   = 3;
            bipolar_coordinates     = [3 3];
            
        %%%
        % Bipolar Ring
        %%%
        case '0+1-'
            polarity                = 'bipolar';
            electrode               = 'ring';
            bipolar_coordinates     = [0 1];

        case '0+3-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [0 3];

        case '1+0-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [1 0];

        case '1+2-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [1 2];

        case '1_GUI+2A_GUI2B_GUI2C_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [0 1];

        case '1_GUI+4_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [0 3];

        case '2+1-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [2 1];

        case '2+3-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [2 3];

        case '3+0-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [3 0];

        case '3+2-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [3 2];

        case '4_GUI+1_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'ring';
            bipolar_coordinates     = [3 0];

        case '4_GUI+3A_GUI3B_GUI3C_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'ring';  
            bipolar_coordinates     = [3 2];

        %%%
        % Monopolar Segmented
        %%%
        case 'C+2A_GUI-'
            polarity                = 'monopolar';
            electrode              	= 'segmented';
            
        case 'C+2B_GUI-'
            polarity                = 'monopolar';
            electrode              	= 'segmented';
            
        case 'C+2C_GUI-'
            polarity                = 'monopolar';
            electrode              	= 'segmented';
            
        case 'C+3A_GUI-'
            polarity                = 'monopolar';
            electrode              	= 'segmented';
            
        case 'C+3B_GUI-'
            polarity                = 'monopolar';
            electrode              	= 'segmented';
            
        case 'C+3C_GUI-'
            polarity                = 'monopolar';
            electrode              	= 'segmented';
            
        %%%
        % Bipolar Segmented
        %%%
        case '2A_GUI+3A_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'segmented';
            
        case '2B_GUI+3B_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'segmented';
            
        case '2C_GUI+3C_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'segmented';
            
        case '3A_GUI+2A_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'segmented';
            
        case '3B_GUI+2B_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'segmented';
            
        case '3C_GUI+2C_GUI-'
            polarity                = 'bipolar';
            electrode              	= 'segmented';
            
        %%%
        % Unknown
        %%%
        case '3+4-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
        case '4+5-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
        case '4+3-'
            polarity                = 'unkown';
            electrode              	= 'unkown';            
        case '5+4-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
        case '5+6-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
        case '6+5-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
      	case 'C+4-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
        case 'C+5-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
        case 'C+6-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
        case 'C+7-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
        case 'C+8-'
            polarity                = 'unkown';
            electrode              	= 'unkown';
        
    end
    
    configuration_string    = {configuration_string};
    polarity                = {polarity};
    electrode               = {electrode};
    
    parameter_table_row     = table(configuration_string, monopolar_coordinates, bipolar_coordinates, ...
        amplitude, frequency, pulse_width_a, pulse_width_b, pulse_width_c, polarity, electrode, ...
        'VariableNames', variable_names);
    
    parameter_table         = [parameter_table; parameter_table_row];
end

data_table = [data_table parameter_table];
end

