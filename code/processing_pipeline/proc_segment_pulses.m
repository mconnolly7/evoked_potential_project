function ep_struct = proc_segment_pulses(ep_struct)
% find the average of LFP response after each stimulation pulse for a
% single stimulation epoch

for c1 = 1:length(ep_struct)
    ecog_window_ms          = 99;               
    ecog_window_s           = ecog_window_ms/1000;
    ecog_window_idx         = ecog_window_s * ep_struct(c1).sampling_rate_ecog;
    
%     emg_window_ms           = 40;               
%     emg_window_s            = emg_window_ms/1000;
%     emg_window_idx          = emg_window_s * ep_struct(c1).sampling_rate_ecog;
    
    ecog_stim_pulse_offset 	= ep_struct(c1).ecog_pulse_offset_idx;
%     emg_stim_pulse_offset   = ep_struct(c1).emg_pulse_offset_idx;
    
    stim_pulse_time         = ep_struct(c1).stim_pulse_time;
    stim_start_time         = ep_struct(c1).stim_start_time;
    
    ecog_bipolar            = ep_struct(c1).ecog_bipolar;
%     emg                     = ep_struct(c1).emg;
    
    sampling_rate_ecog      = ep_struct(c1).sampling_rate_ecog;
%     sampling_rate_emg       = ep_struct(c1).sampling_rate_ecog; % TODO: Fix this
    
    ecog_matrix             = [];
%     emg_matrix              = [];
    
    for c2 = 1:size(stim_pulse_time,2)
        
        % ECOG
        pulse_start_time_raw        = stim_pulse_time(c2)-stim_start_time;
        ecog_pulse_start_idx_raw  	= floor(pulse_start_time_raw * sampling_rate_ecog);
        ecog_pulse_start_idx       	= ecog_pulse_start_idx_raw + ecog_stim_pulse_offset;
        ecog_pulse_end_idx          = ecog_pulse_start_idx + ecog_window_idx;

        if  ecog_pulse_end_idx < size(ecog_bipolar,2)
        	ecog_matrix(:,:,c2) = ecog_bipolar(:,ecog_pulse_start_idx:ecog_pulse_end_idx);
        end
        
%         % EMG
%         emg_pulse_start_idx_raw  	= floor(pulse_start_time_raw * sampling_rate_emg);
%         emg_pulse_start_idx       	= emg_pulse_start_idx_raw + emg_stim_pulse_offset;
%         emg_pulse_end_idx           = emg_pulse_start_idx + emg_window_idx;
% 
%         if  emg_pulse_end_idx < size(emg,2)
%         	emg_matrix(:,:,c2) = emg(:,emg_pulse_start_idx:emg_pulse_end_idx);
%         end
        
    end
    
    ep_struct(c1).ecog_matrix   = ecog_matrix;
    
    % ecog_matrix is C by T by N matrix where C is 26 channels, T is time
    % index (variable), and N is number of pulses deliverd during the c1
    % stimulation epoch
%     ep_struct(c1).emg_matrix    = emg_matrix;

end

end