function ep_struct = proc_filter_ep_2D(ep_struct, offset, downsample_ratio)

% initializes ep_model as a 2x13, um, model
for c1 = 1:length(ep_struct)
    c1
    ep_model(1)                 = proc_filter_CEP_2D_helper(ep_struct(c1), 1:13, offset, downsample_ratio);
    ep_model(2)                 = proc_filter_CEP_2D_helper(ep_struct(c1), 14:26, offset, downsample_ratio);
    
    ep_struct(c1).ep_model_2D   = ep_model;
end
end


function ep_model = proc_filter_CEP_2D_helper(ep_struct, channels, offset_t, downsample_ratio)

sampling_rate_ecog      = ep_struct.sampling_rate_ecog/downsample_ratio;
offset_idx              = offset_t * sampling_rate_ecog;
offset_idx              = max(floor(offset_idx),1);

ecog_matrix             = ep_struct.ecog_matrix(channels,offset_idx:end,:);
ecog_matrix_centered    = ecog_matrix - mean(ecog_matrix,2);
ecog_mean               = squeeze(mean(ecog_matrix_centered,3));
% ecog_mean           = ecog_mean(:,offset_idx:end);  
% ecog_mean           = ecog_mean - mean(ecog_mean,2);

ecog_mean               = resample(ecog_mean',1,downsample_ratio)';
ecog_reshape            = reshape(ecog_mean',[],1);

tt                      = (1:size(ecog_mean,2))/sampling_rate_ecog * 1000;
tt                      = tt + tt(offset_idx);

t_mat                   = repmat(tt,size(ecog_mean,1),1);
t_reshape               = reshape(t_mat',[],1);

channel_mat             = repmat(channels',1, size(t_mat,2));
channel_reshape         = reshape(channel_mat', [], 1);

X                       = [channel_reshape, t_reshape];
Y                       = ecog_reshape;

ep_model                        = gp_object;
ep_model.covariance_function    = {@covSEard};
ep_model.mean_function          = {@meanZero};
ep_model.initialize_data(X, Y);

% ep_model.minimize(1)
end

