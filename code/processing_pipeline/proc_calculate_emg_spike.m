function ep_struct = p6_calculate_emg_spike(ep_struct, n_sigma)

w = [330 770;
    330 770];

emg_channels = [2 4];

for c1 = 1:length(ep_struct)
    
    pulse_start_idx                 = ep_struct(c1).pulse_start_idx;
    event_detected                  = [];
    
    for c2 = 1:size(emg_channels,2)
        
        window_start                = pulse_start_idx + w(c2,1);
        window_end                  = pulse_start_idx + w(c2,2);
        
        data                        = squeeze(ep_struct(c1).emg_matrix(emg_channels(c2),1:end,:));
        data_filt                   = data - mean(data);
%         data_filt                   = highpass(data_mean, 20, 22000);

        data_window                 = data_filt(window_start:window_end,:);
        sigma                       = std(reshape(data_window,[],1));
        threshold                   = n_sigma*sigma;
        
        event_detected(c2,:)        = any(abs(data_window)>threshold,1);
        emg_threshold(c2)           = threshold;
    end
    
    detected_events                 = sum(sum(event_detected));
    data_table                      = ep_struct(c1).data_table;

    if ismember('detected_events', data_table.Properties.VariableNames)
        data_table.detected_events  = detected_events;
    else
        event_table                 = table(detected_events);
        data_table                  = [data_table event_table];
    end

    ep_struct(c1).data_table        = data_table;    
    ep_struct(c1).event_detected    = event_detected;
    ep_struct(c1).emg_threshold     = emg_threshold;
    
end

end

