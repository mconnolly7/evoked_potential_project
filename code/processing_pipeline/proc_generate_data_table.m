function data_table = proc_generate_data_table(ep_struct_all, save_dir)

data_table = [];
for c1 = 1:size(ep_struct_all,2)
    ep_struct               = ep_struct_all(c1);
    subject_id              = ep_struct.subject_id;
    stim_parameter_string   = ep_struct.stim_parameter_string;
    
    subject_dir             = sprintf('%s%s', save_dir, subject_id);   
    save_path               = sprintf('%s/ep_data_%04d',subject_dir,c1);
    
    data_table_row          = table(c1, {subject_id}, {save_path}, {stim_parameter_string}, ...
        'VariableNames', {'UID', 'subject_id', 'data_path', 'parameter_string'});
    
    data_table              = [data_table; data_table_row];
end

    

end

