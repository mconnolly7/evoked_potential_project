function ep_struct = proc_calculate_ep(ep_struct)

for c1 = 1:length(ep_struct)
    [~, ep_max]                 = ep_struct(c1).ep_model_2D(1).discrete_extrema(2);
    ep_struct(c1).ep_max        = ep_max;
    data_table                  = ep_struct(c1).data_table;
    
    if ismember('ep_max', data_table.Properties.VariableNames)
        data_table.ep_max   = ep_max;
    else
        ep_table        	= table(ep_max);
        data_table          = [data_table ep_table];
    end
    
    ep_struct(c1).data_table = data_table;

end

end

