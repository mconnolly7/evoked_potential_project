function ep_struct_all = proc_load_ep_struct(file_paths)
% loads previously saved ep_structs for a single subject

for c1 = 1:size(file_paths,1)
    load(file_paths{c1});    
    ep_struct_all(c1)   = ep_struct;
end

end


