function ep_struct = proc_filter_emg(ep_struct)

for c1 = 1:length(ep_struct)
emg_matrix          = ep_struct(c1).emg_matrix;
emg_mean            = squeeze(mean(emg_matrix,3));

ep_struct(c1).emg_mean  = emg_mean;
end

