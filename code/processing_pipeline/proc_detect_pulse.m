function patient_struct = proc_detect_pulse(patient_struct)
% detects time index when pulse is delivered 

ep_data = patient_struct.ep_data;

for c1 = 1:length(ep_data)
    stim_pulse_time     = ep_data(c1).stim_pulse_time;
    stim_start_time     = ep_data(c1).stim_start_time;
    ecog_bipolar        = ep_data(c1).ecog_bipolar;
    sampling_rate_ecog  = ep_data(c1).sampling_rate_ecog;
    
    ecog_window_ms       = 10;               
    ecog_window_s        = ecog_window_ms/1000;
    ecog_window_idx      = ecog_window_s * ep_data(c1).sampling_rate_ecog;
    
    ecog_matrix         = [];
    
    for c2 = 1:size(stim_pulse_time,2)
        
        pulse_offset_idx    = max(1,floor((stim_pulse_time(c2)-stim_start_time) * sampling_rate_ecog));
        ecog_pulse_end_idx  = pulse_offset_idx + ecog_window_idx;
        
        if  ecog_pulse_end_idx < size(ecog_bipolar,2)
            ecog_matrix(:,:,c2) = ecog_bipolar(:,pulse_offset_idx:ecog_pulse_end_idx);        
        end

    end
       
    ecog_abs                                = abs(ecog_matrix);
    ecog_mean                               = squeeze(mean(mean(ecog_abs,1),3));

    ecog_diff                               = diff(squeeze(ecog_mean));
    [~, pulse_offset_idx]                   = max(ecog_diff);
    
    ep_data(c1).ecog_pulse_offset_idx    	= pulse_offset_idx;
    ep_data(c1).ecog_pulse_offset_time   	= pulse_offset_idx/sampling_rate_ecog;    

    
end

patient_struct.ep_data = ep_data;

end

