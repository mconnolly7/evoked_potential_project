function patient_struct = proc_segment_bulk_file(patient_struct)

subject_path = patient_struct.subject_path
file_paths = patient_struct.file_paths
subject_id = patient_struct.subject_id

% Create ep_struct array
ep_struct = [];
index = 1;

for i_file_path = 1:size(file_paths,2)
% load bulk files
file_path = [subject_path file_paths{i_file_path}];
load(file_path)

    % segment epochs and populate fields
    for c1 = 1:length(stim_start_times)
        c1
        n_samples                               = size(ecog,2);
        stim_start_idx                          = floor(stim_start_times(c1) * sampling_rate_ecog);
        stim_stop_idx                           = floor(stim_stop_times(c1) * sampling_rate_ecog);
        stim_stop_idx                           = min(n_samples,stim_stop_idx);

        ep_struct(index).ecog                   = ecog(:,stim_start_idx:stim_stop_idx);
        
        if ~isempty(ecog2)
            ep_struct(index).ecog2            	= ecog2(:,stim_start_idx:stim_stop_idx);
        end
%         ep_struct(index).emg                       = emg(:,stim_start_idx:stim_stop_idx);
%         ep_struct(index).emg_labels                = emg_labels;
        ep_struct(index).stim_start_time           = stim_start_times(c1);
        ep_struct(index).stim_stop_time            = stim_stop_times(c1);
        ep_struct(index).stim_pulse_time           = stim_pulse_times{c1};
        ep_struct(index).stim_parameter_string     = stimulation_params_legend{c1};
        ep_struct(index).sampling_rate_ecog        = sampling_rate_ecog;
        ep_struct(index).subject_id                = subject_id;
        
        if index == 46
           a= 1; 
        end
        index = index + 1;

    end

end

patient_struct.ep_data = ep_struct;
% /Users/mconn24/Box Sync/EP_data_for_Mark/ephys007/f15_LT1D-1.024F0004_R_STN_EP_part3_preprocessed

