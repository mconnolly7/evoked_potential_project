function proc_save_ep_struct(ep_struct_all, save_dir)
% ep_struct_all is 1 structure with n entires and m fields; n is number of
% stimulation epochs

for c1 = 1:length(ep_struct_all)
    ep_struct   = ep_struct_all(c1);
    subject_id  = ep_struct.subject_id;
    
    subject_dir    = sprintf('%s%s', save_dir, subject_id);
    if ~exist(subject_dir, 'dir')
        mkdir(subject_dir);
    end
    
    save_path   = sprintf('%s/ep_data_%04d',subject_dir,c1);
    
    save(save_path, 'ep_struct'); % each ep_struct is 1 stim epoch
end
