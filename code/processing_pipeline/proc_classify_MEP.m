clear
channel             = 4;
subject_id          = {'EP002','EP010','EP014','EP015','EP017'};

data_dir            = 'evoked_potential_project/data/';
feature_dir         = [data_dir 'processed_data/'];
label_dir           = [data_dir 'label_data/'];

data_table          = [];

for c1 = 1:size(subject_id,2)
	fprintf('\nSubject: %d\n\tStimulation: ', c1)
    subject_feature_dir    	= [feature_dir subject_id{c1} '/'];
	subject_label_dir    	= sprintf('%s/%s_CH%d.xlsx',label_dir, subject_id{c1}, channel);

    mep_table               = readtable(subject_label_dir);
   	mep_data            	= mep_table{1:end,2:end};
    
	ep_struct               = proc_load_ep_struct(subject_feature_dir);
    for c2 = 1:size(ep_struct,2)
        if ep_struct(c2).data_table.frequency  ~= 10 
            continue;
        end
        
        fprintf('%d, ', c2)
        
        feature_all             = ep_struct(c2).emg_matrix;
        feature_channel         = squeeze(feature_all(channel,170:end,:));
        centered_features       = (feature_channel - mean(feature_channel))';
        
        n_pulses                = size(centered_features,1);
        pulse_id                = (1:n_pulses)';
        
        mep_labels          	= mep_data(:,c2);      
        data_subtable           = table(centered_features, mep_labels(1:n_pulses));
    
        meta_row                = addvars(ep_struct(c2).data_table, subject_id(c1), 'NewVariableNames', {'subject_id'});
        meta_subtable           = repmat(meta_row,n_pulses,1);
        
        pulse_subtable          = table(pulse_id);
        data_subtable           = [pulse_subtable meta_subtable data_subtable];
        
        data_table              = [data_table; data_subtable];
    end
end







