function patient_struct = proc_reference_ecog_2_14(patient_struct)

% Bipolar re-reference for 2x14 ECoG strip 
% ECoG strip is 2 rows by 14 columns 2mm diameter contacts spaced 4mm apart
% ECoG strip situated 3cm lateral to midline with long axis parallel to
% central sulcus

% After rereferencing ecog_bipolar is concatenation of 2 rows by 13
% columns into 26 number of channels

% Load ep_data, bipolar reference, append to ep_data
ep_data = patient_struct.ep_data;

for c1 = 1:length(ep_data)   
    ecog            = ep_data(c1).ecog;
    ecog_bipolar    = nan(size(ecog,1)-2, size(ecog,2));
    
    for c2 = 1:size(ecog_bipolar,1)
        if c2 <14
            ecog_bipolar(c2,:) = ecog(c2+1,:) - ecog(c2,:);
        else
            ecog_bipolar(c2,:) = ecog(c2+2,:) - ecog(c2+1,:);
        end
    end

    ep_data(c1).ecog_bipolar = ecog_bipolar;
    
end   

patient_struct.ep_data = ep_data;

end

