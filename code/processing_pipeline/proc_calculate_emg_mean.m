function ep_struct = p6_calculate_emg_mean(ep_struct, n_sigma)
w = [200 770;
    200 770];

emg_channels = [2 4];

for c1 = 1:length(ep_struct)
    
    pulse_start_idx                 = ep_struct(c1).pulse_start_idx;    
    

    for c2 = 1:size(emg_channels,2)
        
        window_start                    = pulse_start_idx + w(c2,1);
        window_end                      = pulse_start_idx + w(c2,2);
        data                            = squeeze(ep_struct(c1).emg_matrix(emg_channels(c2),1:end,:));
        data_filt                       = data - mean(data);

        data_window                     = data_filt(window_start:window_end,:);
        
        t = ((1:size(data_filt,1))-pulse_start_idx)/22000 * 1000;
        t = t(window_start:window_end);

        m = mean(data_window,2);
        s = std(reshape(data_window,[],1));
        
        plot(t,data_window, 'color', .5*ones(1,3))
        hold on
        
        plot(t,[m m+s m-s])
        hold off
        title(sprintf('Region = %d', ep_struct(c1).data_table.region))
        
        detected_events(c2)                 = (max(m)-min(m));
    end
    
    detected_events                 = mean(detected_events);
    data_table                      = ep_struct(c1).data_table;
%     fprintf('c1 = %d, MEP1 = %.3f, MEP1 = %.3f, mean = %.3f\n', c1, detected_events(1), detected_events(2), mean(detected_events))
    
    if ismember('detected_events', data_table.Properties.VariableNames)
        data_table.detected_events  = detected_events;
    else
        event_table                 = table(detected_events);
        data_table                  = [data_table event_table];
    end

    ep_struct(c1).data_table        = data_table;    

end

end

