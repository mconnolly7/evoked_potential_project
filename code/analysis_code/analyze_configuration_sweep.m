%% 
beta            = 0;
eta             = [0 .01 .1 .5 1 5];
n_samples       = 100;
clear Y_error
for c3 = 1:size(eta,2)
    load_dir = 'evoked_potential_project/data/simulation_data/safe_opt_experiments/EP002/eta_sweep/';
    load_name = sprintf('EP002_beta_%d_eta_%.3f_ns_%d_sweep_%d.mat', beta, eta(c3), n_samples, 5);
    load_path = [load_dir load_name];
      
    load(load_path)
    
    ep_gt       = ep_model.predict(input_space);
    emg_gt      = emg_model.predict(input_space);
    safe_idx    = emg_gt < 1;
    Y_safe_max  = max(ep_gt(safe_idx));

    for c4 = 1:size(X_opt,3)
        X_opt_trial     = squeeze(X_opt(:,:,c4));
        Y_opt_gt(:,c4)  = ep_model.predict(X_opt_trial);
        e_rate(c4,c3)   = error_convergence_rate((1:100)', Y_opt_gt(1:100,c4), Y_safe_max);
    end
    
    X_opt_end       = squeeze(X_opt(end,:,:));
    Y_error(:,c3)   = Y_safe_max  - ep_model.predict(X_opt_end');
    c3
end    
   
    
%%
subplot(2,1,1)
boxplot(Y_error,'Notch', 'on')
ylabel('Y error')
xticklabels({});
set(gca, 'FontSize', 16)

subplot(2,1,2)
boxplot(e_rate,'Notch', 'on')
xlabel('Eta')
ylabel('Convergence Rate')
xticklabels({'0.000', '0.001','0.010','0.100','1.000', '5.000'});
set(gca, 'FontSize', 16)
