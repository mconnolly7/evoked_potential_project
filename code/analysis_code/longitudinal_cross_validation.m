data_path           = 'evoked_potential_project/data/feature_data/MEP_features.mat';
data_table      	= load(data_path);

subjects_table      = data_table.data_table.subject_id;
unique_subjects     = unique(subjects_table);
n_subjects          = size(unique_subjects,1);

X_all               = data_table.data_table.centered_features;
Y_all               = data_table.data_table.mep_labels == 1;

cost_value          = 5;

for c1 = 4:n_subjects-1
    fprintf('Training with %d subjects\n', c1)
    train_subjects 	= nchoosek(1:5,c1);
    n_folds         = size(train_subjects,1);
    
    for c2 = 2:n_folds
        fprintf('\tFold: %d\n', c2);
        
        train_idx   = zeros(size(subjects_table));
        for c3 = 1:size(train_subjects,2)
            train_idx = train_idx | strcmp(unique_subjects(train_subjects(c2,c3)), subjects_table);
        end
        
        cost_matrix         = [0 1;cost_value 0];

        x_train             = X_all(train_idx,:);
        y_train             = Y_all(train_idx,:);
        
        x_test              = X_all(~train_idx,:);
        y_test{c2}          = Y_all(~train_idx,:);
% 
%         if sum(y_train) > 0 && sum(y_test{c2}) > 0
%             svm_model           = fitcsvm(x_train, y_train, 'Standardize',true,...
%                 'KernelFunction','rbf','KernelScale','auto','Cost', cost_matrix);
% 
%             [y_est{c2}, scores] = svm_model.predict(x_test);
%             y_scores{c2}        = scores(:,2);
% 
%             c_mat{c1}(c2,:,:)       = confusionmat(y_test{c2}', y_est{c2}')';
%         else
%             c_mat{c1}(c2,:,:)       = nan(2,2);
%         end
    end
    
end


%%
for c1 = 1:size(c_mat,2)
    for c2 = 1:size(c_mat{c1}, 1)
        cmat_fold                   = squeeze(c_mat{c1}(c2,:,:));
        true_positive{c1}(c2)        = cmat_fold(2,2);
        true_negative{c1}(c2)        = cmat_fold(1,1);
        false_positive{c1}(c2)       = cmat_fold(2,1);
        false_negative{c1}(c2)       = cmat_fold(1,2);
        
        precision{c1}(c2)       = true_positive{c1}(c2) ./ (true_positive{c1}(c2) + false_positive{c1}(c2));
        recall{c1}(c2)          = true_positive{c1}(c2) ./ (true_positive{c1}(c2) + false_negative{c1}(c2));

        f1{c1}(c2)              = 2 * (precision{c1}(c2) .* recall{c1}(c2)) ./ (precision{c1}(c2) + recall{c1}(c2));
    end
end


%%
for c1 = 1:size(true_positive,2)
    true_positive_m(c1)     = nanmean(true_positive{c1});
    true_negative_m(c1)   	= nanmean(true_negative{c1});
    false_positive_m(c1)  	= nanmean(false_positive{c1});
    false_negative_m(c1)  	= nanmean(false_negative{c1});
    
    precision_m(c1)         = nanmean(precision{c1});
    recall_m(c1)            = nanmean(recall{c1});
    f1_m(c1)                = nanmean(f1{c1});
    
    true_positive_s(c1)     = nanstd(true_positive{c1});
    true_negative_s(c1)   	= nanstd(true_negative{c1});
    false_positive_s(c1)  	= nanstd(false_positive{c1});
    false_negative_s(c1)  	= nanstd(false_negative{c1});
    
    precision_s(c1)         = nanstd(precision{c1});
    recall_s(c1)            = nanstd(recall{c1});
    f1_s(c1)                = nanstd(f1{c1});
end
    


%%
close all
X   = [1 1 1
    2 2 2
    3 3 3
    4 4 4]+ [-.1 0 .1];
    
Y_m = [precision_m; recall_m; f1_m]';
Y_s = [precision_s; recall_s; f1_s]';
hold on
bar(X,Y_m,3)
errorbar(X,Y_m,Y_s, 'LineStyle', 'none','LineWidth',3)
xlabel('N Training Subjects');
legend({'Precision', 'Recall', 'F1 Score'})















