data_path           = 'evoked_potential_project/data/feature_data/MEP_features.mat';
data_table      	= load(data_path);

X_all               = data_table.data_table.centered_features;
Y_all               = data_table.data_table.mep_labels == 1;
    
cv                  = cvpartition(Y_all,'KFold',5,'Stratify',true);
costs               = [.01 .1 1 2 5 10 20 50 100 200 2000];

for c2 = 1:size(costs,2)
    fprintf('cost = %.2f\n', costs(c2))
    
    for c1 = 1:cv.NumTestSets
        fprintf('\tcrossval %d, ', c1)
        tic
        
        cost_matrix         = [0 1;costs(c2) 0];

        x_train             = X_all(cv.training(c1),:);
        y_train             = Y_all(cv.training(c1));

        x_test              = X_all(cv.test(c1),:);
        y_test{c1}          = Y_all(cv.test(c1));

        svm_model           = fitcsvm(x_train, y_train, 'Standardize',true,...
            'KernelFunction','rbf','KernelScale','auto','Cost', cost_matrix);

        [y_est{c1}, scores] = svm_model.predict(x_test);
        y_scores{c1}        = scores(:,2);

        c_mat(c2,c1,:,:)       = confusionmat(y_test{c1}', y_est{c1}')';
        fprintf('%.1f\n', toc)
    end

    %
    [X,Y,T,AUC(c2,:)] = perfcurve(y_test,y_scores,1);


    plot(X(:,1), Y(:,1))
    hold on
    drawnow
    ylim([0 1])
end

%%
for c1 = 1:size(c_mat,1)
    for c2 = 1:size(c_mat, 2)
        cmat_fold                   = squeeze(c_mat(c1,c2,:,:));
        true_positive(c1,c2)        = cmat_fold(2,2);
        true_negative(c1,c2)        = cmat_fold(1,1);
        false_positive(c1,c2)       = cmat_fold(2,1);
        false_negative(c1,c2)       = cmat_fold(1,2);
        
    end
end


%%

precision       = true_positive ./ (true_positive + false_positive);
recall          = true_positive ./ (true_positive + false_negative);

beta            = 1;
f1              = (1+beta^2) * (precision .* recall) ./ (beta^2 * precision + recall);
beta            = 2;
f2              = (1+beta^2) * (precision .* recall) ./ (beta^2 * precision + recall);
beta            = .5;
f05             = (1+beta^2) * (precision .* recall) ./ (beta^2 * precision + recall);
hold on
x = 1:10;log(costs(1:10));
plot(x,mean(precision,2))
plot(x,mean(recall,2))
plot(x,mean(f05,2))
plot(x,mean(f1,2))
plot(x,mean(f2,2))
plot(x,AUC(:,1))
legend({'precision','recall','f05','f1','f2','AUC'})