function evhi = evhi_scratch(gp1, gp2, input_space, ref)

Y1_est_baseline = gp1.predict(input_space);
Y2_est_baseline = gp2.predict(input_space);

pareto_idx      = get_pareto_2(1, -1, [Y1_est_baseline Y2_est_baseline]);

volume_baseline = get_discrete_volume(Y1_est_baseline(pareto_idx),Y2_est_baseline(pareto_idx),ref);

for c1 = 1:size(input_space,1)
    gp1_cand            = gp_object();
    gp2_cand            = gp_object();
    
    [Y1_m,~,~,Y1_s]     = gp1.predict(input_space(c1,:));
    [Y2_m,~,~,Y2_s]     = gp2.predict(input_space(c1,:));
        
    X_cand              = [gp1.x_data; input_space(c1,:)];
    Y1_cand             = [gp1.y_data; Y1_m + 1*Y1_s];
    Y2_cand             = [gp2.y_data; Y2_m - 1*Y2_s];
       
    gp1_cand.initialize_data(X_cand,Y1_cand)
    gp2_cand.initialize_data(X_cand,Y2_cand)
    
    Y1_est_cand         = gp1_cand.predict(input_space);
    Y2_est_cand         = gp2_cand.predict(input_space);
    
    pareto_idx_cand     = get_pareto_2(1, -1, [Y1_est_cand Y2_est_cand]);
    volume_cand(c1)     = get_discrete_volume(Y1_est_cand(pareto_idx),Y2_est_cand(pareto_idx),ref);

end
% plot(volume_cand - volume_baseline);

evhi = volume_cand - volume_baseline;
end

function discrete_vol = get_discrete_volume(Y1,Y2,ref)
dy1 = .1;
dy2 = .1;

hold on

grid_1 = ref(1):dy1:max(Y1);
grid_2 = min(Y2):dy2:ref(2);

output_space    = combvec(grid_1, grid_2)';
output_vol      = zeros(size(output_space,1),1);

for c1 = 1:size(Y1,1)
    
    a = max(ref(1),Y1(c1));
    b = min(ref(1),Y1(c1));
    
    c = max(ref(2),Y2(c1));
    d = min(ref(2),Y2(c1));
    
    vol_idx_1   = output_space(:,1) >= b & output_space(:,1) <= a; 
    vol_idx_2   = output_space(:,2) >= d & output_space(:,2) <= c; 
    
    vol_idx                 = vol_idx_1 == 1 & vol_idx_2 == 1;
    output_vol(vol_idx)     = 1;
    
%     patch([b a a b], [d d c c], 'r', 'FaceAlpha', .1)
end
discrete_vol = sum(output_vol) * dy1 * dy2;
end


