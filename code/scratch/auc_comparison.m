%%
[ep1_model, emg_model]      = create_bipolar_gt_models(2);


%%
load('/Users/mconn24/repositories/evoked_potential_project/data/simulation_data/ehvi_hypr_model_search_2020_04_17.mat')
data_struct_ehvi = data_struct;

%%
color = parula(6);
for c1 = 1:6
%     auc_ehvi(c1,:,:) = pareto_roc(data_struct(c1), ep1_model, emg_model);
    
    [ep1_model, emg_model]      = create_bipolar_gt_models(data_struct(c1).subject_id);
    pareto_roc(data_struct(c1), ep1_model, emg_model, color(c1,:));
end

legend({'EP002','EP005','EP007','EP009','EP010','EP012', '50% Chance'})
%%
load('/Users/mconn24/repositories/evoked_potential_project/data/simulation_data/rand_model_search_3D_fixed_emg')
data_struct_rand = data_struct;


auc_rand = pareto_roc(data_struct(1), ep1_model, emg_model);

%%
load('/Users/mconn24/repositories/evoked_potential_project/data/simulation_data/ehvi_hypr_2_model_search_2020_04_17.mat')
data_struct_full = data_struct;

load('/Users/mconn24/repositories/evoked_potential_project/data/simulation_data/ehvi_hypr_2_simple_model_search_2020_04_17.mat')
data_struct_simple = data_struct;

auc_full = pareto_roc(data_struct_full, ep1_model, emg_model, 'r');

auc_simple = pareto_roc(data_struct_simple, ep1_model, emg_model,'b');

