data_dir = '/Users/mconn24/Box Sync/UG3/hippocampal_ccep/';

close all

ant_channels            = [4, 10, 16, 22];
pos_channels            = ant_channels + 1;

t_offset                = .05;

for c1 = 8;
    file_name       = sprintf('KP_2020_01_06_%03d.ns6', c1);
    data_path       = [data_dir file_name];
    table_path      = [data_dir strrep(file_name,'.ns6','_stimulation_table.csv')];
    
    
    openNSx(data_path);
    stimulation_table = readtable(table_path);
end

sampling_frequency_raw  = NS6.MetaTags.SamplingFreq;
sampling_frequency_lp   = sampling_frequency_raw/6;


%% Down sample
lfp_data_raw        = double(NS6.Data(ant_channels, :))';
lfp_downsample      = resample(lfp_data_raw, 1, 6);
lfp_lowpass         = lowpass(lfp_downsample, 250, 5000);


%%
pulse_window_time 	= .1;
pulse_widnow_idx  	= pulse_window_time * sampling_frequency_lp;

onset_window_time  	= 0.05;        
onset_window_idx    = onset_window_time * sampling_frequency_lp;

artifact_time       = .05;
artifact_idx        = artifact_time * sampling_frequency_lp;

clear pulse_data
for c2 = 1:4
    for c1 = 1:size(stimulation_table,1)
        
        % Define the start idx
        stim_start_ts       = stimulation_table{c1,2};
        stim_start_idx      = floor(stim_start_ts*sampling_frequency_lp);
        
        % Detect the artifact start
        onset_segment       = lfp_lowpass(stim_start_idx:stim_start_idx + onset_window_idx,c2);
        d_abs_onset         = abs(diff(onset_segment));
        onset_lag           = find(d_abs_onset > std(d_abs_onset),1);
        onset_idx           = stim_start_idx + onset_lag;

        
        pulse_start_idx     = onset_idx + artifact_idx;
        pulse_end_idx       = pulse_start_idx + pulse_widnow_idx;
        pulse_segment_idx   = pulse_start_idx:pulse_end_idx;
        % store 
        
%         plot(lfp_lowpass(pulse_segment_idx, c2))
        pulse_data(c2,c1,:)  = lfp_lowpass(pulse_segment_idx,c2);
   end
end
%%

close all
clear pulse_smooth
gp_smooth_subsample = 10;
t_pulse             = (1:size(pulse_data,3))/sampling_frequency_lp;

for c1 = 1:60;size(pulse_data,2)
    c1
    
    pulse           = squeeze(pulse_data(:,c1,:))';
    t_pulse_idx     = (1:size(pulse,1))';
    
    for c2 = 1:size(pulse,2)
        
        gp_smooth = gpr_model();
        gp_smooth.initialize_data(t_pulse_idx(1:gp_smooth_subsample:end), pulse(1:gp_smooth_subsample:end,c2))

        gp_smooth.mean_function = {'meanSum' {{@meanPoly,1}, @meanConst}};
        gp_smooth.hyperparameters.mean = [0;0;];
        gp_smooth.minimize(10)
        
        pulse_smooth(c2,c1,:) = gp_smooth.predict(t_pulse_idx);
                
        cla
        hold on
        plot(t_pulse, pulse(:,c2))
        plot(t_pulse,squeeze(pulse_smooth(c2,c1,:)), 'color', .5*[1 1 1])
    end
    
    
end

%%
t_pulse             = (1:size(pulse_data,3))/sampling_frequency_lp;
pulse_processed     = pulse_data - pulse_smooth;
plot(t_pulse,squeeze(mean(pulse_processed([1 ],:,:),2))' - squeeze(mean(pulse_processed([2],:,:),2))')

% hold on
% plot(mean(channel_data - pulse_smooth',2))
% drawnow
%     end
%%
% openNSx('/Users/mconn24/Box Sync/UG3/hippocampal_ccep/lfp_data/datafile002.ns5')
data                    = NS5.Data(4,:);
sampling_frequency      = NS5.MetaTags.SamplingFreq;
t                       = (1:size(data,2))/sampling_frequency;
hold on
plot(t,NS5.Data(20,:))
plot(t,NS5.Data(21,:))

