close all

sampling_rate       = 1/Ch1.interval;
stimulation_start   = 307.5;

threshold           = [];
t_start             = stimulation_start + 50;
t_end               = t_start + 20;

t_start_idx         = floor(t_start * sampling_rate);
t_end_idx           = floor(t_end * sampling_rate);
t_idx               = t_start_idx:t_end_idx;

pulse_data                  = Ch1.values(t_idx); 
pulse_onset_times           = detect_pulse_onset(pulse_data, 0, [], sampling_rate, threshold);

lfp_data                    = Ch5.values(t_idx);
[ep_mean, ep_std, ep_ci]    = ep_stats_on_channel(pulse_data, pulse_onset_times, sampling_rate);

hold on 
plot(ep_mean)
hold on
plot([ep_mean+ep_ci; ep_mean-ep_ci]')

%%

hold on
plot(t_pulse, pulse_data);
plot(pulse_onset_times,1e-3, 'r*')
%%
t = (1:length(Ch12.values)) * Ch12.interval;

plot(t,Ch12.values)

