
data_dir = 'evoked_potential_project/data/subject_models/4D/';

d = dir([data_dir 'E*']);
    
data_table  = [];

for c1 = 1:size(d,1)
    f_name      = d(c1).name;
    data_path   = [data_dir f_name];
    
    data        = load(data_path);
    
    ep1_model   = data.ep1_model;
    emg_model   = data.emg_model;
    
    X_data      = ep1_model.x_data;
    cathode     = X_data(:,1);
    anode       = X_data(:,2);
    amplitude   = X_data(:,3);
    pulse_width = X_data(:,4);
    
    ep1         = ep1_model.y_data;
    emg         = emg_model.y_data;
    
    subject_id  = str2double(f_name(3:5));
    subject_id  = repmat(subject_id, size(ep1));
    
    subject_table = table(subject_id, cathode, anode, amplitude, pulse_width, ep1, emg);
    
    data_table  = [data_table; subject_table];
end

writetable(data_table, 'evoked_potential_data.csv');
