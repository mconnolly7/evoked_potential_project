a = load('/Users/mconn24/repositories/evoked_potential_project/data/simulation_data/monopolar_experiments/EP002_S10_P25_200.mat', 'pareto_idx', 'input_space', 'pareto_idx_gt')

gpc         = gpc_model();
pm          = double(a.pareto_idx(:,end,35));
pm(pm == 0) = -1;
gpc.initialize_data(a.input_space, pm)
gpc.minimize(1)

subplot(1,3,1)
gpc.plot_mean
xlabel('Contact');
view(0,90)
gpc.plot_data
colormap('jet')
subplot(1,3,2)

[~,~,~,fs2] = gpc.predict(a.input_space);
imagesc(flip(reshape(fs2, 4, 50)'))
% gpc.plot_confidence_magnitude
colormap('jet')

subplot(1,3,3)
pm          = double(a.pareto_idx_gt');
pm(pm == 0) = -1;
gpc.initialize_data(a.input_space, pm)
colormap('jet')

gpc.minimize(1)
gpc.plot_mean
xlabel('Contact');
view(0,90)