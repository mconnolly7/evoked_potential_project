close all
load('/Users/mconn24/Box Sync/EP_data_for_Mark/EPdata_forMarkJan2020.mat')
amplitude   = EP(5).StimAmplitude(1:33)';
frequency   = EP(5).StimFrequency(1:33)';
pulse_width = EP(5).StimPulsewidth(1:33)';

cathode     = [1	0	2	2	2	2	0	0	3	1	1	3	2	0	0	...
    3	1	0	2	3	1	2	0	1	3	1	1	3	1	0	0	2	3]';

anode       = [1	3	2	2	1	1	0	1	2	2	1	3	3	1	0	...
    0	0	1	3	2	0	1	0	2	3	1	0	2	2	1	1	2	3]';

% ep1         = EP(5).EP1ramp(1:33, 8);
ep1         = nanmean(EP(5).EP1ramp');
ep1         = ep1(1:33)';

emg         = nanmean(abs(EP(5).relativeAmplitudesEMG(:,1:33,2)))';  % This is probably wrong
 
data_table  = table(cathode, anode, amplitude, pulse_width, frequency, ep1, emg);
pulse_idx   = data_table.pulse_width == 60;

u_amplitudes  = unique(amplitude);
for c1 = 1:size(u_amplitudes,1)
    
    amp_idx     = amplitude == u_amplitudes(c1);
    x1          = data_table.cathode(amp_idx & pulse_idx);
    x2          = data_table.anode(amp_idx & pulse_idx);
    X           = [x1 x2];
    
    Y1           = data_table.ep1(amp_idx & pulse_idx);
    Y2           = data_table.emg(amp_idx & pulse_idx) + randn(size(Y1));
    
    subplot(2,2,1)

    amp_model_1(c1) = gp_object();
    amp_model_1(c1).initialize_data(X,Y1);
%     amp_model_1(c1).minimize(1);
    amp_model_1(c1).plot_mean
%     amp_model_1(c1).plot_data
	view(71,12)

    subplot(2,2,3)

    amp_model_2(c1) = gp_object();
    amp_model_2(c1).initialize_data(X,Y2);
    amp_model_2(c1).minimize(1);
    amp_model_2(c1).plot_mean
	view(71,12)

%     amp_model_2(c1).plot_data
    
end  

x1          = data_table.cathode(pulse_idx);
x2          = data_table.anode(pulse_idx);
x3          = data_table.amplitude(pulse_idx);
X           = [x1 x2 x3];

Y1           = data_table.ep1(pulse_idx);
Y2           = data_table.emg(pulse_idx);
    
subplot(2,2,2);hold on
subplot(2,2,4);hold on
%%
figure
x1          = data_table.cathode(pulse_idx);
x2          = data_table.anode(pulse_idx);
x3          = data_table.amplitude(pulse_idx);
X           = [x1 x2 x3];

X_t         = [X; 0 0 3.2; 0 0 3.4];
Y2_t        = [Y2; 7; 40];
idx_t       = X_t(:,1) == 0 & X_t(:,2) == 0 ;

joint_model_ep = gp_object();
joint_model_ep.initialize_data(X,Y1)
% joint_model_ep.hyperparameters.lik = -2

% joint_model_ep.minimize(10)

joint_model_emg = gp_object();
joint_model_emg.initialize_data(X,Y2)
% joint_model_emg.initialize_data(X_t,Y2_t)
% joint_model_emg.hyperparameters.cov(1) = 1000;
% joint_model_emg.hyperparameters.lik = 1;
% joint_model_emg.minimize(10)
%

amp_input = combvec(0,0,1:.1:5)';
hold on
[f, s] = joint_model_emg.predict(amp_input);
[f, s] = joint_model_ep.predict(amp_input);
plot(amp_input(:,3), [f f+s f-s])
% scatter(X_t(idx_t, 3), Y2_t(idx_t));
% plot(amp_input(:,3), s)
%
figure
input_anode     = 0:.1:3;
input_cathode   = 0:.1:3;
%
for c1 = 1:3
    input_amp       = u_amplitudes(c1);

    input           = combvec(input_cathode, input_anode, input_amp)';
    Y1_est          = joint_model_ep.predict(input);
    Y2_est          = joint_model_emg.predict(input);

    t1              = reshape(input(:,1), size(input_cathode,2), size(input_anode,2));
    t2              = reshape(input(:,2), size(input_cathode,2), size(input_anode,2));
    t3              = reshape(input(:,3), size(input_cathode,2), size(input_anode,2));
    e1              = reshape(Y1_est, size(input_cathode,2), size(input_anode,2));
    e2              = reshape(Y2_est, size(input_cathode,2), size(input_anode,2));
    
    subplot(2,2,1)
    hold on
    surf(t1,t2,e1, 'LineStyle', 'none', 'FaceAlpha', .5)
  	view(71,12)

    subplot(2,2,3)
    hold on
    surf(t1,t2,e2, 'LineStyle', 'none', 'FaceAlpha', .5)
    view(71,12)
end

input_anode     = 0:3;
input_cathode   = 0:3;
input_amp       = 0:.1:5;
input           = combvec(input_cathode, input_anode, input_amp)';

Y1_est          = joint_model_ep.predict(input);
Y2_est          = joint_model_emg.predict(input);

for c1 = 1:size(Y1_est,1)
    d1                  = Y1_est(c1) >= Y1_est;
    d2                  = Y2_est(c1) <= Y2_est;
    is_pareto(c1,1)     = all(d1 | d2);
end

subplot(2,2,[2 4])
hold on
scatter(Y1_est(is_pareto(:,1)), Y2_est(is_pareto(:,1)), 'r', 'filled')
scatter(Y1_est(~is_pareto(:,1)), Y2_est(~is_pareto(:,1)), 'k', 'filled')


joint_model_ep = gp_object();
joint_model_ep.initialize_data(X,Y1)
% joint_model_ep.minimize(10)
%%
input_anode     = 0:3;
input_cathode   = 0:3;
input_amp       = 0:.1:5;
input_pareto    = combvec(input_cathode, input_anode, input_amp)';


input_amp = combvec(0,0,1:.1:5)';
hold on

joint_model_emg = gp_object();
X               = X(1:31,:);
Y2              = Y2(1:31);
joint_model_emg.initialize_data(X,Y2)
% plot(input_amp(:,3),joint_model_emg.predict(input_amp))
Y1_est          = joint_model_ep.predict(input_pareto);
Y2_est          = joint_model_emg.predict(input_pareto);

for c1 = 1:size(Y1_est,1)
    d1                  = Y1_est(c1) >= Y1_est;
    d2                  = Y2_est(c1) <= Y2_est;
    is_pareto(c1,1)       = all(d1 | d2);
end
scatter(Y2_est(is_pareto), Y1_est(is_pareto), 'filled')
scatter(Y2_est(~is_pareto), Y1_est(~is_pareto), 'filled')
% plot(Y2_est(is_pareto), Y1_est(is_pareto))

%
joint_model_emg = gp_object();
X(32,:)     = [0 0 3.1];
Y2(32)      = max(Y2);
joint_model_emg.initialize_data(X,Y2)
% plot(input(:,3),joint_model_emg.predict(input))
Y1_est          = joint_model_ep.predict(input_pareto);
Y2_est          = joint_model_emg.predict(input_pareto);

for c1 = 1:size(Y1_est,1)
    d1                  = Y1_est(c1) >= Y1_est;
    d2                  = Y2_est(c1) <= Y2_est;
    is_pareto(c1,2)       = all(d1 | d2);
end
scatter(Y2_est(is_pareto), Y1_est(is_pareto), 'filled')
scatter(Y2_est(~is_pareto), Y1_est(~is_pareto), 'filled')
% plot(Y2_est(is_pareto), Y1_est(is_pareto))


joint_model_emg = gp_object();
X(32,:)     = [0 0 4.9];
Y2(32)      = [max(Y2)];
joint_model_emg.initialize_data(X,Y2)
% plot(input(:,3),joint_model_emg.predict(input))
Y1_est          = joint_model_ep.predict(input_pareto);
Y2_est          = joint_model_emg.predict(input_pareto);

for c1 = 1:size(Y1_est,1)
    d1                  = Y1_est(c1) >= Y1_est;
    d2                  = Y2_est(c1) <= Y2_est;
    is_pareto(c1,3)       = all(d1 | d2);
end
% scatter(Y2_est(is_pareto), Y1_est(is_pareto), 'filled')
% scatter(Y2_est(~is_pareto), Y1_est(~is_pareto), 'filled')
% plot(Y2_est(is_pareto), Y1_est(is_pareto))
%

%% sigmoid scratch

x = -0:.01:120;
k = .1;
y = 1./(1+exp(-k*x+4));
plot(x,y)




