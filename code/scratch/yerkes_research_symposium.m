close all; ; clc;
data_table_path = 'evoked_potential_project/data/processed_data/data_table';
load(data_table_path);

subject_id          = 'EP002'; emg_channels        = [4 6 8];
% subject_id          = 'EP010'; emg_channels        = [2];
offset              = .002;
polarity            = 'monopolar';
electrode           = 'ring';

frequency           = 10;
pulse_width_a       = 60;

valid_idx           = strcmp(data_table.subject_id, subject_id);
valid_idx           = valid_idx & strcmp(data_table.polarity, polarity);
valid_idx           = valid_idx & strcmp(data_table.electrode, electrode);
valid_idx           = valid_idx & data_table.pulse_width_a == pulse_width_a;
valid_idx           = valid_idx & data_table.frequency == frequency;
valid_table         = data_table(valid_idx,:);

ep_struct           = proc_load_ep_struct(valid_table.data_path);
ep_struct           = proc_reference_ecog_2_14(ep_struct);
ep_struct           = proc_detect_pulse(ep_struct);
ep_struct           = proc_segment_pulses(ep_struct);
ep_struct           = proc_filter_ep_2D(ep_struct, offset, 1);

%%
close all
X                   = [valid_table.monopolar_coordinates valid_table.amplitude];

input_channels      = 1:13;
input_times         = 2:.01:4;

cep_input_space     = combvec(input_channels, input_times)';
for c1 = 1:size(ep_struct,2)
    cep_model       = ep_struct(c1).ep_model_2D(1);
    cep_model.minimize(1)
    p2_window       = cep_model.predict(cep_input_space);
    Y_p2(c1,1)      = max(p2_window);
    
    emg_matrix      = ep_struct(c1).emg_matrix(emg_channels,200:end,:);
    emg_centered    = emg_matrix - mean(emg_matrix, 2);
    emg_mean        = mean(emg_centered,3);
    
    emg_peaks       = max(emg_mean,[],2);

    Y_emg(c1,1)     = sum(emg_peaks);
end

%%
close all;
figure;
hold on
p2_window = reshape(p2_window,13,[]);
for c1 = 1:13
    p2_mat(c1,:) = p2_window(c1:13:end);
end

for c1 = 6
    plot(input_times, p2_mat(c1,:) + 0*5*c1-1, 'LineWidth',2);
end
xlabel('milliseconds (ms)')
% yticks([])
ylabel('microvolts (mV)');
set(gca, 'FontSize', 16)
%%

close all
p2_model    = gp_object();
emg_model   = gp_object();

p2_model.initialize_data(X, Y_p2);
emg_model.initialize_data(X, Y_emg);

input_channels          = 0:3;
input_amplitude         = 1:.1:5;
input_space             = combvec(input_channels, input_amplitude)';     

p2_est                  = p2_model.predict(input_space);
emg_est                 = emg_model.predict(input_space);

scatter(p2_est, emg_est)

for c1 = 1:size(p2_est,1)
    d1                  = p2_est(c1) >= p2_est;
    d2                  = emg_est(c1) <= emg_est;
    is_pareto(c1)       = all(d1 | d2);
end

subplot(2,2,4)
hold on
scatter(p2_est(is_pareto), emg_est(is_pareto), 'r')
scatter(p2_est(~is_pareto), emg_est(~is_pareto), 'k')

%%
input_x     = reshape(input_space(:,1), 4, []);
input_y     = reshape(input_space(:,2), 4, []);
output_z    = reshape(is_pareto, 4, []);

subplot(2,2,1)
hold on
for c1 = 1:4
    hold on
    scatter(input_x(c1,~output_z(c1,:)), input_y(c1,~output_z(c1,:)),'k')
    scatter(input_x(c1,output_z(c1,:)), input_y(c1,output_z(c1,:)),'r')
end

subplot(2,2,3)
p2_model.plot_mean;
subplot(2,2,2)
emg_model.plot_mean

%%
ep_struct   = proc_reference_ecog_2_14(ep_struct);
ep_struct   = proc_detect_pulse(ep_struct);
ep_struct   = proc_segment_pulses(ep_struct);

emg4        = squeeze(ep_struct.emg_matrix(4,:,:));

plot((1:length(emg4))/22, emg4 - mean(emg4), 'k', 'LineWidth', 2)
box off
set(gca, 'FontSize', 16)
xlabel('milliseconds')
ylabel('mV')

%%
close all
image_path = '/Users/mconn24/Dropbox/research/Conferences/Yerkes/ecog_sweep_labeled.png';
image_data =  imread(image_path);

xImage = [-.5 3; -.5 3];   % The x data for the image corners
zImage = [0 0; 0 0];             % The y data for the image corners
yImage = [5 5; .5 .5];   % The z data for the image corners
surf(xImage,yImage,zImage,...    % Plot the surface
 'CData',image_data,...
 'FaceColor','texturemap', 'EdgeColor', 'none');

ylabel('Amplitude (mA)')
xlabel('Monopolar Cathode')
zlabel('EP1 Magnitude')

xlim([-.5 3])
ylim([.5 5])

set(gca,'FontSize', 16)
hold on 
p2_model.minimize(1)
p2_model.plot_mean 
p2_model.plot_data
caxis([0 12])
colormap('jet')
 
print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/surface_with_image.png', '-dpng', '-r200')

%%
close all
figure
scatter(p2_model.y_data(8), emg_model.y_data(8),200, 'filled', 'MarkerFaceColor', ...
    0*ones(1,3), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')

xlim([0 12])
ylim([4 22]);
xlabel('EP1 Magnitude')
ylabel('EMG Magnitude')
set(gca,'FontSize', 16)
print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/pareto_1', '-dpng', '-r300')


figure
scatter(p2_model.y_data(7:8), emg_model.y_data(7:8),200, 'filled', 'MarkerFaceColor', ...
    0*ones(1,3), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')

xlim([0 12])
ylim([4 22]);
xlabel('EP1 Magnitude')
ylabel('EMG Magnitude')
set(gca,'FontSize', 16)
print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/pareto_2', '-dpng', '-r300')

figure
scatter(p2_model.y_data, emg_model.y_data, 200, 'filled', 'MarkerFaceColor', ...
    0*ones(1,3), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')

xlim([0 12])
ylim([4 22]);
xlabel('EP1 Magnitude')
ylabel('EMG Magnitude')
set(gca,'FontSize', 16)
print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/pareto_all', '-dpng', '-r300')
%%
figure

xlim([0 12])
ylim([4 22]);
xlabel('EP1 Magnitude')
ylabel('EMG Magnitude')
set(gca,'FontSize', 16)
print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/pareto_0', '-dpng', '-r300')

%%
close all

figure
p2_model.initialize_data(p2_model.x_data, p2_model.y_data);
p2_model.minimize(1)
p2_model.plot_mean;
p2_model.plot_data;
ylabel('Milliamps (mA)')
xlabel('Monopolar Cathode')
zlabel('EP1 Magnitude')
colormap('jet')
set(gca,'FontSize', 16)

figure
emg_model.initialize_data(emg_model.x_data, emg_model.y_data);
emg_model.hyperparameters.lik = -1;
% emg_model.hyperparameters.mean = 0
emg_model.minimize(1)
emg_model.plot_mean
emg_model.plot_data
ylabel('Milliamps (mA)')
xlabel('Monopolar Cathode')
zlabel('EMG Magnitude')
colormap('jet')
set(gca,'FontSize', 16)


%
% close all

p2_est                  = p2_model.predict(input_space);
emg_est                 = emg_model.predict(input_space);

% scatter(p2_est, emg_est)

for c1 = 1:size(p2_est,1)
    d1                  = p2_est(c1) >= p2_est;
    d2                  = emg_est(c1) <= emg_est;
    is_pareto(c1)       = all(d1 | d2);
end

figure
hold on

scatter(p2_est(~is_pareto), emg_est(~is_pareto), 200, 'filled', 'MarkerFaceColor', ...
    0*ones(1,3), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')

scatter(p2_est(is_pareto), emg_est(is_pareto),  200, 'filled', 'MarkerFaceColor', ...
    'R', 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')


% xlim([2 10])
% ylim([4 16]);
xlabel('EP1 Magnitude')
ylabel('EMG Magnitude')
set(gca,'FontSize', 16)
print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/pareto_interpolated', '-dpng', '-r300')

%
figure

input_x     = reshape(input_space(:,1), 4, []);
input_y     = reshape(input_space(:,2), 4, []);
output_z    = reshape(is_pareto, 4, []);

hold on
for c1 = 1:4
    hold on
    scatter(input_x(c1,~output_z(c1,:)), input_y(c1,~output_z(c1,:)), 200, 'filled', 'MarkerFaceColor', ...
    0*ones(1,3), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')

    scatter(input_x(c1,output_z(c1,:)), input_y(c1,output_z(c1,:)), 200, 'filled', 'MarkerFaceColor', ...
    'r', 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')
end

xticks([0 1 2 3])
xlabel('Contact Configuration')
xticklabels({'C+/0-', 'C+/1-', 'C+/2-', 'C+/3-'})
ylabel('Milliamp (mA)')
set(gca,'FontSize', 16)
xlim([-.5 3.5])


