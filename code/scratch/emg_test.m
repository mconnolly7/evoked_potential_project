% Load the EP model 
close all
load_dir                    = 'evoked_potential_project/data/modeled_data/bipolar_ring/LOK/bipolar_ring_model';
data_struct                 = load(load_dir);

ep_model                    = data_struct.joint_model_ep;
ep_model.initialize_data(ep_model.x_data, ep_model.y_data*10);
emg_model                   = data_struct.joint_model_emg;

u_amplitudes    = [1 3 5];
input_anode     = 0:.1:3;
input_cathode   = 0:.1:3;

emg_model.hyperparameters.lik = 1;

for c2 = 1:3
    input_amp       = u_amplitudes(c2);
    
    input           = combvec(input_cathode, input_anode, input_amp)';
    Y1_est          = ep_model.predict(input);
    Y2_est          = emg_model.predict(input);
    
    t1              = reshape(input(:,1), size(input_cathode,2), size(input_anode,2));
    e1              = reshape(Y1_est, size(input_cathode,2), size(input_anode,2));
    e2              = reshape(Y2_est, size(input_cathode,2), size(input_anode,2));
    
    subplot(1,2,1)
    hold on
    surf(t1,t2,e1, 'LineStyle', 'none', 'FaceAlpha', .5)
    view(71,12)

    subplot(1,2,2)
    hold on
    surf(t1,t2,e2, 'LineStyle', 'none', 'FaceAlpha', .5)
    view(71,12)
    scatter3(emg_model.x_data(:,1),emg_model.x_data(:,2), emg_model.y_data(:,1))
end