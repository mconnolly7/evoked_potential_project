clear
load('/Users/mconn24/Box Sync/EP_data_for_Mark/EPdata_forMarkJan2020.mat')
papers_dir      = '/Users/mconn24/Box Sync/papers/';
parameter_dir   = [papers_dir '2019_07_01_paper_evoked_potential_optimization/data/parameter_tables/'];
file_name       = {'EP002', 'LOK', 'EP010', 'EP015', 'EP017'};
data_idx        = [2 5 10 15 17];

contact         = [0 1 2 3];
amplitude       = 1:.1:5;
input_space     = combvec(contact, amplitude)';
close all

c1              = 2;
for c1 = 1%:5
    parameter_path  = [parameter_dir file_name{c1} '.xlsx'];


    data_table      = readtable(parameter_path);

    valid_idx       = ~isnan(data_table.amplitude);
    contact         = data_table.monopolar_contact(valid_idx);
    amplitude       = data_table.amplitude(valid_idx);
    X               = [contact amplitude];

    ep1             = EP(data_idx(c1)).EP1ramp;
    emg             = EP(data_idx(c1)).amplitudesEMG;

    ep1_magnitude   = max(ep1(:,1:13),[],2);
    Y_ep1           = ep1_magnitude(valid_idx);
    
    emg_peaks       = squeeze(max(emg,[],1));
    emg_magnitude   = nansum(emg_peaks,2);
    Y_emg           = emg_magnitude(valid_idx);

    ep1_model       = gp_object();
    emg_model       = gp_object();

    ep1_model.initialize_data(X, Y_ep1);
    emg_model.initialize_data(X, Y_emg);
    
    y1_est                      = ep1_model.predict(input_space);
    y2_est                      = emg_model.predict(input_space);
    
    costs_est                   = [y1_est y2_est];
    [pareto_idx, dom_order]     = get_pareto_2(1, -1, costs_est);
    
    pareto_proportion(c1)      	= mean(pareto_idx);
    
    if 1
        ep1_model.minimize(1)
        emg_model.minimize(1)

        figure; %subplot(2,2,1)
        ep1_model.plot_mean
        ep1_model.plot_data
        xlabel('Contact Configuration')
        ylabel('Amplitude (mA)')
        zlabel('EP1 Magnitude (�V)')
        set(gca,'FontSize', 16)
        colormap('jet')
        
        image_path = '/Users/mconn24/Dropbox/research/Conferences/Yerkes/ecog_sweep_labeled.png';
        image_data =  imread(image_path);

        xImage = [-.5 3; -.5 3];   % The x data for the image corners
        zImage = [0 0; 0 0];             % The y data for the image corners
        yImage = [5 5; .5 .5];   % The z data for the image corners
        surf(xImage,yImage,zImage,...    % Plot the surface
         'CData',image_data,...
         'FaceColor','texturemap', 'EdgeColor', 'none');
        caxis([0 6.5])

        ylabel('Amplitude (mA)')
        xlabel('Monopolar Cathode')
        zlabel('EP1 Magnitude')

        xlim([-.5 3])
        ylim([.5 5])

        print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/ep1_surface', '-dpng', '-r300')

        figure; %subplot(2,2,2)
        emg_model.plot_mean
        emg_model.plot_data
        xlabel('Contact Configuration')
        ylabel('Amplitude (mA)')
        zlabel('EMG Magnitude (�V)')
        set(gca,'FontSize', 16)
        colormap('jet')
        print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/emg_surface', '-dpng', '-r300')

        input_x1        = reshape(input_space(:,1), 4, []);
        input_x2        = reshape(input_space(:,2), 4, []);
        pareto_y        = reshape(pareto_idx, 4, []);
        output_y1       = reshape(costs_est(:,1), 4, []);
        output_y2       = reshape(costs_est(:,2), 4, []);
      
        figure; %subplot(2,2,4)
        hold on
        %%
        close all
        marker_styles = {'s', 'd', '^', 'o'}
        for c2 = 1:4
            hold on
            scatter(input_x1(c2,~pareto_y(c2,:)), input_x2(c2,~pareto_y(c2,:)), 200, 'filled', 'MarkerFaceColor', ...
                0*ones(1,3), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k', 'Marker',  marker_styles{c2})
            
            scatter(input_x1(c2,pareto_y(c2,:)), input_x2(c2,pareto_y(c2,:)), 200, 'filled', 'MarkerFaceColor', ...
                'r', 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k', 'Marker',  marker_styles{c2})
        end
        
        xticks([0 1 2 3])
        xlabel('Contact Configuration')
        xticklabels({'C+/0-', 'C+/1-', 'C+/2-', 'C+/3-'})
        ylabel('Milliamp (mA)')
        set(gca,'FontSize', 16)
        xlim([-.5 3.5])
%         title(file_name{c1})
        print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/parameter_space', '-dpng', '-r300')
%%
        figure; %subplot(2,2,3)
        hold on
        scatter(output_y1(~pareto_y), output_y2(~pareto_y), 200, 'filled', 'MarkerFaceColor', ...
                0*ones(1,3), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')
        scatter(output_y1(pareto_y), output_y2(pareto_y), 200, 'filled', 'MarkerFaceColor', ...
                'r', 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')
        xlabel('EP1 Magnitude')
        ylabel('EMG Magnitude')
        set(gca,'FontSize', 16)
        print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/objective_plot_highlight', '-dpng', '-r300')
        
        figure; %subplot(2,2,3)
        scatter(costs_est(:,1), costs_est(:,2), 200, 'filled', 'MarkerFaceColor', ...
                0*ones(1,3), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k')
        xlabel('EP1 Magnitude')
        ylabel('EMG Magnitude')
        set(gca,'FontSize', 16)
        print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/objective_plot', '-dpng', '-r300')
        
        %%
        figure; %subplot(2,2,4)
        hold on
        colors = [197 90 16; 
            84 130 53
            50 117 182
            191 144 0]/255;
        
        marker_styles = {'s', 'd', '^', 'o'}
        for c2 = 1:4
            hold on
%             scatter(output_y1(c2,~pareto_y(c2,:)), output_y2(c2,~pareto_y(c2,:)), 200, 'filled', 'MarkerFaceColor', ...
%                 0*ones(1,3), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k', 'Marker',  marker_styles{c2})
%             
%             scatter(output_y1(c2,pareto_y(c2,:)), output_y2(c2,pareto_y(c2,:)), 200, 'filled', 'MarkerFaceColor', ...
%                 'k', 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k', 'Marker',  marker_styles{c2})
            
            scatter(output_y1(c2,:), output_y2(c2,:), 200, 'filled', 'MarkerFaceColor', ...
                colors(c2,:), 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k', 'Marker',  marker_styles{c2})
        end
        
        xlabel('EP1 Magnitude')
        ylabel('EMG Magnitude')
        set(gca,'FontSize', 16)
        print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/objective_space_shape_color', '-dpng', '-r300')
        
    end
       
end

%%
close all
figure
m = mean(pareto_proportion*100);
s = std(pareto_proportion*100);

hold on
bar([1 2],[100 m], 'FaceColor', .5*ones(1,3), 'EdgeColor', 'k', 'LineWidth',2)
errorbar([1 2],[100 m]', [0 s]', 'LineStyle', 'none', 'LineWidth', 2, ...
    'color', 'k')

plot([1 2], [110 110], 'k', 'LineWidth', 2)
annotation('textbox',[.5 .83 .1 .1],'String','*','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', 30);

rng(3)
x = rand(1,5)/1.5+1.5 + 1/8;
scatter(x, pareto_proportion*100, 200, 'filled', 'MarkerFaceAlpha', .5, 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'r')

xticks([1 2])
xticklabels({'Full Space', 'Optimal Space'})
set(gca, 'FontSize', 16)
ylabel('Parameter Space Size')
print('/Users/mconn24/Dropbox/research/Conferences/Yerkes/summary', '-dpng', '-r300')