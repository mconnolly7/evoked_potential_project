function [ep1_model, emg_model] = model_ep_emg_objective_4D(PLOT)
% close all
load('/Users/mconn24/Box Sync/EP_data_for_Mark/EPdata_forMarkJan2020.mat')

if ~exist('PLOT', 'var')
    PLOT = 1;
end

% Collect and organize collected 3D data
amplitude   = EP(5).StimAmplitude(1:33)';
frequency   = EP(5).StimFrequency(1:33)';
pulse_width = EP(5).StimPulsewidth(1:33)';

cathode     = [1	0	2	2	2	2	0	0	3	1	1	3	2	0	0	...
    3	1	0	2	3	1	2	0	1	3	1	1	3	1	0	0	2	3]';

anode       = [1	3	2	2	1	1	0	1	2	2	1	3	3	1	0	...
    0	0	1	3	2	0	1	0	2	3	1	0	2	2	1	1	2	3]';

ep1             = nanmean(EP(5).EP1ramp');
ep1             = ep1(1:33)';

emg             = nanmean(abs(EP(5).relativeAmplitudesEMG(:,1:33,2)))';  % This is probably wrong
 
data_table      = table(cathode, anode, amplitude, pulse_width, frequency, ep1, emg);

data_table_20   = data_table;
data_table_60   = data_table;
data_table_120  = data_table;

data_table_20.pulse_width   = repmat(20,33,1);
data_table_20.ep1           = data_table_20.ep1 * pulse_width_sigmoid(20);
data_table_20.emg           = data_table_20.emg * pulse_width_sigmoid(20);

data_table_60.pulse_width   = repmat(60,33,1);
data_table_60.ep1           = data_table_60.ep1 * pulse_width_sigmoid(60);
data_table_60.emg           = data_table_60.emg * pulse_width_sigmoid(60);

data_table_120.pulse_width  = repmat(120,33,1);
data_table_120.ep1          = data_table_120.ep1 * pulse_width_sigmoid(120);
data_table_120.emg          = data_table_120.emg * pulse_width_sigmoid(120);

data_table_4D               = [data_table_20; data_table_60; data_table_120];

X_cathode       = data_table_4D.cathode;
X_anode         = data_table_4D.anode;
X_amplitude     = data_table_4D.amplitude;
X_pulse_width  	= data_table_4D.pulse_width;
X               = [X_cathode, X_anode, X_amplitude, X_pulse_width];

ep1_model       = gp_object;
emg_model       = gp_object;

Y_ep1           = data_table_4D.ep1;
Y_emg           = data_table_4D.emg;

ep1_model.initialize_data(X,Y_ep1);
emg_model.initialize_data(X,Y_emg);

input_anode     = 0:.1:3;
input_cathode   = 0:.1:3;

u_amplitudes    = unique(X_amplitude);
u_pulse_width   = unique(X_pulse_width);

if PLOT
    for c2 = 1:3

        input_pw        = u_pulse_width(c2);

        subplot(1,3,c2)
        for c1 = 1:3
            input_amp       = u_amplitudes(c1);

            input           = combvec(input_cathode, input_anode, input_amp, input_pw)';
            Y1_est          = ep1_model.predict(input);

            t1              = reshape(input(:,1), size(input_cathode,2), size(input_anode,2));
            t2              = reshape(input(:,2), size(input_cathode,2), size(input_anode,2));
            t3              = reshape(input(:,3), size(input_cathode,2), size(input_anode,2));
            e1              = reshape(Y1_est, size(input_cathode,2), size(input_anode,2));

            hold on
            surf(t1,t2,e1, 'LineStyle', 'none', 'FaceAlpha', .5)
            view(71,12)
            zlim([0 1.2])
 
        end
        colormap('jet')
        xlabel('Cathode')
        ylabel('Anode')
        zlabel('EP1 Magnitude')
        
        set(gca,'FontSize', 14)
        title(sprintf('Pulse Width %dµs', input_pw))
    end
    
end
end


function pw_multiplier = pulse_width_sigmoid(x)


% x = -0:.01:120;
k               = .1;
pw_multiplier   = 1./(1+exp(-k*x+4));

end


