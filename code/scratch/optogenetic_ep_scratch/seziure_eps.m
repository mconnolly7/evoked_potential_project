function seziure_eps
close all
sampling_rate_          = 2000;
window_duration_t       = .1; % seconds
window_duration_idx     = window_duration_t * sampling_rate_; 
sz_times                = [69 109; 2357 2399];

ep_data_all             = [];
data_all                = [];
pulse_idx_all           = [];
pulse_time_all          = [];

data_dir = '/Users/mconn24/OneDrive - Emory University/projects/optogenetic_epilepsy_ep/EPI_042_7Hz-stim/';
experiment_table_d      = dir([data_dir 'experiment_table*']);
experiment_table_path   = [data_dir experiment_table_d.name];
load(experiment_table_path)

d = dir([data_dir 'Resampled*']);
for c1 = 1:3;size(d,1)
    c1
    clear ep_data
    file_name               = d(c1).name;
    file_path               = [data_dir file_name];
    
    load(file_path)
    
    frequency               = experiment_table.pulse_frequency_pulse(c1);
    start_time              = experiment_table.stimulation_time(c1);

    pulse_start_idx         = get_pulse_start(stim, 1, frequency, sampling_rate_);
    pulse_start_time        = pulse_start_idx / sampling_rate_ + start_time;
    
    for c2 = 1:size(pulse_start_idx,2)
        ep_window           = pulse_start_idx(c2):pulse_start_idx(c2)+window_duration_idx;
        ep_data(:,c2)       = data(2,ep_window);
        
    end
    
    pulse_idx_all           = [pulse_idx_all pulse_start_idx];
    pulse_time_all          = [pulse_time_all pulse_start_time];
    ep_data_all             = [ep_data_all ep_data];
    data_all                = [data_all data(2,:)];
end

counter = 1;
for c1 = 7:7:size(ep_data_all,2)-10
%     hold on
    mean_idx = c1-6:c1;
    ep_mean(:, counter) = mean(ep_data_all(:, mean_idx),2);
    ep_time(:, counter) = pulse_time_all(mean_idx(end));
    counter = counter+1;
end

[~, sz_start_idx]   = min(abs(ep_time - sz_times(1,1)));
[~, sz_end_idx]     = min(abs(ep_time - sz_times(1,2)));
hold on
t = (1:201)/2;
a = plot(t, ep_mean(:,sz_start_idx+1:sz_end_idx)*1000^2, 'color', .8 * [1 0 0 ])

b = plot(t, ep_mean(:,1:sz_start_idx)*1000^2, 'color', .5 * [1 1 1])
% subplot(1,3,1)
% surf(ep_mean(:,1:sz_start_idx), 'EdgeColor', 'none')
% subplot(1,3,2)
% surf(ep_mean(:,sz_start_idx+1:sz_end_idx), 'EdgeColor', 'none')
% subplot(1,3,3)
% surf(ep_mean(:,sz_end_idx+1:end), 'EdgeColor', 'none')

xlabel('Milliseconds')
ylabel('Microvolts')
xlim([0 100])
set(gca, 'FontSize', 24)
legend([b(1) a(1)], {'Pre-Seizure','Seizure'}, 'Box', 'off')
end

function pulse_start_idx = get_pulse_start(stim_data, threshold, frequency, sampling_rate_)
    d_stim          = stim_data(2:end) - stim_data(1:end-1);
    
    offset          = floor(1/frequency * sampling_rate_ - 10);  
    peak_idx        = find(d_stim > threshold);
    window_start    = 1;
    c1              = 1;
    pulse_stop      = window_start+offset;

    while pulse_stop < size(d_stim,2)
        pulse_window                = window_start:pulse_stop;
        [~, pulse_start_idx_rel]    = max(d_stim(pulse_window));
        pulse_start_idx(c1)         = window_start+pulse_start_idx_rel-1;
        
        window_start                = pulse_start_idx(c1)+offset;
        pulse_stop                  = window_start+offset;

        c1 = c1+1;
    end
    
%     plot(d_stim(1:2000))

end