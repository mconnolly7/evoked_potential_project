
hold on
input_cathode   = 0:1:3;
input_anode     = 0:1:3;
input_amp       = 1:.05:5;
input_pulse     = 20:1:120; 
input_space   	= combvec(input_cathode, input_anode, input_amp, input_pulse)';

n_samples       = 100;
n_burn_in       = 15;
acq_function    = 'UCB';
acq_params      = .0;
setpoint        = [];

%%
ep1_model       = model_ep_emg_objective_4D(0);


%%
[X_samples, Y_samples, X_optimal_est, Y_optimal_est, Y_ground_truth] =  ...
    bayes_opt_on_model(ep1_model, input_space, n_samples, n_burn_in, acq_function, acq_params, setpoint);


plot(Y_ground_truth)