function area = dbs_objective(x_in, dbs_input, mu_stn, sigma_stn, scale_stn, mu_ic, sigma_ic, scale_ic, reference)
    
k               = 2;

for c2 = 1:size(dbs_input,1)
    current         = x_in(c2);
    
    stn_mult        = mvnpdf(dbs_input(c2,:),mu_stn,sigma_stn) * scale_stn; %+ normrnd(0,0.001);
    ic_mult         = mvnpdf(dbs_input(c2,:),mu_ic,sigma_ic) * scale_ic*.7; %+ normrnd(0,0.001);
    
    stn_ep(c2)   = stn_mult ./ (1+exp(-k * current + 4));
    ic_ep(c2)    = ic_mult ./ (1+exp(-k * current + 4));
    
end

costs           = [sum(stn_ep), sum(ic_ep), sum(x_in/10,2)];
% reference   = [0 6.6 16];

stn_cost        = costs(:,1) - reference(1);
ic_cost         = reference(2) - costs(:,2);
current_cost    = reference(3) - costs(:,3);
area            = stn_cost .* ic_cost .* current_cost;
end

