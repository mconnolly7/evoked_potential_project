close all
% clear
clc

figure_1        = figure('Position', [-782 161 542 785]);
x1              = linspace(0,1,4);
x2              = linspace(0,1,8); 

% x1              = linspace(0,1,3);
% x2              = linspace(0,1,4); 

% x1              = .5;
% x2              = linspace(0,1,4); 

dbs_input       = combvec(x1, x2)';

mu_ic           = [.4 .55];
sigma_ic        = [.4 -0.03; 
                -.03 2]/10;
scale_ic        = 1/mvnpdf(mu_ic,mu_ic,sigma_ic);
       

x1              = -3:.02:3;
x2              = -3:.02:4;

[X1,X2]         = meshgrid(x1,x2);
X               = [X1(:) X2(:)];

y_ic            = mvnpdf(X,mu_ic,sigma_ic);
y_ic            = reshape(y_ic,length(x2),length(x1));

mu_stn          = [.75 .4];
sigma_stn       = [.5 0.00; 
                0.00 .05];
scale_stn       = 1/mvnpdf(mu_stn,mu_stn,sigma_stn);

y_stn          	= mvnpdf(X,mu_stn,sigma_stn);
y_stn          	= reshape(y_stn,length(x2),length(x1));

reference       = [0 6.6 16];

%
% Plot
%
ax1                         = axes;
[~, m]                      = contourf(x1,x2,y_ic*scale_ic, 5, 'LineWidth', 5);
ax1.Colormap                = inferno;
drawnow
hFills                      = m.FacePrims;  % array of TriangleStrip objects
[hFills.ColorType]          = deal('truecoloralpha');  % default = 'truecolor'

hFills(1).ColorData(4)      = 0;   % default=255

for idx = 2 : numel(hFills)
   hFills(idx).ColorData(4) = 200;   
end
drawnow

ax2                         = axes;
[c, m]                      = contourf(x1,x2,y_stn*scale_stn, 5, 'LineWidth', 5);
ax2.Colormap                = viridis;
drawnow
hFills                      = m.FacePrims;  % array of TriangleStrip objects
[hFills.ColorType]          = deal('truecoloralpha');  % default = 'truecolor'
hFills(1).ColorData(4)      = 0;   % default=255

for idx = 2 : numel(hFills)
   hFills(idx).ColorData(4) = 200;   % default=255
end
drawnow

ax3 = axes;
scatter(dbs_input(:,1), dbs_input(:,2), 1000, 'filled', 'MarkerfaceColor', [.5 0 0], 'MarkerEdgeColor', [0 0 0], 'LineWidth', 3)
drawnow

linkprop([ax1,ax2,ax3],{'XLim','YLim'});
ax1.Visible     = 'off';
ax2.Visible     = 'off';
ax3.Visible     = 'off';
ax1.Box         = 'off';
ax2.Box         = 'off';
ax3.Box         = 'off';

ax1.XTick       = [];
ax1.YTick       = [];
ax2.XTick       = [];
ax2.YTick       = [];
ax3.XTick       = [];
ax3.YTick       = [];

xlabel('x')
ylabel('y')
xlim([-.3 1.3])
ylim([-.3 1.3])

set(ax1, 'FontSize', 16)
set(gcf,'Color', 'w')

figure_2        = figure('Position', [-1377 600 560 420]);
figure_3        = figure('Position', [-1377 100 560 420]);

n_vars         	= size(dbs_input,1);
lower_bound    	= ones(1,n_vars)*0;
upper_bound     = ones(1,n_vars)*5;

%%
n_trials        = 10;
n_samples       = 100;    
n_burn_in       = 30;

bao_opt                         = gpr_model();
bao_opt.acquisition_function    = 'UCB';
eta                             = [2.5 .5 0.1];
n_min_eval                      = [1 5 10];

for hyp_idx = 1:1;size(eta,2)
    for trial = 6:n_trials

        clear objective_val input_param max_param
        for sample = 1:n_samples
            
            % Select next sample
            if sample > n_burn_in
                bao_opt.initialize_data(input_param,  objective_val, lower_bound, upper_bound);
                bao_opt.minimize(5)
                input_param(sample,:)   = bao_opt.ga_acquisition_function(eta(hyp_idx));
            else           
                input_param(sample,:)   = rand(1,n_vars) * 5;
            end

            objective_val(sample,1) = dbs_objective(input_param(sample,:), dbs_input, mu_stn, sigma_stn, scale_stn, mu_ic, sigma_ic, scale_ic, reference);

            % Get the max parameter
            if sample > n_burn_in*1000
                max_param(sample,:)                         = bao_opt.ga_find_max();
            else
                [max_val_noise(sample), max_val_idx]        = max(objective_val);
                max_param(sample,:)                         = input_param(max_val_idx,:);
            end

            set(0, 'currentfigure', figure_3); 
            plot(max_param)
            xlim([1 n_samples])
            set(0, 'currentfigure', figure_2);
                        
            if n_burn_in == n_samples
                rand_max_val(trial,sample) = dbs_objective(max_param(sample,:), dbs_input, mu_stn, sigma_stn, scale_stn, mu_ic, sigma_ic, scale_ic, reference);

                plot(rand_max_val(trial,:))  
                drawnow
            else
                BAO_max_val(hyp_idx, trial,sample) = dbs_objective(max_param(sample,:), dbs_input, mu_stn, sigma_stn, scale_stn, mu_ic, sigma_ic, scale_ic, reference);

                plot(squeeze(BAO_max_val(hyp_idx,trial,:)))  
                title(sprintf('Trial: %d, Sample: %d, Max: %.2f', trial, sample, BAO_max_val(hyp_idx, trial,sample)))

                drawnow
            end

            set(0, 'currentfigure', figure_1); 
            cla(ax3)
            hold on

            for c3 = 1:size(dbs_input,1)
                scatter(dbs_input(c3,1), dbs_input(c3,2), 1000, [max_param(sample, c3)/5 0 0],'filled', 'MarkerEdgeColor', 'k', 'LineWidth', 3, 'MarkerFaceAlpha', max_param(sample,c3)/5)
            end
        end
    end
end



%% Find optimum with GA

n_vars                      = size(dbs_input,1);
lower_bound                 = ones(1,n_vars)*0;
upper_bound                 = ones(1,n_vars)*5;

options                     = optimoptions('ga','PlotFcn', @gaplotbestf);
% options.MaxGenerations      = 500;
% options.UseParallel         = true;
PLOT                        = 0;
fun                         = @(x_in) -1*dbs_objective(x_in, dbs_input, mu_stn, sigma_stn, scale_stn, mu_ic, sigma_ic, scale_ic, reference);

[W_out_ga, f_val, exit_flag, output, population, scores]  ...
    = ga(fun,n_vars,[],[],[],[],lower_bound,upper_bound,[],options);

f_max = dbs_objective(W_out_ga, dbs_input, mu_stn, sigma_stn, scale_stn, mu_ic, sigma_ic, scale_ic, reference);

%%
set(0, 'currentfigure', figure_1); 
cla(ax3)
hold on

for sample = 1:size(dbs_input,1)
    scatter(dbs_input(sample,1), dbs_input(sample,2), 1000, [W_out_ga(sample)/5 0 0],'filled', 'MarkerEdgeColor', 'k', 'LineWidth', 3, 'MarkerFaceAlpha', W_out_ga(sample)/5)
end


%%
bao_error           = (f_max - BAO_max_val)/f_max;
bao_cumm_error      = sum(bao_error,3);
bao_final           = bao_error(:,:,end);

rand_error = (f_max - rand_max_val)/f_max;
rand_cumm_error = sum(rand_error,2);
rand_final          = rand_error(:,end);

anova1([bao_final' rand_final])

%% Evaluate random search 
%

n_trials    = 10;
n_samples   = 1000;    

figure_2    = figure('Position', [-1377 398 560 420]);

for trial = 1:n_trials
    
    % Generate random data
    x_rand          = rand(n_samples,32)*5;

    for trial = 1:n_samples
        x_in                = x_rand(trial,:);

        
        area(trial,trial) = dbs_objective(x_in, dbs_input, mu_stn, sigma_stn, scale_stn, mu_ic, sigma_ic, scale_ic);

        [max_area(trial,trial), max_area_idx]      = max(area(trial,:));
        best_param                              = x_rand(max_area_idx,:);


        set(0, 'currentfigure', figure_1); 
        cla(ax3)
        hold on

        for sample = 1:size(dbs_input,1)
            scatter(dbs_input(sample,1), dbs_input(sample,2), 1000, [best_param(sample)/5 0 0],'filled', 'MarkerEdgeColor', 'k', 'LineWidth', 3)
        end


        set(0, 'currentfigure', figure_2); 
        cla
        plot(max_area(trial,:))
        title(sprintf('Trial: %d, Sample: %d, Area: %.2f', trial, trial, max_area(trial,trial)))

    %     cla
    %     max_dom_order = max(dom_order);
    %     
    %     scatter3(costs(:,1),costs(:,2),costs(:,3), 200,'MarkerFaceAlpha', .2, 'MarkerFaceColor', [.8 .8 .8], 'MarkerEdgeColor', 'none')
    %     hold on
    %     scatter3(costs(pareto_idx,1),costs(pareto_idx,2),costs(pareto_idx,3),200, 'filled', 'MarkerFaceColor', v(2,:), 'MarkerEdgeColor', [0 0 0], 'LineWidth',3 )
    %     scatter3(costs(dom_order==max_dom_order/2,1),costs(dom_order==max_dom_order/2,2),costs(dom_order==max_dom_order/2,3),200, 'filled', 'MarkerFaceColor', v(5,:), 'MarkerEdgeColor', [0 0 0], 'LineWidth',3 )
    %     scatter3(costs(dom_order==max_dom_order/4,1),costs(dom_order==max_dom_order/4,2),costs(dom_order==max_dom_order/4,3),200, 'filled', 'MarkerFaceColor', v(8,:), 'MarkerEdgeColor', [0 0 0], 'LineWidth',3 )

        drawnow
    end

    
end







