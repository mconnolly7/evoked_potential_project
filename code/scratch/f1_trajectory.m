% Load the EP model 
load_dir                    = 'evoked_potential_project/data/modeled_data/bipolar_ring/LOK/bipolar_ring_model';
data_struct                 = load(load_dir);

ep_model                    = data_struct.joint_model_ep;
ep_model.initialize_data(ep_model.x_data, ep_model.y_data*10);
emg_model                   = data_struct.joint_model_emg;
emg_model.hyperparameters.lik = 1;
% 
load('evoked_potential_project/data/simulation_data/ehvi_model_search.mat')
evhi_data = data_struct;

% load('evoked_potential_project/data/simulation_data/rand_model_search.mat')
% rand_data = data_struct;

%%
clear precision recall f1_score
input_cathode               = 0:3;
input_anode                 = 0:3;
input_amplitudes            = .1:.25:5;
input_space                 = combvec(input_cathode, input_anode, input_amplitudes)';

% Generate ground truth pareto idx for real-time error plotting
f1              = ep_model.predict(input_space);
f2              = emg_model.predict(input_space);

% Estimate Pareto front
costs_est       = [f1 f2];
[pareto_idx_gt dom_order_gt] 	= get_pareto_2(1,-1, costs_est);

for c1 = 0:100
max_dom_order   = c1;

for c1 = 1:1%size(evhi_data,2) % per config
    data = evhi_data(c1);
    
    for c2 = 1:size(data.pareto_est, 3) % per trial
        dom_order_est           = data.dom_order_est(:,:,c2);
        
        pos_est                 = dom_order_est  <= max_dom_order;
        neg_est                 = dom_order_est  > max_dom_order;

        pos_gt                  = dom_order_gt <= max_dom_order;
        neg_gt                  = dom_order_gt > max_dom_order;
        
        pareto_true_pos         = pos_est & pos_gt;
        pareto_false_pos        = pos_est & neg_gt;
        pareto_true_neg         = neg_est & neg_gt;
        pareto_false_neg        = neg_est & pos_gt;
        
        precision               = sum(pareto_true_pos) ./ (sum(pareto_true_pos) + sum(pareto_false_pos));
        recall(:,c2)                  = sum(pareto_true_pos) ./ (sum(pareto_true_pos) + sum(pareto_false_neg));
        specificity(:,c2)             = sum(pareto_true_neg) ./ sum(neg_gt);
%         f1_score(:,c2)          = 2 * (precision .* recall) ./ (precision + recall);
        
        
    end
%     f1_score(isnan(f1_score)) = 0;

    recall_all(:,c1+1) = recall(end,:);
    specificity_all(:,c1+1) = specificity(end,:);
%     X(:,mdm+1) = f1_score(end,:);
%     hold on
%     plot(mean(f1_score,2));
end
end

% ylim([0 1])
%%

clear recall_all specificity_all

for c1 = 1:100
max_dom_order   = c1-2;

data = rand_data(1);
% data = evhi_data(1);

dom_order_est           = squeeze(data.dom_order_est(:,15,:));

pos_est                 = dom_order_est  <= max_dom_order;
neg_est                 = dom_order_est  > max_dom_order;

pos_gt                  = dom_order_gt == 0;
neg_gt                  = dom_order_gt > 0;

pareto_true_pos         = pos_est & pos_gt;
pareto_false_pos        = pos_est & neg_gt;
pareto_true_neg         = neg_est & neg_gt;
pareto_false_neg        = neg_est & pos_gt;

precision               = sum(pareto_true_pos) ./ (sum(pareto_true_pos) + sum(pareto_false_pos));
recall                  = sum(pareto_true_pos) ./ (sum(pareto_true_pos) + sum(pareto_false_neg));
specificity             = sum(pareto_true_neg) ./ sum(neg_gt);
% f1_score                = 2 * (precision .* recall) ./ (precision + recall);


%     end
%     f1_score(isnan(f1_score)) = 0;

recall_all(:,c1)        = recall;
specificity_all(:,c1)   = specificity;
%     X(:,mdm+1) = f1_score(end,:);
%     hold on
%     plot(mean(f1_score,2));

end

hold on
plot(mean(1-specificity_all), mean(recall_all), 'k')



