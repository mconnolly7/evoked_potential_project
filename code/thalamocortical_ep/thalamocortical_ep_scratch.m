% %%
clear
data_path = '/Users/mconn24/OneDrive - Emory University/projects/thalamic_dbs_effect_on_cortex/data/';


patient_struct.subject_path = [data_path 'ephys021/'];
patient_struct.subject_id = 21;
patient_struct.stim_ids = [31 22];
d = dir([patient_struct.subject_path '*part*.mat']);
patient_struct.file_paths = {d.name};
% d = dir([subject_path '*mono*.mat']);

% subject_path = [data_path 'ephys024/'];
% subject_id = 24;
% stim_ids = [8 32];
% d = dir([subject_path '*.mat']);

% subject_path = [data_path 'ephys029/'];
% subject_id = 29;
% stim_ids = [7 32];
% d = dir([subject_path '*.mat']);

% subject_path = [data_path 'ephys052/'];
% subject_id = 52;
% stim_ids = [5 15];
% d = dir([subject_path '*.mat']);

% %% No standard EP sweep
% subject_path = [data_path 'ephys058/'];
% subject_id = 24;
% stim_ids = [8 32];
%

%%
patient_struct = proc_segment_bulk_file(patient_struct);

patient_struct = proc_reference_ecog_2_14(patient_struct);

patient_struct = proc_detect_pulse(patient_struct);

patient_struct = proc_segment_pulses(patient_struct);


%% Plot strip
plot_strip(patient_struct, patient_struct.stim_ids(1))

%% Comparing monopolar contacts standard


%% Comparing monopolar contacts high amplitude


%% Plotting bipolar to confirm EP

close all
t = (1:size(patient_struct(1).ecog_matrix,2))/22;
tt = t(100:end);


f = figure('Position', [-847 338 560 420]); 

hold on
for c1 = patient_struct.stim_ids
    
    stim_idx = c1;
    patient_struct(stim_idx).stim_parameter_string
    data = squeeze(mean(patient_struct(stim_idx).ecog_matrix,3))';
    data_seg = data(100:end,16);
    data_seg = data_seg - data_seg(1);
    
    plot(tt,data_seg)
end

xlabel('Millisecond')
ylabel('EP Magnitude (µV)')
set(gca, 'FontSize', 14)

f = figure('Position', [680 2 454 1095]);
subplot(1,2,1)

for c1 = 1:26
    if c1 == 14
        subplot(1,2,2)
    end
    
    hold on
    
    
    data_seg = data(100:end,c1);
    data_seg = data_seg - data_seg(1);
    
    plot(tt,data_seg-30*c1)
    
end

subplot(1,2,1)
yticklabels([])
yticks([])
set(gca,'FontSize', 16)

subplot(1,2,2)
yticklabels([])
yticks([])
set(gca,'FontSize', 16)













