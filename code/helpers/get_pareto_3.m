function [pareto_idx, dom_order] = get_pareto_3(objective_1_dir, objective_2_dir, objective_3_dir, costs_est)

dom_order   = zeros(size(costs_est,1),1);
idx_vector  = 1:size(costs_est,1);
dom_count   = 0;

while ~isempty(costs_est)
    clear pareto_idx;
    for c1 = 1:size(costs_est,1)

        opt_1               = objective_1_dir * costs_est(c1,1) >= objective_1_dir * costs_est(:,1);
        opt_2               = objective_2_dir * costs_est(c1,2) >= objective_2_dir * costs_est(:,2);
        opt_3               = objective_3_dir * costs_est(c1,3) >= objective_3_dir * costs_est(:,3);
        pareto_opt          = opt_1 | opt_2 | opt_3;

        pareto_idx(c1)      = all(pareto_opt);     
    end
    
    if nargout == 1
       break; 
    end
    
    dom_order(idx_vector(pareto_idx))   = dom_count;
    dom_count                           = dom_count+1;
    
    costs_est(pareto_idx,:)             = [];
    idx_vector(pareto_idx)              = [];
end

if nargout == 2
   pareto_idx = dom_order == 0; 
end

end