function hyperpriors = organize_hyperpriors_2D(hyperprior_table, subject)

holdout_idx                 = hyperprior_table.subject_id ~= subject;
hyperprior_table_holdout    = hyperprior_table(holdout_idx,:);

hyperpriors.mean.mean       = mean(hyperprior_table_holdout.mean);
hyperpriors.mean.stdv       = std(hyperprior_table_holdout.mean);

hyperpriors.lik.mean        = mean(hyperprior_table_holdout.lik);
hyperpriors.lik.stdv        = std(hyperprior_table_holdout.lik);

hyperpriors.cov1.mean       = mean(hyperprior_table_holdout.cov1);
hyperpriors.cov1.stdv       = std(hyperprior_table_holdout.cov1);

hyperpriors.cov2.mean       = mean(hyperprior_table_holdout.cov2);
hyperpriors.cov2.stdv       = std(hyperprior_table_holdout.cov2);

hyperpriors.cov3.mean       = mean(hyperprior_table_holdout.cov3);
hyperpriors.cov3.stdv       = std(hyperprior_table_holdout.cov3);

end

