% close all; clear

%% Load the EP data 
load_dir                    = 'evoked_potential_project/data/processed_data/EP002/';
ep_struct                   = p0_load_ep_struct(load_dir);

%% Construct the emg and ep objective models 
ep_struct                   = p6_calculate_emg_spike(ep_struct, 9);
emg_model                   = m7_construct_emg_objective(ep_struct, [], 1);
ep_model                    = m7_construct_ep_objective(ep_struct);

input_regions               = 1:10;
input_amplitudes            = .1:.1:5;
input_space                 = combvec(input_regions, input_amplitudes)';

%% Run a simulation against the model
clear ep_struct

clc
objective_1_init            = @m7_construct_ep_objective;
objective_2_init            = @m7_construct_emg_objective;

n_trials        = 40;
USE_HYPERPRIOR  = 0;
USE_SAFE        = 1;
y_bounds        = [-inf 0; inf 100];
threshold       = 1;
beta            = 0;
eta             = [1];
alpha           = 1;
PLOT            = 1;
% safe_seed       = [1 2 3 4 5 6 7 8 9 10; 5 1 5 1 5 1 5 1 5 1]';
safe_seed       = [1  3  5  7  9 ; 5 1 5 1 5]';
n_samples       = 50;

for c2 = 1:size(eta, 2)
    for c1 = 11:n_trials

        [X_opt(:,:,c1), Y_opt(:,c1), X_sample(:,:,c1), Y_sample(:,c1)] ...
            = safe_opt_on_model(ep_model, emg_model, objective_1_init, objective_2_init, ...
            threshold, beta, eta(c2), alpha, PLOT, input_space, y_bounds, safe_seed, ...
            n_samples, USE_HYPERPRIOR, USE_SAFE);

        save_dir = 'evoked_potential_project/data/simulation_data/safe_opt_experiments/';
        save_name = sprintf('EP002_beta_%d_eta_%.3f_ns_%d_sweep_%d.mat', beta, eta(c2), n_samples, size(safe_seed,1));
        save_path = [save_dir save_name];
        save(save_path)
    end
    
end
%%
% X_opt_end = squeeze(X_opt(end,:,:));
% scatter(input_space(:,1), input_space(:,2))

[y_safety,~,~,fs_safety]    = emg_model.predict(input_space);
safe_idx                    = y_safety + fs_safety*0 < 1;
scatter(input_space(safe_idx,1), input_space(safe_idx,2), 150, 'Marker', 'o', 'MarkerEdgeColor', ones(1,3)*.8)

hold on
scatter(X_opt_end(1,:), X_opt_end(2,:), 300, 'Marker', 'x', 'LineWidth', 2)
xlabel('Contact')
ylabel('Amplitude (mA)')
set(gca,'FontSize', 16)