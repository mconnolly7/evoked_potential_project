clear; close all; clc;
% load('evoked_potential_project/data/simulation_data/safe_opt_experiments/EP010/eta_sweep/EP010_beta_0_eta_1.000_ns_100_sweep_5.mat')
% rep_trial = 9; 

load('evoked_potential_project/data/simulation_data/safe_opt_experiments/EP002/eta_sweep/EP002_beta_0_eta_1.000_ns_50_sweep_5.mat')
% load('evoked_potential_project/data/simulation_data/safe_opt_experiments/EP002/eta_sweep/EP002_beta_0_eta_1.000_ns_50_sweep_5.mat')
% [1 3 4 5 7 9 10 13 19 25 27 29 36 39 40]
for rep_trial = [ 7 40]

    
    figure;

    emg_gt                  = emg_model.predict(input_space);
    safe_idx                = emg_gt < 1;

    safe_input              = input_space(safe_idx,:);
    cep_safe                = ep_model.predict(safe_input);
    [max_cep, max_cep_idx]  = max(cep_safe);
    
    contacts                = 1:10;
    amplitudes              = 0.1:.1:5;
    params                  = combvec(contacts, amplitudes)';
    
    t1                      = reshape(params(:,1), 10, 50);
    t2                      = reshape(params(:,2), 10, 50);
    
    ep_model.t = params;
    ep_model.t1 = t1;
    ep_model.t2 = t2;
    
    emg_model.t = params;
    emg_model.t1 = t1;
    emg_model.t2 = t2;
    
    subplot(3,2,1)
    ep_model.plot_mean();
    plot3(safe_input(max_cep_idx,1), safe_input(max_cep_idx,2), max_cep, 'rx', 'MarkerSize', 15, 'LineWidth', 10)
    xlim([1 10])
    ylim([.1 5])
    xlabel('Contact configuration')
    ylabel('Amplitude (mA)')
    zlabel('CEP Magnitude (�V)')
    title('CEP Ground Truth Model')
    set(gca,'FontSize', 16)
    
    subplot(3,2,3)
    emg_model.plot_mean;
    plot3(safe_input(max_cep_idx,1), safe_input(max_cep_idx,2), ...
        emg_model.predict(safe_input(max_cep_idx,:)), 'rx', 'MarkerSize', 15, 'LineWidth', 10)
    title('MEP Ground Truth Model')
    xlabel('Contact configuration')
    ylabel('Amplitude (mA)')
    zlabel('MEP Risk (a.u.)')
    xlim([1 10])
    ylim([.1 5])
    set(gca,'FontSize', 16)


    idx         = 6:size(X_sample,1);

    subplot(3,2,2)
    hold on
    plot(X_sample(:,1,rep_trial), ':', 'color', .5*ones(1,3), 'LineWidth', 2)
    plot(idx, X_opt(idx,1,rep_trial), 'k-', 'LineWidth', 3)
    patch([0 6 6 0], [0 0 10 10], .7*ones(1,3), 'facealpha', .2, 'EdgeColor', 'none')

    legend({'Sampled Parameter','Estimated Optimal'});
    ylabel('Contact configuration')
    title('Representative Trial')
    set(gca,'FontSize', 16)
    xlim([1 50])
    ylim([.8 10])

    subplot(3,2,4)
    hold on
    plot(X_sample(:,2,rep_trial), ':', 'color', .5*ones(1,3), 'LineWidth', 2)
    plot(idx, X_opt(idx,2,rep_trial), 'k-', 'LineWidth', 3)
    patch([0 6 6 0], [0 0 10 10], .7*ones(1,3), 'facealpha', .2, 'EdgeColor', 'none')
    xlabel('Sample')
    ylabel('Amplitude (mA)')

    set(gca,'FontSize', 16)
    xlim([1 50])
    ylim([.8 5])
    
    subplot(3,2,6)
    y_opt_est = ep_model.predict(squeeze(X_opt(idx,:,rep_trial)))
    hold on
    
    plot(idx, y_opt_est, ':', 'color', .5*ones(1,3), 'LineWidth', 2)
%     plot(idx, X_opt(idx,2,rep_trial), 'k-', 'LineWidth', 3)
    patch([0 6 6 0], [0 0 15 15], .7*ones(1,3), 'facealpha', .2, 'EdgeColor', 'none')
    xlabel('Sample')
    ylabel('CEP Magnitude (�V)')

    set(gca,'FontSize', 16)
    xlim([1 50])
    ylim([0 15])

end
%%
%%%%%
subplot(2,3,2)
tt = 6:50;
xx = squeeze(X_opt(tt,1,:)); 

hold on
plot(tt, xx, 'color', .7*ones(1,3))
plot(tt,prctile(xx,50,2), 'color', [1 0 0], 'LineWidth', 3)
plot(tt,prctile(xx,25,2), 'color', .0*ones(1,3), 'LineWidth', 3, 'LineStyle', '--')
plot(tt,prctile(xx,75,2), 'color', .0*ones(1,3), 'LineWidth', 3, 'LineStyle', '--')
patch([0 6 6 0], [0 0 10 10], .7*ones(1,3), 'facealpha', .2, 'EdgeColor', 'none')

ylim([.8 10])
xlim([1 50])
xticklabels([]);
ylabel('Contact configuration')
set(gca,'FontSize',16)
title('Search Trajectory');


%%%%%
subplot(2,3,5)
xx = squeeze(X_opt(tt,2,:));

hold on
plot(tt, xx, 'color', .7*ones(1,3))
plot(tt,prctile(xx,50,2), 'color', [1 0 0], 'LineWidth', 3)
plot(tt,prctile(xx,25,2), 'color', .0*ones(1,3), 'LineWidth', 3, 'LineStyle', '--')
plot(tt,prctile(xx,75,2), 'color', .0*ones(1,3), 'LineWidth', 3, 'LineStyle', '--')
patch([0 6 6 0], [0 0 5 5], .7*ones(1,3), 'facealpha', .2, 'EdgeColor', 'none')

xlim([1 50])
ylim([0 5])
xlabel('Samples')
ylabel('Amplitude (mA)')
set(gca,'FontSize',16)


%%%%%
subplot(2,3,1)
hold on
input_subspace                  = combvec(1:10, .1:.2:5)';
cmap                            = winter(size(input_subspace,1));

ep_gt                           = ep_model.predict(input_subspace);
[ep_gt_sort, ep_gt_sort_idx]   	= sort(ep_gt);

emg_gt                          = emg_model.predict(input_subspace);
[emg_gt_sort, emg_gt_sort_idx] 	= sort(emg_gt);

for c1 = 1:size(input_subspace,1)
    idx = ep_gt_sort_idx(c1);
    scatter(input_subspace(idx,1), input_subspace(idx,2), 200, 'MarkerFaceColor', cmap(c1,:), 'MarkerEdgeColor', cmap(c1,:), 'MarkerFaceAlpha', .5 )
end

colormap('winter')
c = colorbar('Ticks', [0 1], 'TickLabels',round([min(ep_gt) max(ep_gt)]));
c.Label.String = 'EP magnitude (mV)';

hold on
for c1 = 1:10
    region_idx              = input_subspace(:,1) == c1;
    safe_idx                = emg_gt < 1;
    
    region_space            = input_subspace(region_idx & safe_idx,:);
    region_ep               = ep_gt(region_idx & safe_idx);
    [~, region_ep_max_idx]  = max(region_ep);
    opt_space(c1,:)         = region_space(region_ep_max_idx,:);
end
scatter(opt_space(:,1), opt_space(:,2), 250, 'o', 'MarkerEdgeColor', 'r', 'LineWidth', 3)

[~, opt_unsafe_idx]         = max(ep_gt);
scatter(input_subspace(opt_unsafe_idx,1), input_subspace(opt_unsafe_idx,2), 300, 'x', 'LineWidth', 3, 'MarkerEdgeColor', 'k')

% safe_idx                = emg_gt < 1;
% region_safe             = input_subspace(safe_idx,:);
% ep_safe                 = ep_gt(safe_idx);
% [~, ep_safe_sort_idx]   = sort(ep_safe,'descend');
% 
% region_sort             = region_safe(ep_safe_sort_idx,:);
% 
% scatter(region_sort(1:10,1), region_sort(1:10,2), 250, 'o', 'MarkerEdgeColor', 'r', 'LineWidth', 3)


xlabel('Contact configuration')
ylabel('Amplitude (mA)')
zlabel('EP Magnitude')
set(gca,'FontSize', 16)
title('EP Model Ground Truth')
view(0,90)
xlim([.5 10.5])
ylim([0 5])

%%%%%
subplot(2,3,4)
hold on

for c1 = 1:size(input_subspace,1)
    idx = emg_gt_sort_idx(c1);
    scatter(input_subspace(idx,1), input_subspace(idx,2), 200, 'MarkerFaceColor', cmap(c1,:), 'MarkerEdgeColor',cmap(c1,:), 'MarkerFaceAlpha', .5 )
end
scatter(opt_space(:,1), opt_space(:,2), 250, 'o', 'MarkerEdgeColor', 'r', 'LineWidth', 3)

colormap('winter')
c = colorbar('Ticks', [0 1], 'TickLabels',round([min(emg_gt) max(emg_gt)]));
c.Label.String = 'EMG events';
set(c,'FontSize', 16)

xlabel('Contact configuration')
ylabel('Amplitude (mA)')
zlabel('EMG events')
title('EMG Model Ground Truth')
set(gca,'FontSize', 16)
view(0,90)
xlim([.5 10.5])
ylim([0 5])

%%%%%
idx         = 6:size(X_sample,1);

subplot(2,3,3)
hold on
plot(X_sample(:,1,rep_trial), ':', 'color', .5*ones(1,3), 'LineWidth', 2)
plot(idx, X_opt(idx,1,rep_trial), 'k-', 'LineWidth', 3)
patch([0 6 6 0], [0 0 10 10], .7*ones(1,3), 'facealpha', .2, 'EdgeColor', 'none')

legend({'Sampled Parameter','Estimated Optimal'});
ylabel('Contact configuration')
title('Representative Trial')
set(gca,'FontSize', 16)
xlim([1 50])
ylim([.8 10])

%%%%%%
subplot(2,3,6)
hold on
plot(X_sample(:,2,rep_trial), ':', 'color', .5*ones(1,3), 'LineWidth', 2)
plot(idx, X_opt(idx,2,rep_trial), 'k-', 'LineWidth', 3)
patch([0 6 6 0], [0 0 5 5], .7*ones(1,3), 'facealpha', .2, 'EdgeColor', 'none')

xlabel('Samples')
ylabel('Amplitude (mA)')
set(gca,'FontSize', 16)
xlim([1 50])
ylim([0 5])

%%%%%
% subplot(2,2,5)
% hold on
% 
% ep_gt       = ep_model.predict(input_space);
% emg_gt      = emg_model.predict(input_space);
% safe_idx    = emg_gt < 1;
% Y_safe_max  = max(ep_gt(safe_idx));
%     
% for c1 = 1:50
%     xx(:,c1) = abs(ep_model.predict(X_opt(tt,:,c1)) - Y_safe_max);
% end
% plot(tt, xx, 'color', .7*ones(1,3))
% plot(tt,prctile(xx,50,2), 'color', [1 0 0], 'LineWidth', 3)
% plot(tt,prctile(xx,25,2), 'color', .0*ones(1,3), 'LineWidth', 3, 'LineStyle', '--')
% plot(tt,prctile(xx,75,2), 'color', .0*ones(1,3), 'LineWidth', 3, 'LineStyle', '--')
% patch([0 6 6 0], [0 0 15 15], .7*ones(1,3))

% ylim([0 10])
% ylabel('Error from safe-max')
% subplot(2,2,6)
% [y_safety,~,~,fs_safety]    = emg_model.predict(input_space);
% safe_idx                    = y_safety + fs_safety*0 < 1;
% X_opt_end                   = squeeze(X_opt(end,:,:));
% scatter(input_space(safe_idx,1), input_space(safe_idx,2), 150, 'Marker', 'o', 'MarkerEdgeColor', ones(1,3)*.8)
% hold on
% scatter(X_opt_end(1,:), X_opt_end(2,:), 300, 'Marker', 'x', 'LineWidth', 2)
% xlabel('Contact configuration')
% ylabel('Amplitude (mA)')
% legend({'Safe Parameters', 'Estimated Optimal'}, 'location', 'Northwest' )
% set(gca,'FontSize', 16)


