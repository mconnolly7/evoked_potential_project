function example_visualize_monopolar_ring 
close all
data_table_path = 'evoked_potential_project/data/processed_data/data_table';
load(data_table_path);

% all subject_id          = {'EP002','EP007','EP008','EP010','EP014','EP015','EP017'};
% all offsets             = [.0005 .001 .0015 .002 .0025];

subject_id          = {'EP002'};
offsets             = [.002];
polarity            = 'monopolar';
electrode           = 'ring';

downsample_ratio    = 2;
frequency           = 10;
pulse_width_a       = 60;
signal_type         = 'ecog';

for c3 = 1:size(offsets,2)
    for c1 = 1:size(subject_id,2)
        valid_idx           = strcmp(data_table.subject_id, subject_id{c1});
        valid_idx           = valid_idx & strcmp(data_table.polarity, polarity);
        valid_idx           = valid_idx & strcmp(data_table.electrode, electrode);
        valid_idx           = valid_idx & data_table.pulse_width_a == pulse_width_a;
        valid_idx           = valid_idx & data_table.frequency == frequency;

        valid_table         = data_table(valid_idx,:);

        f1                  = figure( 'Position',  [0, 0, 1900, 1000]);
        z_limits(1,:)       = visualize_monopolar_ring(valid_table, 1, offsets(c3), downsample_ratio, signal_type);
        
        for c2 = 1:12
            subplot(3,4, c2)
            switch signal_type
                case 'ecog'
                    caxis([min(z_limits) max(z_limits)])
                case 'emg'
                    ylim([min(z_limits) max(z_limits)])
            end           
        end
        
        switch signal_type
            case 'ecog'
                subplot(3,4,12)
               
                c = colorbar; drawnow

                c.Position          = [.93 .11 .01 .51];
                c.Label.String      = 'Microvolts (�V)';
                c.Label.FontSize    = 14;
                
                subplot(3,4,1)
                xlabel('Pos. <- ECOG Channel -> Ant.')

                print('ecog_sweep.png', '-dpng', '-r200')
                
            case 'emg'
                ylim([min(z_limits) max(z_limits)])
                
                print('emg_sweep.png', '-dpng', '-r200')
        end
    end
end

end

function z_limits = visualize_monopolar_ring(valid_table, strip_coordinate, offset, downsample_ratio, signal_type)

discretization      = 1.6;
ep_struct           = proc_load_ep_struct(valid_table.data_path);

ep_struct           = proc_reference_ecog_2_14(ep_struct);
ep_struct           = proc_detect_pulse(ep_struct);
ep_struct           = proc_segment_pulses(ep_struct);

ep_struct           = proc_filter_ep_2D(ep_struct,offset, downsample_ratio);

subplot_idx         = [9 5 1 10 6 2 11 7 3 12 8 4];
amplitude_idx      	= [1 3 5 1 3 5 1 3 5 1 3 5];
contact_idx         = [0 0 0 1 1 1 2 2 2 3 3 3];

for c1 = 1:12
    subplot(3,4,subplot_idx(c1))
    plot_idx        = valid_table.monopolar_coordinates == contact_idx(c1) ...
        & valid_table.amplitude == amplitude_idx(c1);

    switch signal_type
        case 'ecog'
            ep_struct(plot_idx).ep_model_2D(strip_coordinate).minimize(1)
            ep_struct(plot_idx).ep_model_2D(strip_coordinate).plot_mean
            [~, y_max(c1), ~, y_min(c1)] = ep_struct(plot_idx).ep_model_2D(strip_coordinate).discrete_extrema(discretization);
            tt = ep_struct(1).ep_model_2D(strip_coordinate).x_data(:,2);

            ylim([min(tt), max(tt)])
            xlim([1,13])
            xlabel('ECOG Channel')
            ylabel('Milliseconds (ms)')
            view(90,-90)
           	set(gca, 'FontSize', 14)
            colormap('jet')
            
        case 'emg'
            emg4        = squeeze(ep_struct(plot_idx).emg_matrix(4,200:end,:));
            emg4        = emg4 - mean(emg4);
            emg_t       = (1:size(emg4,1))/22 - ep_struct(plot_idx).emg_pulse_offset_time(4)*1000;

            plot(emg_t,emg4, 'k');
            y_max(c1)   = max(max(emg4));
            y_min(c1)   = min(min(emg4));

            xlim([min(emg_t), max(emg_t)])
            
            xlabel('Milliseconds (ms)')
            ylabel('Microvolts (�V)')
            set(gca, 'FontSize', 14)
            box off
    end
    
end



z_limits = [y_min y_max];
end