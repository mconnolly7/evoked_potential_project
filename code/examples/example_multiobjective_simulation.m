clc
% Select subjects
subject_ids                 = [2 5 7 10]; 
subject_ids                 = [2]; 

% Set experiment dimension
dimension                   = 2;

% EP model path
ep_model_path               = sprintf('evoked_potential_project/data/subject_models/%dD/', dimension);

% Load the hyperprior data
hyperprior_data_path        = sprintf('evoked_potential_project/data/meta_modeling_data/hyperprior_data_%dD.xlsx', dimension);
hyperprior_table            = readtable(hyperprior_data_path);

% Create hyperprior settings
hyperprior_ep1_idx          = strcmp(hyperprior_table.objective,'EP1');
hyperprior_emg_idx          = strcmp(hyperprior_table.objective,'EMG');

hyperprior_ep1_table        = hyperprior_table(hyperprior_ep1_idx,:);
hyperprior_emg_table        = hyperprior_table(hyperprior_emg_idx,:);

% Construct the input space
input_cathode               = 0:3;
input_anode                 = 0:3;
input_amplitudes            = .1:.1:5;
input_pulse_width           = 20:20:120;

switch dimension
    case 2
        input_space                 = combvec(input_cathode, input_amplitudes)';
        objective_1_init            = @model_objective_2D;
        objective_2_init            = @model_objective_2D;
    case 3
        input_space                 = combvec(input_cathode, input_anode, input_amplitudes)';
        objective_1_init            = @model_objective_3D;
        objective_2_init            = @model_objective_3D;
    case 4
        input_space                 = combvec(input_cathode, input_anode, input_amplitudes, input_pulse_width)';      
        objective_1_init            = @model_objective_4D;
        objective_2_init            = @model_objective_4D;
end

optimization_config.input_space     = input_space;

% Set up objectives
optimization_config.objective_1_dir         = 1;
optimization_config.objective_2_dir       	= -1;

%%%%%%%
% Set up algorithm configuration
%%%%%%%

% Grid
%   search_parameters(1) = grid size 

% Grid
algorithm_config_list(1).search_strategy         = 'grid';
algorithm_config_list(1).search_parameters       = 10;
algorithm_config_list(1).model_function          = {[], []};
algorithm_config_list(1).hyperprior_param        = [];
algorithm_config_list(1).name                    = 'grid';

% Grid + Model
algorithm_config_list(2).search_strategy         = 'grid';
algorithm_config_list(2).search_parameters       = 10;
algorithm_config_list(2).model_function          = {objective_1_init objective_2_init};
algorithm_config_list(2).hyperprior_param        = [];
algorithm_config_list(2).name                    = 'grid_model';

% Grid + Model + Hyperprior
algorithm_config_list(3).search_strategy         = 'grid';
algorithm_config_list(3).search_parameters       = 10;
algorithm_config_list(3).model_function          = {objective_1_init objective_2_init};
algorithm_config_list(3).hyperprior_param        = 1;
algorithm_config_list(3).name                    = 'grid_model_hyperprior';

% Random
%   search_parameters not used 

% Random
algorithm_config_list(4).search_strategy         = 'random';
algorithm_config_list(4).search_parameters       = [];
algorithm_config_list(4).model_function          = {[], []};
algorithm_config_list(4).hyperprior_param        = [];
algorithm_config_list(4).name                    = 'random';

% Random + Model
algorithm_config_list(5).search_strategy         = 'random';
algorithm_config_list(5).search_parameters       = [];
algorithm_config_list(5).model_function          = {objective_1_init objective_2_init};
algorithm_config_list(5).hyperprior_param        = [];
algorithm_config_list(5).name                    = 'random_model';

% Random + Model + Hyperprior
algorithm_config_list(6).search_strategy     	= 'random';
algorithm_config_list(6).search_parameters   	= [];
algorithm_config_list(6).model_function          = {objective_1_init objective_2_init};
algorithm_config_list(6).hyperprior_param        = 1;
algorithm_config_list(6).name                    = 'random_model_hyperprior';

% Evolution
%   search_parameters(1) = generation size 

% Evolution + Model
algorithm_config_list(7).search_strategy         = 'evolution';
algorithm_config_list(7).search_parameters       = 20;
algorithm_config_list(7).model_function          = {objective_1_init objective_2_init};
algorithm_config_list(7).hyperprior_param        = [];
algorithm_config_list(7).name                    = 'evolution_model';

% Evolution + Model + Hyperprior
algorithm_config_list(8).search_strategy     	= 'evolution';
algorithm_config_list(8).search_parameters       = 20;
algorithm_config_list(8).model_function          = {objective_1_init objective_2_init};
algorithm_config_list(8).hyperprior_param        = 1;
algorithm_config_list(8).name                    = 'evolution_model_hyperprior';

% EHVI
%   search_parameters(1) = burn in 
%   search_parameters(2) = EHVI uncertainty multiplier

% EHVI + Model
algorithm_config_list(9).search_strategy         = 'ehvi';
algorithm_config_list(9).search_parameters       = [10 100];
algorithm_config_list(9).model_function          = {objective_1_init objective_2_init};
algorithm_config_list(9).hyperprior_param        = [];
algorithm_config_list(9).name                    = 'ehvi_model';

% EHVI + Model + Hyperprior
algorithm_config_list(10).search_strategy        = 'ehvi';
algorithm_config_list(10).search_parameters      = [10 100];
algorithm_config_list(10).model_function         = {objective_1_init objective_2_init};
algorithm_config_list(10).hyperprior_param       = 1;
algorithm_config_list(10).name                   = 'ehvi_model_hyperprior';

% Set up simulation
optimization_config.n_samples             	= 100;
optimization_config.n_trials               	= 30;

PLOT                                        = 0;

rng(0)

for c3 = 1:size(subject_ids,2)
    
    subject_model_path          = sprintf('%sEP%03d_model',ep_model_path, subject_ids(c3));
    model_struct                = load(subject_model_path);
    ep1_model                   = model_struct.ep1_model;
    emg_model                   = model_struct.emg_model;
    
    
    [y_mean, y_fs]              = ep1_model.predict(input_space);
    cnr_cep                   	= (max(y_mean) - min(y_mean))/mean(y_fs);
    
    [y_mean, y_fs]              = emg_model.predict(input_space);
    cnr_mep                   	= (max(y_mean) - min(y_mean))/mean(y_fs);
    
%     ep1_model.hyperparameters.cov'
%     emg_model.hyperparameters.cov'
%     figure
%     subplot(1,2,1)
%     plot_bipolar(ep1_model)
%     zlabel('cEP Magnitude')
%     
%     subplot(1,2,2)
%     plot_bipolar(emg_model)
%     zlabel('mEP Magnitude')
%     
%     
    ep1_model.noise_sigma     	= cnr_cep/8;
    emg_model.noise_sigma    	= cnr_mep/8;
    
    hyperprior_ep1           	= organize_hyperpriors(hyperprior_ep1_table, subject_ids(c3), dimension);
    hyperprior_emg            	= organize_hyperpriors(hyperprior_emg_table, subject_ids(c3), dimension);

    optimization_config.objective_1        	= ep1_model;
    optimization_config.objective_2        	= emg_model;
    
    for c2 = 9; size(algorithm_config_list,2);
        algorithm_config                    = algorithm_config_list(c2);
        algorithm_config.hyperprior_data  	= {hyperprior_ep1 hyperprior_emg};
        algorithm_name                      = algorithm_config.name;
        
        parfor c1 = 1:optimization_config.n_trials         

            fprintf('Subject: %d, Configuration: %d, Trial: %d) ', c3, c2, c1)
            
            tic
            optimization_results(c1) = multi_objective_on_model(optimization_config, algorithm_config, PLOT);
            fprintf('\n')
            toc

        end

        save_path = sprintf('%s_%dD_EP%03d', algorithm_name, dimension, subject_ids(c3));
        save(save_path, 'optimization_config', 'algorithm_config', 'optimization_results');
        beep
    end
end


