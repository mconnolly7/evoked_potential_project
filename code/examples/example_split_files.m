%% EP002
subject_id  = {'EP002', 'EP002'};
file_dir    = '/Users/mconn24/Box Sync/papers/2019_07_01_paper_evoked_potential_optimization/data/';
file_paths  = {[file_dir 'EP002/f19_RT4D-5.501F0006_R_STN_evoked_part2_preprocessed.mat']
    [file_dir 'EP002/f18_RT4D-5.501F0005_R_STN_evoked_part1_preprocessed.mat']};

% EP007
subject_id  = [subject_id, 'EP007', 'EP007', 'EP007'];
file_dir    = '/Users/mconn24/Box Sync/EP_data_for_Mark/ephys007/';
file_paths  = [file_paths
    [file_dir 'f13_RT2D-1.010F0005_R_STN_EP_part1_preprocessed.mat']
    [file_dir 'f14_RT2D-1.010F0006_R_STN_EP_part2_preprocessed.mat']
    [file_dir 'f15_LT1D-1.024F0004_R_STN_EP_part3_preprocessed.mat']];

% EP008
subject_id  = [subject_id, 'EP008', 'EP008', 'EP008'];
file_dir    = '/Users/mconn24/Box Sync/EP_data_for_Mark/ephys008/';
file_paths  = [file_paths
    [file_dir 'f7_LT2D-3.038F0001_EPpart1_preprocessed.mat']
    [file_dir 'f8_LT2D-3.038F0002_EPpart2_preprocessed.mat']
    [file_dir 'f9_LT2D-3.038F0003_EPpart3_preprocessed.mat']];

% EP010
subject_id  = [subject_id, 'EP010', 'EP010'];
file_dir    = '/Users/mconn24/Box Sync/papers/2019_07_01_paper_evoked_potential_optimization/data/';
file_paths  = [file_paths
    [file_dir 'EP010/f12_RT1D-2.981F0002_EPs_part1_preprocessed.mat']
    [file_dir 'EP010/f13_RT1D-2.981F0003_EPs_part2_preprocessed.mat']];

% EP011
subject_id  = [subject_id, 'EP011', 'EP011', 'EP011'];
file_dir    = '/Users/mconn24/Box Sync/EP_data_for_Mark/ephys011/';
file_paths  = [file_paths
    [file_dir 'f9_RT1D-6.004F0002_EP_part1_preprocessed.mat']
    [file_dir 'f10_RT1D-6.004F0003_EP_part2_preprocessed.mat']
    [file_dir 'f11_RT1D-6.004F0004_EP_part3_preprocessed.mat']];

% EPO14
subject_id  = [subject_id, 'EP014', 'EP014', 'EP014'];
file_dir    = '/Users/mconn24/Box Sync/papers/2019_07_01_paper_evoked_potential_optimization/data/';
file_paths  = [file_paths
    [file_dir 'EP014/f13_LT2D0.024F0001_EP_part1_preprocessed.mat']     
    [file_dir 'EP014/f14_LT2D0.024F0002_EP_part2_preprocessed.mat']
    [file_dir 'EP014/f15_LT2D0.024F0003_EP_part3_preprocessed.mat']];

% EPO15
subject_id  = [subject_id, 'EP015', 'EP015', 'EP015'];
file_dir    = '/Users/mconn24/Box Sync/papers/2019_07_01_paper_evoked_potential_optimization/data/';
file_paths  = [file_paths
    [file_dir 'EP015/f19_LT4D-1.535F0001_EP_part1_preprocessed.mat']
	[file_dir 'EP015/f20_LT4D-1.535F0002_EP_part2_preprocessed.mat']
    [file_dir 'EP015/f21_LT4D-1.535F0003_EP_part3_preprocessed.mat']];
        
% EPO17
subject_id  = [subject_id, 'EP017', 'EP017', 'EP017'];
file_dir    = '/Users/mconn24/Box Sync/papers/2019_07_01_paper_evoked_potential_optimization/data/';
file_paths  = [file_paths
    [file_dir 'EP017/f19_RT2D-3.506F0005_EP_part1_preprocessed.mat']
    [file_dir 'EP017/f20_RT2D-3.506F0006_EP_part2_preprocessed.mat']
    [file_dir 'EP017/f21_RT2D-3.506F0007_EP_part3_preprocessed.mat']];

%        
ep_struct                       = [];
for c1 = 1:size(file_paths,1)
    c1
    ep_struct                   = [ep_struct proc_segment_bulk_file(file_paths{c1}, subject_id{c1})];
end

%%
save_dir                    = 'evoked_potential_project/data/processed_data/';
proc_save_ep_struct(ep_struct, save_dir);

%%
data_table  = proc_generate_data_table(ep_struct,save_dir);

%%
data_table  = proc_process_stim_parameter_string(data_table);








