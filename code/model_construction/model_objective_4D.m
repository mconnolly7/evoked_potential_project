function objective_model = model_objective_4D(x_data, y_data, hyperpriors, hyperprior_config)

objective_model     = gp_object();
objective_model.initialize_data(x_data, y_data)

if ~isempty(hyperpriors) && ~isempty(hyperprior_config) && ~isnan(hyperprior_config)
    
    cov1_mean      	= hyperpriors.cov1.mean;
    cov1_stdv       = hyperpriors.cov1.stdv;
    cov1            = {@priorGauss,cov1_mean,cov1_stdv * hyperprior_config}; % Gaussian prior
    
    cov2_mean       = hyperpriors.cov2.mean;
    cov2_stdv       = hyperpriors.cov2.stdv;
    cov2           	= {@priorGauss,cov2_mean,cov2_stdv * hyperprior_config}; % Gaussian prior
    
    cov3_mean       = hyperpriors.cov3.mean;
    cov3_stdv       = hyperpriors.cov3.stdv;
    cov3            = {@priorGauss,cov3_mean,cov3_stdv * hyperprior_config}; % Gaussian prior
    
    cov4_mean       = hyperpriors.cov4.mean;
    cov4_stdv       = hyperpriors.cov4.stdv;
    cov4            = {@priorGauss,cov4_mean,cov4_stdv * hyperprior_config}; % Gaussian prior
    
    cov5_mean       = hyperpriors.cov5.mean;
    cov5_stdv       = hyperpriors.cov5.stdv;
    cov5            = {@priorGauss,cov5_mean,cov5_stdv * hyperprior_config}; % Gaussian prior
    
    mean1_mean      = hyperpriors.mean.mean;
    mean1_stdv      = hyperpriors.mean.stdv;
    mean1           = {@priorGauss,mean1_mean,mean1_stdv * hyperprior_config}; % Gaussian prior
    
    lik_mean      	= hyperpriors.lik.mean;
    lik_stdv        = hyperpriors.lik.stdv;
    lik             = {@priorGauss,lik_mean,lik_stdv * hyperprior_config}; % Gaussian prior
    
    prior.cov       = {cov1; cov2; cov3; cov4; cov5};
    prior.mean      = {mean1};
    prior.lik       = {lik};
    
    inf_method                          = {@infPrior, @infExact, prior};
    objective_model.inference_method  	= inf_method;

    objective_model.minimize(10)
end

end