clear
subject_id          = {'EP002','EP010','EP014','EP015','EP017'};

data_dir            = 'evoked_potential_project/data/';
feature_dir         = [data_dir 'processed_data/'];
label_dir           = [data_dir 'label_data/'];

data_table          = [];
idx                 = 1;
for c1 = 1:size(subject_id,2)
	fprintf('\nSubject: %d\n\tStimulation: ', c1)
    
    subject_feature_dir    	= [feature_dir subject_id{c1} '/'];
	subject_label_dir    	= sprintf('%s%s_mean.xlsx',label_dir, subject_id{c1});

    mep_table               = readtable(subject_label_dir);
   	mep_data            	= mep_table{1:end,2:end};
    mep_features            = nan(2000,832,120);
	ep_struct               = proc_load_ep_struct(subject_feature_dir);
    stimulation_id          = c1;
    
    for c2 = 1:size(ep_struct,2)
        feature_all             = ep_struct(c2).emg_matrix(:,50:end,:);        
        mep_labels          	= mep_data(c2,:)';    
              
        if ep_struct(c2).data_table.frequency  ~= 10 || ...
            size(feature_all,3) ~= 120
            continue;
        end
        
        fprintf('%d, ', c2)        
        
        for c3 = 1:size(mep_labels,1)
            mep_features(idx,:,:)   = squeeze(feature_all(c3,:,:));
            data_subtable           = table(mep_labels(c3), subject_id(c1), idx,...
                'VariableNames', {'mep_labels', 'subject_id', 'stimulation_id'});
            
            data_subtable           = [data_subtable ep_struct(c2).data_table];
            data_table              = [data_table; data_subtable];
            
            idx = idx + 1;
        end
        
    end
end







