%% Generate mean images
close all
figure('Position',  [190, 100, 1600, 500])

subject = {'EP002','EP010','EP014','EP015','EP017'};
for c2 = 1%:size(subject,2)
    load_dir       	= ['evoked_potential_project/data/processed_data/' subject{c2} '/'];
    ep_struct       = proc_load_ep_struct(load_dir);
    
    for c1 = 1:size(ep_struct,2)
        if ep_struct(c1).data_table.frequency  ~= 10 
            continue;
        end
        
        for c3 = 1:8
            channel         = c3;
            data_all        = ep_struct(c1).emg_matrix;
            data_channel    = squeeze(data_all(channel,50:end,:));
            data_center     = data_channel - mean(data_channel);

            plot(data_center, 'color', .5*ones(1,3));
            hold on

            plot(mean(data_center,2),'color', 'k', 'LineWidth', 3);
            hold off

            title(sprintf('stimulation %03d, channel %03d', c1, channel));

            file_name       = sprintf('stimulation_%03d_channel_%03d.jpg', c1, channel);
            save_path       = sprintf('evoked_potential_project/data/mep_images/mean_images/%s/CH%d/',subject{c2},channel);

            if ~exist(save_path, 'dir')
                mkdir(save_path)
            end

            print(gcf, '-djpeg100', [save_path file_name], '-r0');
        end
    end
end

%%
close all
figure('Position',  [190, 100, 1600, 500])
channel             = 4;

subject = {'EP017'};
for c3 = 1:3
    load_dir       	= ['evoked_potential_project/data/processed_data/' subject{c3} '/'];
%     ep_struct       = p0_load_ep_struct(load_dir);
    
    for c1 = 40; 1:size(ep_struct,2)
        if ep_struct(c1).data_table.frequency  ~= 10 
            continue;
        end
        
        data_all        = ep_struct(c1).emg_matrix;
        data_channel    = squeeze(data_all(channel,170:end,:));
        data_center     = data_channel - mean(data_channel);

        pulse_idx       = 1:size(data_center,2);
        for c2 = 1:size(data_center,2)

            plot(data_center(:,pulse_idx ~= c2), 'color', .5*ones(1,3));
            hold on 
            plot(data_center(:,pulse_idx == c2), 'color', 'r', 'linewidth', 2);
            hold off

            title(sprintf('stimulation %03d, pulse %03d, channel %03d', c1, c2, channel));

            file_name       = sprintf('stimulation_%03d_pulse_%03d_channel_%03d.jpg', c1, c2, channel);
            save_path       = sprintf('evoked_potential_project/data/mep_images/%s/stimulation_%03d/',subject{c3}, c1);

            if ~exist(save_path, 'dir')
                mkdir(save_path)
            end

            print(gcf, '-djpeg100', [save_path file_name], '-r0');
        end
    end
end
