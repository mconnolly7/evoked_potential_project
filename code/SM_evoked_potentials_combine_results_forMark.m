% combine files created by SM_evoked_potentials_analysis.m

my_reload = 1; %1 to clear and reload; 0 to just replot EPs

if (my_reload)
    clear all
    my_reload = 1;
end

%%% ephys002 (STN)
pathname = 'data/human/ephys002/bipolar_next_NOmega_trigger_reorganized';
stim_param_legend = {    '1+0-/1mA/60us-70us-480us/10Hz'	    '1+0-/3mA/60us-70us-480us/10Hz'	    '1+0-/5mA/60us-70us-480us/10Hz'	'C+0-/1mA/60us-70us-480us/10Hz'	    'C+0-/3mA/60us-70us-480us/10Hz'	    'C+0-/5mA/60us-70us-480us/10Hz'	    '0+1-/1mA/60us-70us-480us/10Hz'	    '0+1-/3mA/60us-70us-480us/10Hz'	    '0+1-/5mA/60us-70us-480us/10Hz'	 ...
    '2+1-/1mA/60us-70us-480us/10Hz'	    '2+1-/3mA/30us-70us-240us/10Hz'	    '2+1-/3mA/30us-70us-240us/130Hz'	    '2+1-/3mA/60us-70us-480us/10Hz'	    '2+1-/3mA/60us-70us-480us/130Hz'	    '2+1-/3mA/120us-70us-480us/10Hz'	    '2+1-/5mA/60us-70us-480us/10Hz'	 ...
    'C+1-/1mA/60us-70us-480us/10Hz'	    'C+1-/3mA/30us-70us-240us/10Hz'	    'C+1-/3mA/60us-70us-480us/10Hz'	'C+1-/3mA/120us-70us-480us/10Hz'	    'C+1-/5mA/60us-70us-480us/10Hz'	    '1+2-/1mA/60us-70us-480us/10Hz'	    '1+2-/3mA/60us-70us-480us/10Hz'	    '1+2-/5mA/60us-70us-480us/10Hz'	   ...
    '3+2-/1mA/60us-70us-480us/10Hz'	    '3+2-/3mA/60us-70us-480us/10Hz'	    '3+2-/5mA/60us-70us-480us/10Hz'	    'C+2-/1mA/60us-70us-480us/10Hz'	    'C+2-/3mA/60us-70us-480us/10Hz'	    'C+2-/5mA/60us-70us-480us/10Hz'	    '2+3-/1mA/60us-70us-480us/10Hz'	    '2+3-/3mA/60us-70us-480us/10Hz'	    '2+3-/5mA/60us-70us-480us/10Hz'	  ...
    'C+3-/1mA/60us-70us-480us/10Hz'	    'C+3-/3mA/60us-70us-480us/10Hz'	    'C+3-/5mA/60us-70us-480us/10Hz' }; %	'C+2-/?V/90us/156Hz'	'C+3-/?V/90us/155Hz'	'2+1-/?V/60/158Hz'	'2+1-/?+25%V/60/158Hz'};
PRE_STIM = -0.01;     % pre-stim period in sec
PST_STIM = 0.1;     % post-stim period in sec 
sampling_rate = 22000; 
channels_anatomy = {'postS1csf_S1csf'	'S1csf_CS' 'CS_postM1csf'	'postM1csf_M1'	'M1_M1'	'M1_antM1'	'antM1_antM1csf'	'antM1csf_preM1csf'	'preM1csf_preM1csf'	'preM1csf_preM1csf'	'preM1csf_preM1'	'preM1_preM1csf'	'preM1csf_preM1csf' 'postS1csf_S1csf'	'S1csf_CS'	'CS_postM1csf'	'postM1csf_M1'	'M1_M1'	'M1_antM1'	'antM1_antM1csf'	'antM1csf_preM1csf'	'preM1csf_preM1csf'	'preM1csf_preM1csf'	'preM1csf_preM1csf'	'preM1csf_preM1'	'preM1_preM1'};;

% %%STN
% pathname = 'G:\Data_from_MyTransfer_part2\Ecog\PD\A_evoked_potentials\Lok,Wai\matlab_renamed\preprocessed\analysis\bipolar_next_MERGED2Hz';
% PRE_STIM = -0.01;     % pre-stim period in sec (WAS -0.05)
% PST_STIM = 0.1;     % post-stim period in sec (WAS 0.05)
% sampling_rate = 22000; 
% stim_param_legend = {'C+1-/3mA/60us-70us-60us/10Hz' '0-3+/3mA/60us-70us-60us/10Hz' 'C+2-/1mA/60us-70us-60us/10Hz' 'C+2-/3mA/60us-70us-60us/10Hz' '2-1+/5mA/60us-70us-60us/10Hz' '2-1+/3mA/60us-70us-60us/10Hz' 'C+0-/5mA/60us-70us-60us/10Hz' '0-1+/3mA/20us-70us-20us/10Hz' '3-2+/3mA/60us-70us-60us/10Hz' '1-2+/3mA/60us-70us-60us/10Hz' 'C+1-/5mA/60us-70us-60us/10Hz'...
%     'C+3-/3mA/60us-70us-60us/10Hz' '2-3+/5mA/60us-70us-60us/10Hz' '0-1+/1mA/60us-70us-60us/10Hz' 'C+0-/1mA/60us-70us-60us/10Hz' '3-0+/3mA/60us-70us-60us/10Hz' '1-0+/1mA/60us-70us-60us/10Hz' '0-1+/3mA/120us-70us-120us/10Hz' '2-3+/1mA/60us-70us-60us/10Hz' '3-2+/1mA/60us-70us-60us/10Hz' '1-0+/3mA/60us-70us-60us/10Hz' '2-1+/1mA/60us-70us-60us/10Hz' 'C+0-/3mA/60us-70us-60us/10Hz'...
%     '1-2+/5mA/60us-70us-60us/10Hz' 'C+3-/1mA/60us-70us-60us/10Hz' 'C+1-/1mA/60us-70us-60us/10Hz' '1-0+/5mA/60us-70us-60us/10Hz' '3-2+/5mA/60us-70us-60us/10Hz' '1-2+/1mA/60us-70us-60us/10Hz' '0-1+/3mA/60us-70us-60us/10Hz' '0-1+/5mA/60us-70us-60us/10Hz' 'C+2-/5mA/60us-70us-60us/10Hz' 'C+3-/5mA/60us-70us-60us/10Hz' '2-3+/3mA/60us-70us-60us/10Hz'...
%     '0-3+/2V/60us/156Hz/~0.8mA'	'0-3+/4V/60us/156Hz/~1.6mA'	'0-3+/6V/60us/156Hz/~2.4mA'	'0-3+/5V/60us/156Hz/~2mA'	'0-3+/9V/60us/156Hz/~3.6mA'	'0-3+/10V/60us/156Hz/~4mA'};
% channels_anatomy = {'preS1_preS1' 'preS1_preS1' 'preS1_preS1' 'preS1_preS1' 'preS1_postS1' 'postS1_S1' 'S1_S1' 'S1_antS1' 'antS1_CS' 'CS_postM1' 'postM1_M1' 'M1_M1' 'M1_antM1'   'preS1_preS1' 'preS1_preS1' 'preS1_preS1' 'preS1_preS1' 'preS1_preS1' 'preS1_postS1' 'postS1_S1' 'S1_S1' 'S1_antS1' 'antS1_postM1' 'postM1_M1' 'M1_M1' 'M1_antM1'}; 


% %%% STN (VA, TDT)
% pathname = 'E:\Data_from_MyTransfer_part2\Ecog\PD\A_evoked_potentials\Mitchell\EPanalysis\bipolar_next';
% PRE_STIM = -0.01;     % pre-stim period in sec (WAS -0.05)
% PST_STIM = 0.1;     % post-stim period in sec (WAS 0.05)
% sampling_rate = 24414; 
% stim_param_legend = {'2-3+/2V/60us/10Hz/0.7mA' '2-3+/10V/60us/10Hz/3.4mA' '2-3+/5V/60us/10Hz/1.7mA' '1-0+/10V/60us/10Hz/3.4mA' '1-0+/2V/60us/10Hz/0.7mA' '1-0+/5V/60us/10Hz/1.7mA' '1-2+/2V/60us/10Hz/0.7mA' '1-2+/5V/60us/10Hz/1.7mA' '1-2+/10V/60us/10Hz/3.4mA' '3-2+/10V/60us/10Hz/3.4mA' '3-2+/5V/60us/10Hz/1.7mA' '3-2+/2V/60us/10Hz/0.7mA' '2-1+/2V/60us/10Hz/0.7mA'...
%     '2-1+/5V/60us/10Hz/1.7mA' '2-1+/10V/60us/10Hz/3.4mA' '0-1+/2V/60us/10Hz/0.7mA' '0-1+/10V/60us/10Hz/3.4mA' '0-1+/5V/60us/10Hz/1.7mA'};
% channels_anatomy = {'prepreS1_preS1csf' 'preS1csf_preS1' 'preS1_preS1' 'preS1_preS1csf' 'preS1csf_postS1' 'postS1_S1' 'S1_antS1' 'antS1_postM1' 'postM1_M1' 'M1_antM1' 'antM1_preM1csf' 'preM1csf_preM1' 'preM1_preM1'  'prepreS1_prepreS1csf' 'prepreS1csf_prepreS1csf' 'prepreS1csf_prepreS1csf' 'prepreS1csf_prepreS1csf' 'prepreS1csf_postS1' 'postS1_S1' 'S1_antS1' 'antS1_postM1' 'postM1_M1' 'M1_M1' 'M1_antM1' 'antM1_preM1csf' 'preM1csf_preM1'}; 

% % STN (Boston Sci)
% pathname = 'E:\Data_from_MyTransfer_part2\Ecog\PD\A_evoked_potentials\Sexton\matlab_renamed\preprocessed\analysis\bipolar_next';
% PRE_STIM = -0.01;     % pre-stim period in sec (WAS -0.05)
% PST_STIM = 0.1;     % post-stim period in sec (WAS 0.05)
% sampling_rate = 22000; 
% stim_param_legend = {'1-2+/3mA/60us-480us/10Hz','2-1+/3mA/60us-480us/10Hz','2-3+/3mA/60us-480us/10Hz','3-2+/3mA/60us-480us/10Hz','3-4+/3mA/60us-480us/10Hz','4-3+/3mA/60us-480us/10Hz','4-5+/3mA/60us-480us/10Hz','5-4+/3mA/60us-480us/10Hz','2-4+/3mA/60us-480us/10Hz','4-2+/3mA/60us-480us/10Hz','1-2+/5mA/60us-480us/10Hz','2-1+/5mA/60us-480us/10Hz',...
%     '2-3+/5mA/60us-480us/10Hz','3-2+/5mA/60us-480us/10Hz','2-3+/3mA/60us-60us/10Hz','3-2+/3mA/60us-60us/10Hz','2-4+/3mA/60us-60us/10Hz','4-2+/3mA/60us-60us/10Hz','1-S+/3mA/60us-480us/10Hz','2-S+/3mA/60us-480us/10Hz','3-S+/3mA/60us-480us/10Hz','4-S+/3mA/60us-480us/10Hz','5-S+/3mA/60us-480us/10Hz','6-S+/3mA/60us-480us/10Hz','7-S+/3mA/60us-480us/10Hz','8-S+/3mA/60us-480us/10Hz','2-S+/5mA/60us-480us/10Hz',...
%     '2-4+/0.5mA/60us-BSci/130Hz','2-4+/1mA/60us-BSci/130Hz','2-4+/1.5mA/60us-BSci/130Hz','2-4+/2mA/60us-BSci/130Hz','2-4+/1.5mA-r/60us-BSci/130Hz','2-4+/2mA-r/60us-BSci/130Hz','2-4+/2.5mA/60us-BSci/130Hz','2-4+/3mA/60us-BSci/130Hz','2-4+/3.5mA/60us-BSci/130Hz','2-4+/4mA/60us-BSci/130Hz', 'Left/2-4+/6.5mA/60us-BSci/130Hz'};
% channels_anatomy = {'preS1-s_preS1' 'preS1_preS1' 'preS1_preS1-s' 'preS1-s_postS1-csf' 'postS1-csf_S1-csf' 'S1-csf_antS1-csf' 'antS1-csf_CS' 'CS_postM1-csf' 'postM1-csf_postM1' 'postM1_M1' 'M1_M1' 'M1_antM1' 'antM1_antM1' 'preS1-s_preS1' 'preS1_preS1' 'preS1_preS1-s' 'preS1-s_postS1-csf' 'postS1-csf_postS1-csf' 'postS1-csf_S1-csf' 'S1-csf_antS1-csf' 'antS1-csf_CS' 'CS_postM1-csf' 'postM1-csf_antM1' 'antM1_preM1-s' 'preM1-s_preM1-s' 'preM1-s_preM1-s'}; %bipolar

% pathname = 'E:\Data_from_MyTransfer_part2\Ecog\PD\A_evoked_potentials\Silvers\Steven Silvers TDT Files\Mat files\analysis\bipolar_next_butter3_1-Hz';
% PRE_STIM = -0.01;     % pre-stim period in sec (-0.005 for average montage; 0.01 for bipolar)
% PST_STIM = 0.1;     % post-stim period in sec 
% sampling_rate = 24414.062500; %24414.062500; 
% stim_param_legend = {'S/0-1+/2V/60us/10Hz/0.8mA' 'S/0-1+/5V/60us/10Hz/1.99mA' 'S/0-1+/10V/60us/10Hz/3.98mA' 'S/1-0+/2V/60us/10Hz/0.8mA' 'S/1-0+/5V/60us/10Hz/2mA' 'S/1-0+/10V/60us/10Hz/4mA' 'S/1-2+/2V/60us/10Hz/0.76mA' 'S/1-2+/5V/60us/10Hz/1.9mA' 'S/1-2+/10V/60us/10Hz/3.8mA' 'S/2-1+/2V/60us/10Hz/0.76mA' 'S/2-1+/5V/60us/10Hz/1.9mA' 'S/2-1+/10V/60us/10Hz/3.8mA' 'S/2-3+/2V/60us/10Hz/0.73mA' 'S/2-3+/5V/60us/10Hz/1.82mA' 'S/2-3+/10V/60us/10Hz/3.64mA' 'S/3-2+/2V/60us/10Hz/0.73mA' 'S/3-2+/5V/60us/10Hz/1.82mA' 'S/3-2+/10V/60us/10Hz/3.64mA' 'S/1-2+/5V/120us/10Hz/1.9mA' 'S/2-1+/5V/120us/10Hz/1.9mA'};
% channels_anatomy = {'preS1_preS1-s' 'preS1-s_postS1' 'postS1_antS1' 'antS1_CS' 'CS_postM1' 'postM1_postM1' 'postM1_M1' 'M1_M1' 'M1_antM1' 'antM1_antM1' 'antM1_preM1' 'preM1_preM1' 'preM1_preM1' 'preS1-s_preS1-s' 'preS1-s_postS1' 'postS1_antS1' 'antS1_CS' 'CS_postM1' 'postM1_postM1' 'postM1_M1' 'M1_M1' 'M1_antM1' 'antM1_preM1-s' 'preM1-s_preM1' 'preM1_preM1' 'preM1_preM1'}; %bipolar

% pathname = 'E:\Data_from_MyTransfer_part2\Ecog\PD\A_evoked_potentials\Carter_D\Carter_Daniel\analysis\bipolar_next';
% PRE_STIM = -0.01;     % pre-stim period in sec (WAS -0.05)
% PST_STIM = 0.1;     % post-stim period in sec (WAS 0.05)
% sampling_rate = 22000; 
% stim_param_legend = {'0-1+/2V/60us/10Hz'	'0-1+/5V/60us/10Hz'	'0-1+/10V/60us/10Hz'	'1-0+/2V/60us/10Hz'	'1-0+/5V/60us/10Hz'	'1-0+/10V/60us/10Hz'	'1-2+/2V/60us/10Hz'	'1-2+/5V/60us/10Hz'	'1-2+/10V/60us/10Hz'	'2-1+/2V/60us/10Hz'	'2-1+/5V/60us/10Hz'	'2-1+/10V/60us/10Hz'	'2-3+/2V/60us/10Hz'	'2-3+/5V/60us/10Hz'	'2-3+/10V/60us/10Hz'	'3-2+/2V/60us/10Hz'	'3-2+/5V/60us/10Hz'	'3-2+/10V/60us/10Hz'};
% channels_anatomy = {'postS1csf_S1', 'S1_S1', 'S1_antS1', 'antS1_CS', 'CS_postM1', 'postM1_M1', 'M1_M1', 'M1_antM1', 'antM1_preM1', 'preM1_preM1', 'preM1_prepreM1', 'prepreM1_prepreM1', 'prepreM1_prepreM1', 'postS1csf_S1', 'S1_S1', 'S1_antS1csf', 'antS1csf_CS', 'CS_postM1', 'postM1_M1', 'M1_M1', 'M1_antM1','antM1_preM1s', 'preM1s_preM1', 'preM1_preM1', 'preM1_preM1', 'preM1_preM1'}; %bipolar_next


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
line_type = {'r' 'b' 'g' 'k' 'm' 'c' 'y' 'r:' 'b:' 'g:' 'k:' 'c:' 'm:' 'y:' 'r--' 'b--' 'g--' 'k--' 'm--' 'c--' 'y--' 'r-.' 'b-.' 'g-.' 'k-.' 'm-.' 'c-.' 'y-.'};  %for plotting

if (my_reload)
    
for stim_params_num = 1:length(stim_param_legend)
    filename = ['ep_results' num2str(stim_params_num) '.mat'];
    load([pathname filesep filename]);
    
    %shift baseline to zero
    disp('Shifting pre-stim baseline to zero ')
    mean_diff_stim_epoch_ecog = squeeze(mean_diff_stim_epoch_ecog);
    mean_diff_stim_epoch_emg = squeeze(mean_diff_stim_epoch_emg);  %this should be mean_diff_stim_epoch_emg' if only 1 EMG channel available
    mean_diff_stim_epoch_lfp = squeeze(mean_diff_stim_epoch_lfp);
    
    %(prestim baseline is considered time from 0 (PRE_STIM) to 1 msec before stim)
%     mean_diff_stim_epoch_ecog = mean_diff_stim_epoch_ecog - repmat(mean(mean_diff_stim_epoch_ecog(1:round(abs(PRE_STIM+0.001)*sampling_rate),:)),size(mean_diff_stim_epoch_ecog,1),1);
%     mean_diff_stim_epoch_emg = mean_diff_stim_epoch_emg - repmat(mean(mean_diff_stim_epoch_emg(1:round(abs(PRE_STIM+0.001)*sampling_rate),:)),size(mean_diff_stim_epoch_emg,1),1);
%     mean_diff_stim_epoch_lfp = mean_diff_stim_epoch_lfp - repmat(mean(mean_diff_stim_epoch_lfp(1:round(abs(PRE_STIM+0.001)*sampling_rate),:)),size(mean_diff_stim_epoch_lfp,1),1);
    
    %(prestim baseline is considered time from 1 msec before stim to just before stim)
    mean_diff_stim_epoch_ecog = mean_diff_stim_epoch_ecog - repmat(mean(mean_diff_stim_epoch_ecog(round(abs(PRE_STIM+0.001)*sampling_rate):round(abs(PRE_STIM)*sampling_rate)-1,:)),size(mean_diff_stim_epoch_ecog,1),1);
    mean_diff_stim_epoch_emg = mean_diff_stim_epoch_emg - repmat(mean(mean_diff_stim_epoch_emg(round(abs(PRE_STIM+0.001)*sampling_rate):round(abs(PRE_STIM)*sampling_rate)-1,:)),size(mean_diff_stim_epoch_emg,1),1);
    mean_diff_stim_epoch_lfp = mean_diff_stim_epoch_lfp - repmat(mean(mean_diff_stim_epoch_lfp(round(abs(PRE_STIM+0.001)*sampling_rate):round(abs(PRE_STIM)*sampling_rate)-1,:)),size(mean_diff_stim_epoch_lfp,1),1);

    if (exist('EP_lfp', 'var') && size(mean_diff_stim_epoch_lfp, 2) ~= size(EP_lfp,3)) %this is a workaround since bost sci stim files have fewer or no lfp channels
        if (~isempty(mean_diff_stim_epoch_lfp))
            mean_diff_stim_epoch_lfp = [mean_diff_stim_epoch_lfp mean_diff_stim_epoch_lfp mean_diff_stim_epoch_lfp] ;
        else
            mean_diff_stim_epoch_lfp = EP_ecog(1,:,1:12);  %enter how many LFP channels expected (4 for medtronic, 8 or 12 for Boston Sci)
        end   
    end    
    EP_ecog(stim_params_num,:,:) = mean_diff_stim_epoch_ecog;  %stim_params_num:time:channel
    EP_emg(stim_params_num,:,:) = mean_diff_stim_epoch_emg;
    EP_lfp(stim_params_num,:,:) = mean_diff_stim_epoch_lfp;
    clear mean_diff_stim_epoch_ecog
    clear mean_diff_stim_epoch_emg
    clear mean_diff_stim_epoch_lfp
    
    if (exist('num_events', 'var')) %add number of events to legend
        stim_param_legend(stim_params_num) = cellstr([char(stim_param_legend(stim_params_num)) ' ' num2str(num_events) 'ev']);
    end     
end

t = 1000*(PRE_STIM:1/sampling_rate:PST_STIM); % multiplied by 1000 to change to msec scale

% disp('EMG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
% EP_ecog = EP_emg; %to process EMG

%smooth EP and find all significant peaks and troughs for each channel
wndwSize = 3; %was 5
h = ones(1,wndwSize)/wndwSize; %moving average window
hg = pdf('Normal',-floor(wndwSize/2):floor(wndwSize/2),0,1); %gaussian smoothing window

ind_0ms = find(t >= 0, 1);
ind_n1ms = find(t >= -1, 1);
ind_1ms = find(t >= 1, 1);

peaks_times = {}; peaks_amps = {}; troughs_times = {}; troughs_amps = {}; base_time = {}; base_amp = {};
for ss = 1:size(EP_ecog,1)
    for ch = 1:size(EP_ecog,3)
        tmpsig = EP_ecog(ss,:,ch); 
        smooth1 = filter(h,1,tmpsig);
        smooth2 = my_smooth(filter(hg,1,tmpsig),15);
        smooth3 = my_smooth(tmpsig,15); 
        smooth4 = my_smooth(smooth1,15);
        
        smooth = smooth3; %use this version for smoothing 
        
%         figure; plot(t,tmpsig,'b'); hold on; plot(t,smooth1,'r'); plot(t,smooth4,'m'); plot(t,smooth2,'k'); plot(t,smooth3,'c'); 
%         legend({'original EP', 'moving avg' 'moving avg+smooth' 'gaussian+smooth' 'smooth'})
%         
        %EPsmooth_ecog(ss,:,ch) = smooth;
        EPsmooth_ecog(ss,:,ch) = [smooth(1:ind_0ms-10) tmpsig(ind_0ms-10+1:ind_1ms-1) smooth(ind_1ms:end)]; %don't smooth stim artifact

        
%         %%%%automatic peak detection - not working 
%         thresh = 3; 
% %         baseline_mean = mean(smooth(1:ind_n1ms));
% %         baseline_std = std(smooth(1:ind_n1ms));
%         baseline_mean = mean(smooth(ind_n1ms:ind_0ms-10)); %-10 because smoothed stim artifact starts early
%         baseline_std = std(smooth(ind_n1ms:ind_0ms-10));
% %         baseline_mean = mean(smooth(end-round((PST_STIM/5)*sampling_rate):end)); %last 1/5th of the trace is considered baseline for peak/trough identification
% %         baseline_std = std(smooth(end-round((PST_STIM/5)*sampling_rate):end));
%         [pks,pks_locs] = findpeaks(smooth(ind_1ms:end), 'MinPeakHeight',baseline_mean+thresh*baseline_std, 'MinPeakDistance',100);
%         pks_locs = pks_locs+ind_1ms;
%         [trs,trs_locs] = findpeaks(-1*smooth(ind_1ms:end), 'MinPeakHeight',-1*(baseline_mean-thresh*baseline_std), 'MinPeakDistance',100);
%         trs_locs = trs_locs+ind_1ms;
%         try
%             cross_ind = crossing(smooth(ind_1ms:end));
%             cross_ind = [1 cross_ind+ind_1ms]; %add 1 to help with sorting below
%         catch 
%             cross_ind = []; %this is in case signal is all zeros
%         end
%         
%         %choose only largest peaks/troughs between each zero crossing
%         new_pks_locs = []; new_trs_locs = [];
%         for cr = 1:length(cross_ind)-1
%             tmpi = find(pks_locs >= cross_ind(cr) & pks_locs <= cross_ind(cr+1));
%             if (~isempty(tmpi))
%                 [y,ti] = max(smooth(pks_locs(tmpi)));
%                 new_pks_locs = [new_pks_locs pks_locs(tmpi(ti))];
%             end
%             tmpi = find(trs_locs >= cross_ind(cr) & trs_locs <= cross_ind(cr+1));
%             if (~isempty(tmpi))
%                 [y,ti] = min(smooth(trs_locs(tmpi))); 
%                 new_trs_locs = [new_trs_locs trs_locs(tmpi(ti))];
%             end
%         end
% %         figure; plot(t,tmpsig,'b-'); hold on; plot(t,EPsmooth_ecog(ss,:,ch),'r-'); %plot(t,-1*smooth,'r:'); 
% %         plot([t(1) t(end)], [baseline_mean+thresh*baseline_std baseline_mean+thresh*baseline_std],'k:'); plot([t(1) t(end)], [baseline_mean-thresh*baseline_std baseline_mean-thresh*baseline_std],'k:');
% %         plot(t(new_pks_locs),smooth(new_pks_locs), 'r*'); plot(t(new_trs_locs),smooth(new_trs_locs), 'g*'); plot(t(cross_ind),smooth(cross_ind), 'k*'); hold off; 
% %        % plot(t(pks_locs),smooth(pks_locs), 'ro'); plot(t(trs_locs),smooth(trs_locs), 'go'); plot(t(cross_ind),smooth(cross_ind), 'k*'); hold off; 
% 
%         %identify peak and troughs that are greater (or smaller for troughs) than mean+std
%         signal_mean = mean(tmpsig(ind_1ms:end)); %do not include artifact
%         signal_std = std(tmpsig(ind_1ms:end));
%         signif_pks_locs = new_pks_locs(smooth(new_pks_locs) >= signal_mean+1*signal_std); 
%         signif_trs_locs = new_trs_locs(smooth(new_trs_locs) <= signal_mean-1*signal_std); 
%         
%         %peaks_times{ss,ch} = t(new_pks_locs); peaks_amps{ss,ch} = smooth(new_pks_locs); troughs_times{ss,ch} = t(new_trs_locs); troughs_amps{ss,ch} = smooth(new_trs_locs);
%         peaks_times{ss,ch} = t(signif_pks_locs); peaks_amps{ss,ch} = smooth(signif_pks_locs); troughs_times{ss,ch} = t(signif_trs_locs); troughs_amps{ss,ch} = smooth(signif_trs_locs);
%         base_time{ss,ch} = t(ind_1ms);  base_amp{ss,ch} = smooth(ind_1ms);  %define baseline point at 1ms after stim
    
    end
end
end %my_reload

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%EXECUTE JUST SECTION TO PLOT DESIRED STIM SETTING AND CHANNEL 
% you can specify more than one channel and/or setting to plot (comment out all EPsmooth plot so that legends are correct)

stim_params_to_plot = [5]; %specify which stim setting to plot 
channels_to_plot = [5];   %specify which channel to plot
figure; ct=1; ct2 = 1; mylegend = {}; channel_legend = cellstr(num2str(channels_to_plot'))';
for i = channels_to_plot
    for j = stim_params_to_plot
       plot(t,EP_ecog(j,:,i),char(line_type(ct2))); ct2 = ct2+1; hold on  
       plot(t,EPsmooth_ecog(j,:,i),char(line_type(ct2)));    %plot smoothed
%         plot([0 100], (mean(EP_ecog(j,ind_1ms:end,i))+1*std(EP_ecog(j,ind_1ms:end,i)))*[1 1],'r:') %plot +1 STDEV 
%         plot([0 100], (mean(EP_ecog(j,ind_1ms:end,i))-1*std(EP_ecog(j,ind_1ms:end,i)))*[1 1],'r:') %plot -1 STDEV 
        mylegend = [mylegend arrayfun(@(v1, v2) strcat('Ch', v1, ':', v2), channel_legend(ct), stim_param_legend(j))];
    end    
   xlim([-2 20]); %max 100; keep at 20 when analyzing short latency EPs
   ylim([-30 30]);  %keep at -30 30 for consistency unless EPs are bigger  
   ct=ct+1; 
end
legend(mylegend);  set(gcf,'color','w');
%[x,y] = ginput(12) %uncomment this line to perform manual peak/trough selection

