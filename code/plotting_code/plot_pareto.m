function  pareto_idx = plot_pareto(ep_data, emg_data, input_space, plot_color)

if nargin == 1
    ep_model       	= model_ep_objective(ep_data);
    emg_model      	= model_emg_objective(ep_data);
elseif nargin >= 2
    ep_model     	= ep_data;
    emg_model       = emg_data;
end

% input_region        = unique(input_space(:,1));
% input_amplitude     = unique(input_space(:,2));

emg_est             =  emg_model.predict(input_space);
ep_est              =  ep_model.predict(input_space);

if nargin < 4
    p = [217,95,2]/255;
else   
    p = plot_color;
end
Y_mat   = [ep_est emg_est];

% for c1 = 1:size(input_region,1)
%     channel_idx  = input_space(:,1) == input_region(c1);
%     Y_channel    = [Y_mat(channel_idx,:)];
%     
%     if input_region(c1) < 5
%         p_color = p(1,:);
%     else
%         p_color = p(3,:);
%     end
%     
%     %%%%
%     subplot(2,3,1)
%     hold on
%     p_mopo(c1)  = scatter(Y_channel(:,3), Y_channel(:,1), 200, 'Filled', ...
%         'MarkerFaceAlpha', .5, 'MarkerFaceColor',  p_color);
% %     plot(Y_channel(:,3), Y_channel(:,1),'Color', p_color);
%     
%     %%%%
%     subplot(2,3,4)
%     hold on
%     scatter(Y_channel(:,1), Y_channel(:,2), 200, 'Filled', ...
%         'MarkerFaceAlpha', .5, 'MarkerFaceColor',  p_color);
% %     plot(Y_channel(:,1), Y_channel(:,2),'Color', p_color);
%     
%     %%%%
%     subplot(2,3,5)
%     hold on
%     scatter(Y_channel(:,3), Y_channel(:,2), 200, 'Filled', ...
%         'MarkerFaceAlpha', .5, 'MarkerFaceColor',  p_color);
% %     plot(Y_channel(:,3), Y_channel(:,2),'Color', p_color);
%  
%     %%%%
%     subplot(2,3,2)
%     hold on
%     scatter3(Y_channel(:,1), Y_channel(:,2), Y_channel(:,3), 200, 'Filled', ...
%         'MarkerFaceAlpha', .5, 'MarkerFaceColor',  p_color);    hold on
% %     plot3(Y_channel(:,1), Y_channel(:,2), Y_channel(:,3),'Color', p_color);
%     grid on
% end
% 
for c1 = 1:size(Y_mat,1)
    ep_opt              = Y_mat(c1,1) >= Y_mat(:,1);
    emg_opt             = Y_mat(c1,2) <= Y_mat(:,2);
%     eff_opt             = Y_mat(c1,3) <= Y_mat(:,3);
%     pareto_opt          = ep_opt | emg_opt | eff_opt;
    pareto_opt          = ep_opt | emg_opt ;
    
    pareto_idx(c1)    = all(pareto_opt);
    
end


% subplot(2,3,1)
% p_paro = scatter(Y_mat(pareto_idx,3), Y_mat(pareto_idx,1), 200, ...
% 'Filled', 'MarkerFaceAlpha', .5, 'MarkerFaceColor',  p(2,:));
% 
% subplot(2,3,4)
scatter(Y_mat(pareto_idx,1), Y_mat(pareto_idx,2), 200, ...
'Filled', 'MarkerFaceAlpha', .5, 'MarkerFaceColor',  p);
hold on
scatter(Y_mat(~pareto_idx,1), Y_mat(~pareto_idx,2), 200, ...
'Filled', 'MarkerFaceAlpha', .5, 'MarkerFaceColor',  'k');
hold off
% subplot(2,3,5)
% scatter(Y_mat(pareto_idx,3), Y_mat(pareto_idx,2), 200, ...
% 'Filled', 'MarkerFaceAlpha', .5, 'MarkerFaceColor',  p(2,:));
% 
% subplot(2,3,2)   
% p_paro = scatter3(Y_mat(pareto_idx,1), Y_mat(pareto_idx,2), Y_mat(pareto_idx,3), 200, ...
%     'Filled', 'MarkerFaceAlpha', .5, 'MarkerFaceColor',  p(2,:));

% subplot(2,3,1)
% legend([p_mopo(1), p_mopo(10), p_paro], {'Monopolar', 'Bipolar', 'Pareto Optimal'}, 'Location', 'northwest');
% xlabel('Stimulation Ampltidue (mA)')
% ylabel('EP Magnitude')
% set(gca, 'FontSize', 16)

% subplot(2,3,4)
xlabel('EP Magnitude')
ylabel('P(EMG)')
set(gca, 'FontSize', 16)

% subplot(2,3,5)
% xlabel('Stimulation Ampltidue (mA)')
% ylabel('P(EMG)')
% set(gca, 'FontSize', 16)
% 
% subplot(2,3,2)
% xlabel('EP Magnitude')
% ylabel('P(EMG)')
% xlabel('EP Magnitude')
% zlabel('Amplitude (mA)')
% set(gca, 'FontSize', 16)

end




