function plot_4D_model(objective_model, f, ax)

u_amplitudes    = [1 3 5]';
input_anode     = 0:.1:3;
input_cathode   = 0:.1:3;
u_pulse_width   = [20 60 120];
zmin            = 0;
zmax           = 0;
for c2 = 1:3

    input_pw        = u_pulse_width(c2);

    if nargin == 3
        set(f, 'currentaxes', ax(c2));
    else
        subplot(1,3,c2)
    end
    
    for c1 = 1:3
        input_amp       = u_amplitudes(c1);

        input           = combvec(input_cathode, input_anode, input_amp, input_pw)';
        Y_est           = objective_model.predict(input);

        t1              = reshape(input(:,1), size(input_cathode,2), size(input_anode,2));
        t2              = reshape(input(:,2), size(input_cathode,2), size(input_anode,2));
%             t3              = reshape(input(:,3), size(input_cathode,2), size(input_anode,2));
        e1              = reshape(Y_est, size(input_cathode,2), size(input_anode,2));

        hold on
        surf(t1,t2,e1, 'LineStyle', 'none', 'FaceAlpha', .5)
        view(71,12)
        zmin = min([zmin min(Y_est)]);
        zmax = max([zmax max(Y_est)]);

    end
    colormap(viridis)
    xlabel('Cathode')
    ylabel('Anode')
    zlabel('cEP (µV)')

    set(gca,'FontSize', 12)
    title(sprintf('Pulse Width %dµs', input_pw))
end
    
for c1 = 1:3
    if nargin == 3
        set(f, 'currentaxes', ax(c1));
    else
        subplot(1,3,c2)
    end
    zlim([zmin zmax*1.2])
end


end

