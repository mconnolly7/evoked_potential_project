function [outputArg1,outputArg2] = d8_plot_ep_objective(inputArg1,inputArg2)

if nargin > 1 && PLOT
        
    objective_model.plot_mean
    objective_model.plot_data;
    
    [~, y_max]          = objective_model.discrete_extrema(2);
    detect_prop_range   = [0 y_max];  
    
    xlabel('Cathode')
    ylabel('Amplitude (mA)');
    set(gca,'fontSize', 14);
    view(0, 90)
    zlim(detect_prop_range)
    caxis(detect_prop_range)
    colormap('jet')
    title('Monopolar')   
        
end
end

