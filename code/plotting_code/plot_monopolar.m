function plot_monopolar(objective_model)

objective_model.lower_bound = [0 0];
objective_model.update_plot_inputs
objective_model.plot_mean;

% objective_model.plot_data;
colormap(viridis)
xticks([0 1 2 3])
xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})
    
set(gca, 'FontSize', 20)
ylabel('Amplitude (mA)')
zlabel('mEP (µV)')
end

