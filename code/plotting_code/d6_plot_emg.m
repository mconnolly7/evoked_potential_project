function d6_plot_emg(ep_struct)

for c1 = 4
    data                        = squeeze(ep_struct.emg_matrix(c1,:,:));
    data_mean                   = data - mean(data);
    data_filt                   = highpass(data_mean, 20, 22000);
    data_filt                   = lowpass(data_filt, 50, 22000);
    
    event_detected              = ep_struct.event_detected(c1,:);
    threshold                   = ep_struct.emg_threshold(c1);

        
    pulse_start_time            = ep_struct.pulse_start_time;
    t                           = ((1:size(data_filt,1))/22000  - pulse_start_time) * 1000;
    
%     subplot(2,4,c1)
    hold on

    if mean(event_detected) ~= 1
        plot(t,data_filt(:,~event_detected), 'color', .5*ones(1,3))        
    end
    
    if mean(event_detected) ~= 0
        plot(t,data_filt(:,event_detected==1), 'color', 0*ones(1,3))        
    end
    
    plot([min(t) max(t)], threshold * [1 1], 'r--', 'LineWidth', 2);
    ylim(3*threshold*[-1 1]) 
    
end
end