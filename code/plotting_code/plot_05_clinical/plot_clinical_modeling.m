%%
clear
close all
f = figure('position', [680 452 1750 650]);

x_grid                      = [0.05    0.375    0.7];
y_grid_1                    = [.1 .525];

width                       = .2625;
height                      = [.8 .375];

ax(1)                       = axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(2)                       = axes('Position', [x_grid(2)  y_grid_1(1) width(1) height(2)]);
ax(3)                       = axes('Position', [x_grid(2)  y_grid_1(2) width(1) height(2)]);
ax(4)                       = axes('Position', [x_grid(3)  y_grid_1(1) width(1) height(1)]);

font_size                   = 20;
subject                     = 1;

switch subject
    case {1, 2, 5, 7, 10 }
        clinical_parameters = [1, 3.6];
        [cep_models, mep_models]    = create_monopolar_gt_models(10);

    case 15
        clinical_parameters = [1, 2.25];
        [cep_models, mep_models]    = create_monopolar_gt_models(15);
        
        cep_models.lower_bound(1)   = 0;
        cep_models.upper_bound(1)   = 3;
        
        mep_models.lower_bound(1)   = 0;
        mep_models.upper_bound(1)   = 3;
        
        cep_models.x_data(:,1)      = cep_models.x_data(:,1) - 1;
        mep_models.x_data(:,1)      = mep_models.x_data(:,1) - 1;
        
        cep_models.update_plot_inputs;
        mep_models.update_plot_inputs;
end

cep_models.lower_bound(2)   = 0;
mep_models.lower_bound(2)   = 0;
cep_models.update_plot_inputs;
mep_models.update_plot_inputs;

input_space = combvec(0:3, .1:.1:5)';
cep_models.noise_sigma = .25;

y = cep_models(1).sample(input_space);
set(f, 'currentaxes', ax(3));
cep_models.plot_mean()
% scatter3(input_space(:,1),input_space(:,2), y, 'r', 'filled', 'Marker', 'o')
cep_models.plot_data
colormap(inferno)

ylabel('Amplitude (mA)');
zlabel('cEP (µV)')
xlim([0 3])
ylim([0 5])
scatter3(clinical_parameters(1), clinical_parameters(2), ...
    cep_models.predict(clinical_parameters), 150, [255 0 0]/255,'filled', 'MarkerEdgeColor', 'k');

xticks([0 1 2 3])
xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})

set(gca,'FontSize', font_size)

%%%
set(f, 'currentaxes', ax(2));
mep_models.plot_mean
mep_models.plot_data
colormap(inferno)


ylabel('Amplitude (mA)');
zlabel('mEP (µV)')
xlim([0 3])
ylim([0 5])
scatter3(clinical_parameters(1), clinical_parameters(2), ...
    mep_models.predict(clinical_parameters), 150, [255 0 0]/255,'filled', 'MarkerEdgeColor', 'k');

xticks([0 1 2 3])
xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})

set(gca,'FontSize', font_size)

set(f, 'currentaxes', ax(4));
% 
input_cathode       = [0 1 2 3];
input_amplitude     = 0.1:.1:5;
input_space         = combvec(input_cathode, input_amplitude)';

cep_gt              = cep_models(1).predict(input_space);
mep_gt              = mep_models(1).predict(input_space);

[pareto_idx, dom_order] = get_pareto_2(1, -1, [cep_gt mep_gt]);


max_dom_order   = 2;
    


dom_color_map   = [
         0   104    55
       120   198   121
       217   240   163  
                    ]/255;

hold on
for c2 = 0:max_dom_order
    plot_idx = dom_order == c2;
    plot_handle(c2+1) = scatter(cep_gt(plot_idx,1),mep_gt(plot_idx,1),100,dom_color_map(c2+1,:),'filled', 'MarkerFaceAlpha',.7, 'MarkerEdgeColor', 'k');
end
plot_idx = dom_order > c2;
scatter(cep_gt(plot_idx,1),mep_gt(plot_idx,1),100,[.5 .5 .5])

scatter(cep_models.predict(clinical_parameters),...
    mep_models.predict(clinical_parameters), 250, [ 255 0 0]/255,'filled', 'MarkerEdgeColor', 'k');
[l ,icons,plots,legend_text] = legend( {'Rank 1', 'Rank 2', 'Rank 3', 'Rank ≥ 4', 'Clinical Setting'}, 'Position', [0.8823 0.8077 0.0269 0.0585], 'box', 'off', 'FontSize',20 );


for c1 = 6:9
    icons(c1).Children.MarkerSize = 11;
end
icons(10).Children.MarkerSize = 16;

xlabel('cEP (µV)')
ylabel('mEP (µV)')
set(gca,'FontSize', font_size)


set(f, 'currentaxes', ax(1));
    
for c2 = 0:max(dom_order)
    dom_idx = find(dom_order == c2);
    for c3 = 1:size(dom_idx,1)
        input_d = input_space(dom_idx(c3),:);
        xx = [input_d(1) - 0.4,input_d(1) + 0.4,input_d(1) + 0.4,input_d(1) - 0.4];
        yy = [input_d(2) - .04, input_d(2) - .04, input_d(2) + .04, input_d(2) + .04];

        if c2 > max_dom_order
            patch(xx,yy,[1 1 1], 'EdgeColor', [.5 .5 .5])
        else
%             patch(xx,yy,dom_color_map(c2+1,:), 'FaceAlpha', 1, 'edgecolor', dom_color_map(c2+1,:))
            patch(xx,yy,dom_color_map(c2+1,:), 'FaceAlpha', 1, 'edgecolor', 'k')
        end
    end
end
hold on
scatter(clinical_parameters(1), clinical_parameters(2), 250, [255 0 0]/255, 'filled', 'MarkerEdgeColor', 'k');

ylim([0 5])
xlim([-.8 3.8])
xticks([0 1 2 3])
xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})
ylabel('Amplitude (mA)')
% title(sprintf('Subject: EP0%02d', subject))
title(sprintf('Subject 4'))

set(gca,'FontSize',font_size)
%%
% print( 'pareto_mapping.png','-dpng', '-r500')
