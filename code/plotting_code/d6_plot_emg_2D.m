function d6_plot_emg_2D(ep_struct)
data_table          = p2_get_parameter_table(ep_struct);

valid_stim_idx      = data_table.frequency == 10;
valid_stim_idx      = valid_stim_idx & data_table.pulse_width == 60;
valid_nums          = 1:size(valid_stim_idx,1);
valid_nums          = valid_nums(valid_stim_idx);

stimulation_params  = data_table{valid_stim_idx,:};

regions             = unique(stimulation_params(:,1));
n_regions           = size(regions,1);
amplitudes          = flip(unique(stimulation_params(:,4)));

for c1 = 1:n_regions
    
    region_idx  = stimulation_params(:,1) == regions(c1,1);
    
    for c2 = 1:size(amplitudes,1)

        subplot(3,n_regions, c1 + n_regions*(c2-1))
        amp_idx     = stimulation_params(:,4) == amplitudes(c2);
        plot_idx    = region_idx & amp_idx;

        plot_num    = valid_nums(plot_idx);

        d6_plot_emg(ep_struct(plot_num))
        title(ep_struct(plot_num).stim_params(1:8));
    end
    
end

for c1 = 1:size(valid_nums,2)
    subplot(3,n_regions, c1)
    xlabel('milliseconds');
    ylabel('EMG Amp (mV)')
    
    set(gca,'fontSize', 10);
    drawnow
end
end

