function plot_bipolar(objective_model)

u_amplitudes    = [1 3 5]';
input_anode     = 0:.1:3;
input_cathode   = 0:.1:3;

for c1 = 1:size(u_amplitudes,1)
    input_amp       = u_amplitudes(c1);
    
    input_space     = combvec(input_cathode, input_anode, input_amp)';
    Y_est           = objective_model.predict(input_space);
    t1              = reshape(input_space(:,1), size(input_cathode,2), size(input_anode,2));
    t2              = reshape(input_space(:,2), size(input_cathode,2), size(input_anode,2));
    e1              = reshape(Y_est, size(input_cathode,2), size(input_anode,2));
    
    hold on
    surf(t1,t2,e1, 'LineStyle', 'none', 'FaceAlpha', .5)
%     scatter3(objective_model.x_data(:,1), objective_model.x_data(:,2), objective_model.y_data,...
%         'filled','MarkerFaceColor', 'k','MarkerEdgeColor', 'k')
    xlabel('Cathode')
    ylabel('Anode')
    set(gca, 'FontSize', 20)
    view(71,12)
        
    colormap(viridis)
    zlabel('cEP (µV)')
end
end

