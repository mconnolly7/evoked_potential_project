function d6_plot_ep_2D(ep_struct)
data_table          = p2_get_parameter_table(ep_struct);

valid_stim_idx      = data_table.frequency == 10;
valid_stim_idx      = valid_stim_idx & data_table.pulse_width == 60;
valid_nums          = 1:size(valid_stim_idx,1);
valid_nums          = valid_nums(valid_stim_idx);

stimulation_params  = data_table{valid_stim_idx,:};

regions             = unique(stimulation_params(:,1));
n_regions           = size(regions,1);
amplitudes          = flip(unique(stimulation_params(:,4)));

max_all             = 0;
min_all             = 0;

for c1 = 1:n_regions
    
    region_idx  = stimulation_params(:,1) == regions(c1,1);
    
    for c2 = 1:size(amplitudes,1)

        subplot(3,n_regions, c1 + n_regions*(c2-1))
        amp_idx     = stimulation_params(:,4) == amplitudes(c2);
        plot_idx    = region_idx & amp_idx;

        plot_num    = valid_nums(plot_idx);
        ep_model    = ep_struct(plot_num).ep_model_2D(1);
        
        ep_model.plot_mean();
        title(ep_struct(plot_num).stim_params(1:8));
        [~, y_max, ~, y_min] = ep_model.discrete_extrema(2);
        
        max_all = max(max_all, y_max);
        min_all = min(min_all, y_min);
    end
    
end

for c1 = 1:size(valid_nums,2)
    subplot(3,n_regions, c1)
    if any(c1 == [1 11 21])
        xlabel('Channels')
    end
    
    ylabel('milliseconds');
    
    set(gca,'fontSize', 10);
    view(90,-90)
    
    zlim([min_all max_all])
    xlim([min(ep_model.t(:,1)) max(ep_model.t(:,1))])
    ylim([min(ep_model.t(:,2)) max(ep_model.t(:,2))])
    caxis([min_all max_all]);
    
    colormap('jet')
    drawnow
end
end

