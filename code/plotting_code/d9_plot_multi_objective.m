function d9_plot_multi_objective(pareto_idx, pareto_idx_gt)

for c1 = 1:size(pareto_idx,3)
    pareto_trial                    = pareto_idx(:,:,c1);
    pareto_delta                    = pareto_trial == pareto_idx_gt';
    pareto_end(:,c1)                = pareto_trial(:,end);
    pareto_end_map(:,:,c1)          = reshape(pareto_end(:,c1),4, 50)';
    pareto_trajectory(:,c1)         = mean(pareto_delta);
    pareto_delta_end(:,c1)          = pareto_delta(:,end);
    pareto_delta_end_map(:,:,c1)    = reshape(pareto_delta_end(:,c1),4, 50)';
    
end

% subplot(1,3,1)
% plot(pareto_trajectory, 'color', .5*ones(1,3));
hold on

plot(prctile(pareto_trajectory',50));
plot(prctile(pareto_trajectory',25));
plot(prctile(pareto_trajectory',75));

% subplot(1,3,2)
% imagesc(1:4, .1:.1:5, (mean(pareto_delta_end_map,3)<.5))
% 
% subplot(1,3,3)
% imagesc(1:4, .1:.1:5, reshape(pareto_idx_gt,4,50)')
% xlim([.5 4.5])
end


