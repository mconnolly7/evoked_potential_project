% close all
input_cathode       = [0 1 2 3];
input_amplitude     = 0.1:.1:5;
input_space         = combvec(input_cathode, input_amplitude)';

load('/Users/mconn24/Downloads/Results_per_patient.mat')

close all
f               = figure( 'Position',  [100, 100, 800, 700]);
x_grid_1        = [0.1 .4 .7];
x_grid_2        = [0.1 .6  ];
y_grid_1        = [.6 .1];

width           = [.25 .35];
height          = [.35];


ax(1)        	= axes('Position', [x_grid_1(1)  y_grid_1(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid_1(2)  y_grid_1(1) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid_1(3)  y_grid_1(1) width(1) height(1)]);
ax(4)        	= axes('Position', [x_grid_2(1)  y_grid_1(2) width(2) height(1)]);
ax(5)        	= axes('Position', [x_grid_2(2)  y_grid_1(2) width(2) height(1)]);

%
% cEP
x1c = [.1737 .2616 .1257 .1257];
x2c = [.8074 .7786 -.5204 .5045];
x3c = [.6083 .4779 1.005 1.4331];

% mEP
x1m = [-0.5196	-0.4752	-0.5292	-0.5259];
x2m = [0.6115	0.5039	0.4596	0.5252];
x3m = [1.3436	3.0995	3.081	1.012];

[~, x1c_sort] = sort(x1c,'ascend');
[~, x2c_sort] = sort(x2c,'ascend');
[~, x3c_sort] = sort(x3c,'ascend');

[~, x1m_sort] = sort(x1m,'ascend');
[~, x2m_sort] = sort(x2m,'ascend');
[~, x3m_sort] = sort(x3m,'ascend');

color_idx       = [1  1    2 2 2     3 3      4 4 4];
face_alpha      = [1 .5    1 .5 .3   1 .5     1 .5 .3];

for c1 = 1:10
    set(f, 'currentaxes', ax(1));
    hold on

    plot(x1c(x1c_sort), results_per_patient.auc_mean(c1,x1c_sort),'Color', dark2(color_idx(c1)), 'linewidth', 2)
    xlabel('Cathode Length Scale')
    set(gca,'FontSize', 18)
    plot([0 100], [.5 .5], 'k--', 'LineWidth',3)
    xlim([min(x1c) max(x1c)])

    set(f, 'currentaxes', ax(2));
    hold on
    plot(x2c(x2c_sort), results_per_patient.auc_mean(c1,x2c_sort),'Color', dark2(color_idx(c1)), 'linewidth', 2)
    xlabel('Anode Length Scale')
    set(gca,'FontSize', 18)
    plot([-10 100], [.5 .5], 'k--', 'LineWidth',3)
    xlim([min(x2c) max(x2c)])


    set(f, 'currentaxes', ax(3));
    hold on
    plot(x3c(x3c_sort), results_per_patient.auc_mean(c1,x3c_sort),'Color', dark2(color_idx(c1)), 'linewidth', 2)
    xlabel('Amplitude Length Scale')
    set(gca,'FontSize', 18)
    xlim([min(x3c) max(x3c)])
    plot([0 100], [.5 .5], 'k--', 'LineWidth',3)
    xlim([min(x3c) max(x3c)])

    %%%%%%%
%     set(f, 'currentaxes', ax(4));
%     hold on
%     plot(x1m(x1m_sort), results_per_patient.auc_mean(c1,x1m_sort),'Color', dark2(color_idx(c1)), 'linewidth', 2)
%     xlabel('Cathode Length Scale')
%     set(gca,'FontSize', 18)
%     plot([0 100], [.5 .5], 'k--', 'LineWidth',3)
%     xlim([min(x1m) max(x1m)])
% 
%     set(f, 'currentaxes', ax(5));
%     hold on
%     plot(x2m(x2m_sort), results_per_patient.auc_mean(c1,x2m_sort),'Color', dark2(color_idx(c1)), 'linewidth', 2)
%     xlabel('Anode Length Scale')
%     set(gca,'FontSize', 18)
%     plot([-10 100], [.5 .5], 'k--', 'LineWidth',3)
%     xlim([min(x2m) max(x2m)])
% 
% 
%     set(f, 'currentaxes', ax(6));
%     hold on
%     plot(x3m(x3m_sort), results_per_patient.auc_mean(c1,x3m_sort),'Color', dark2(color_idx(c1)), 'linewidth', 2)
%     xlabel('Amplitude Length Scale')
%     set(gca,'FontSize', 18)
%     xlim([min(x3m) max(x3m)])
%     plot([0 100], [.5 .5], 'k--', 'LineWidth',3)
%     xlim([min(x3m) max(x3m)])
end


set(f, 'currentaxes', ax(4));
load('/Users/mconn24/repositories/evoked_potential_project/data/subject_models/3D/EP002_model.mat')
plot_bipolar(ep1_model)
title('Subject 1')

set(f, 'currentaxes', ax(5));
load('/Users/mconn24/repositories/evoked_potential_project/data/subject_models/3D/EP010_model.mat')
plot_bipolar(ep1_model)
title('Subject 4')
for c1 = [2 3]
    set(f, 'currentaxes', ax(c1));
    yticklabels([])
end

for c1 = [1 2 3]
    set(f, 'currentaxes', ax(c1));
    ylim([.55 .95])
end

for c1 = [1 2 3 4 5]
    set(f, 'currentaxes', ax(c1));
    set(gca,'FontSize', 16)
end

set(f, 'currentaxes', ax(1));
ylabel('Final AUC')

 
% set(f, 'currentaxes', ax(4));
% ylabel('Final AUC')
   



y_coords        = [.9 .4];
x_coords        = [.05 ];
sublabel_size   = 28;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','a','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(2) .1 .1],'String','b','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
print('length_scale.png', '-dpng', '-r500')
%%
clc

YY = reshape(results_per_patient.auc_mean,40,1);
X  = repmat(x1,10,1);
XX = reshape(X,[],1);
[rho, p] = corr(XX,YY, 'type','Spearman')

X  = repmat(x2,10,1);
XX = reshape(X,[],1);
[rho, p] = corr(XX,YY, 'type','Spearman')

X  = repmat(x3,10,1);
XX = reshape(X,[],1);
[rho, p] = corr(XX,YY, 'type','Spearman')

