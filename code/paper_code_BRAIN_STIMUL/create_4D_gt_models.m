function [ep1_models, emg_models] = create_4D_gt_models(subject_ids)

hyperprior_data_path    = 'evoked_potential_project/data/meta_modeling_data/hyperprior_data_4D.xlsx';
hyperprior_table        = readtable(hyperprior_data_path);

augmenting_data_path    = 'evoked_potential_project/data/meta_modeling_data/augmenting_data_4D.xlsx';
augmenting_table        = readtable(augmenting_data_path);

if ~exist('subject_ids', 'var')
    subject_ids             = [2 5 7 9 10 12];
end

[~, data_table]         = organize_data(subject_ids);


subject_ids             = unique(data_table.bipolar_subject);
close all
for c1 = 1:length(subject_ids)
    subject_idx         = data_table.bipolar_subject == subject_ids(c1);
    subject_table       = data_table(subject_idx,:);
    
    % Get subject specific augement data
    augment_idx         = augmenting_table.subject_id == subject_ids(c1);
    augment_anode       = augmenting_table.Anode(augment_idx);
    augment_cathode     = augmenting_table.Cathode(augment_idx);
    augment_amplitude   = augmenting_table.Amplitude(augment_idx);
    augment_x           = [augment_anode augment_cathode augment_amplitude];
    
    augment_ep1         = augmenting_table.EP1(augment_idx);
    augment_emg         = augmenting_table.EMG(augment_idx);
    
    % Get subject specific hyperprior data
    hyperprior_idx      = hyperprior_table.subject_id == subject_ids(c1);
    hyperprior_ep1_idx  = strcmp(hyperprior_table.objective,'EP1');
    hyperprior_emg_idx  = strcmp(hyperprior_table.objective,'EMG');
   
    hyperprior_ep1      = hyperprior_table(hyperprior_idx & hyperprior_ep1_idx,:);
    hyperprior_emg      = hyperprior_table(hyperprior_idx & hyperprior_emg_idx,:);
    
    coordinates         = subject_table.bipolar_coordinates; 
    amplitude           = subject_table.bipolar_amplitude;
    x_data              = [coordinates amplitude; augment_x];
    
    ep1                 = [subject_table.ep1_bipolar; augment_ep1];
    emg                 = [subject_table.emg_bipolar; augment_emg];
    
    % If all the outputs are the same value, the GP will not work
    % Covariance matrix will not be positive semidefinite
    rng(subject_ids(c1))
    if all(emg == 0) 
       emg = emg + randn(size(emg))/4; 
    end
    
    % Augment with pulse width
    x_data_pulse_width  = [repmat(20, size(x_data,1),1); repmat(60, size(x_data,1),1); repmat(120, size(x_data,1),1)];
    x_data              = [repmat(x_data,3,1) x_data_pulse_width];
    
    ep1                 = [ep1 * pulse_width_sigmoid(20); ep1 * pulse_width_sigmoid(60); ep1 * pulse_width_sigmoid(120)];
    emg                 = [emg * pulse_width_sigmoid(20); emg * pulse_width_sigmoid(60); emg * pulse_width_sigmoid(120)];
    
    % Set up ground truth models
    ep1_model                       = gp_object();
    ep1_model.initialize_data(x_data, ep1);
    ep1_model.hyperparameters.mean  = hyperprior_ep1.mean;
    ep1_model.hyperparameters.lik   = hyperprior_ep1.lik;
    ep1_model.hyperparameters.cov   = [hyperprior_ep1.cov1; hyperprior_ep1.cov2; hyperprior_ep1.cov3; hyperprior_ep1.cov4; hyperprior_ep1.cov5];

    emg_model                       = gp_object();
    emg_model.initialize_data(x_data, emg);
    emg_model.hyperparameters.mean  = hyperprior_emg.mean;
    emg_model.hyperparameters.lik   = hyperprior_emg.lik;
    emg_model.hyperparameters.cov(1:4)   = [hyperprior_emg.cov1; hyperprior_emg.cov2; hyperprior_emg.cov3; hyperprior_emg.cov4];
    
    ep1_models(c1) = ep1_model;
    ep1_model(c1).minimize(1);
    
    figure;
    plot_4D_model(ep1_models);
    
    emg_models(c1) = emg_model;
    emg_model(c1).minimize(1);

    figure;
    plot_4D_model(emg_models);
end

end

function pw_multiplier = pulse_width_sigmoid(x)


% x = -0:.01:120;
k               = .1;
pw_multiplier   = 1./(1+exp(-k*x+4));

end

