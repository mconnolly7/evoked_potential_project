function [auc_trajectory_all, e_rate_auc] = calculate_auc_trajectory(data_struct)

optimization_data       = data_struct.optimization_results;
optimization_config     = data_struct.optimization_config;

n_trials                = optimization_config.n_trials;
n_samples               = optimization_config.n_samples;
sample_number          	= 1:n_samples-1;

ep1_model               = optimization_config.objective_1;
emg_model               = optimization_config.objective_2;
input_space             = optimization_config.input_space;

y_gt(:,1)               = ep1_model.predict(input_space);
y_gt(:,2)               = emg_model.predict(input_space);
                
[~, dom_order_gt]       = get_pareto_2(1, -1, y_gt);

for c1 = 1:n_trials % Trial

    dom_order_est               = optimization_data(c1).dom_order_est;

    for c2 = 1:size(dom_order_est,2) % Sample
        auc_trajectory(c2)      = pareto_roc_2(dom_order_est(:,c2), dom_order_gt);
    end

    auc_trajectory_all(:,c1)   	= auc_trajectory;

    fit_model                   = fit(sample_number', 1-auc_trajectory', 'exp1', 'Startpoint',[0 0]);
    e_rate_auc(c1)              = fit_model.b;  

end

end

