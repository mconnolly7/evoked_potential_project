load('evoked_potential_project/data/simulation_data/jne_data_2020_06_29/2D/ehvi_model_2D_EP002.mat')
% load('random_model_2D_EP002.mat')

%%

writerObj = VideoWriter('myVideo.avi');
writerObj.FrameRate = 4;
% set the seconds per image
%  secsPerImage = [5 10 15];
% open the video writer
open(writerObj);
close all
% trials = [4, 5, 21, 23, 22, 27, 28, 29];
trials = [4];
for trial_idx = 1:size(trials,2)
    % trial_idx = trials(trial_idx);
    
    cep_model       = optimization_config.objective_1;
    mep_model       = optimization_config.objective_2;
    input_space     = optimization_config.input_space;
    
    costs_est(:,1)  = cep_model.predict(input_space);
    costs_est(:,2)  = mep_model.predict(input_space);
    
    [pareto_idx, dom_order_gt] = get_pareto_2(1, -1, costs_est);
    
    trial_results   = optimization_results(trials(trial_idx));
    X_sample        = trial_results.X_sample;
    objective_est   = trial_results.objective_est;
    dom_order       = trial_results.dom_order_est;
    Y_est           = trial_results.Y_est;
            
    figure('Position', [680 1 2000 600])

    t_points        = 1:99;[5 15 99];
    for c1 = 1:size(t_points,2)
        
        t               = t_points(c1);
        
        %     subplot(4,3,c1)
        subplot(1,4,1)
        cla
        objective_est(t,1).plot_mean
        objective_est(t,1).plot_data
        colormap(viridis)
        
        ylabel('Amplitude (mA)');
        zlabel('cEP (µV)')
        xlim([0 3])
        ylim([0 5])
        zlim([0 6.5])
        
        xticks([0 1 2 3])
        xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})
        title(sprintf('%d Samples', t))
        
        set(gca,'FontSize', 14)
        
        
        %     subplot(4,3,c1+3)
        subplot(1,4,2)
        cla
        objective_est(t,2).plot_mean
        objective_est(t,2).plot_data
        colormap(viridis)
        ylabel('Amplitude (mA)');
        zlabel('mEP (µV)')
        xlim([0 3])
        ylim([0 5])
        zlim([-2 14])
        xticks([0 1 2 3])
        xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})
        set(gca,'FontSize', 14)
        
        
        %     subplot(4,3,c1+6)
        subplot(1,4,3)
        cla
        Y_est_t         = Y_est(:,:,t);
        dom_order_t     = dom_order(:,t);
        
        max_dom_order   = 2;
        
        dom_color_map   = [255,255,178
            254,204,92
            253,141,60
            240,59,32
            189,0,38]/255;
        
        dom_color_map = flip(dom_color_map);
        
        hold on
        for c2 = 0:max_dom_order
            plot_idx = dom_order_t == c2;
            scatter(Y_est_t(plot_idx,1),Y_est_t(plot_idx,2),100,dom_color_map(c2+1,:),'filled', 'MarkerFaceAlpha',.7)
        end
        
        plot_idx = dom_order_t > c2;
        scatter(Y_est_t(plot_idx,1),Y_est_t(plot_idx,2),100,[.5 .5 .5])
        
        scatter(costs_est(dom_order_gt == 0,1), costs_est(dom_order_gt == 0,2), 30, 'k','filled')
        
        xlabel('cEP (µV)')
        ylabel('mEP (µV)')
        xlim([1 6])
        ylim([0 10])
        set(gca,'FontSize', 14)
        
        
        
        %     subplot(4,3,c1+9)
        subplot(1,4,4)
        cla
        hold on
        
        for c2 = 0:max(dom_order_t)
            dom_idx = find(dom_order_t == c2);
            for c3 = 1:size(dom_idx,1)
                input_d = input_space(dom_idx(c3),:);
                xx = [input_d(1) - 0.4,input_d(1) + 0.4,input_d(1) + 0.4,input_d(1) - 0.4];
                yy = [input_d(2) - .04, input_d(2) - .04, input_d(2) + .04, input_d(2) + .04];
                
                if c2 > max_dom_order
                    patch(xx,yy,[1 1 1], 'EdgeColor', [.5 .5 .5])
                else
                    patch(xx,yy,dom_color_map(c2+1,:), 'FaceAlpha', 1, 'edgecolor', dom_color_map(c2+1,:))
                end
            end
        end
        
        scatter(input_space(dom_order_gt == 0,1), input_space(dom_order_gt == 0,2), 20, [0 0 0],'filled');
        ylim([0 5])
        xlim([-.8 3.8])
        xticks([0 1 2 3])
        xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})
        ylabel('Amplitude (mA)')
        set(gca,'FontSize',14)
        drawnow
        
        frame = getframe(gcf) ;
        writeVideo(writerObj, frame);
%         print( sprintf('trajectory_%d.png', c1),'-dpng', '-r500')

    end
    close(writerObj);
    
end
%%
for c1 = 1:3
    subplot(4,1,c1)
    plot(X_sample(:,c1))
end