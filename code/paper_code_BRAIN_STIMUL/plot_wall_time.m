function plot_wall_time
results_4D  	= load('evoked_potential_project/results/BRAIN_STIMUL_results/AUC_trajectory_results_4D_all.mat');
close all
max_time        = 25*60;
compute_time    = 15;
dark_2 = [27,158,119
217,95,2
117,112,179
231,41,138]/255;

t1              = 15:15:max_time-15;
t2              = [15:15:150 180:30:max_time];
tt1             = [t1 flip(t1)];
tt2             = [t2 flip(t2)];

auc_trajectory_4D   = results_4D.auc_trajectory_all;

[trajectory_mean_4D_rand, trajectory_sterr_4D_rand] = dimension_trajectory(auc_trajectory_4D,3);
[trajectory_mean_4D_ehvi, trajectory_sterr_4D_ehvi] = dimension_trajectory(auc_trajectory_4D,4);

hold on
% plot(t1, trajectory_mean_4D_rand(1:size(t1,2)));
% plot(t2, trajectory_mean_4D_ehvi(1:size(t2,2)));

h(1) = plot(t1, trajectory_mean_4D_rand(1:size(t1,2)), 'color', dark_2(3,:), 'LineWidth', 3);
yy = [trajectory_mean_4D_rand(1:size(t1,2)) - trajectory_sterr_4D_rand(1:size(t1,2)) ...
    flip(trajectory_mean_4D_rand(1:size(t1,2)) + trajectory_sterr_4D_rand(1:size(t1,2)))];
h(2) = patch(tt1, yy, dark_2(3,:), 'FaceAlpha', .4, 'EdgeColor', 'none');

h(3) = plot(t2, trajectory_mean_4D_ehvi(1:size(t2,2)), 'color', dark_2(4,:), 'LineWidth', 3);
yy = [trajectory_mean_4D_ehvi(1:size(t2,2)) - trajectory_sterr_4D_ehvi(1:size(t2,2))...
    flip(trajectory_mean_4D_ehvi(1:size(t2,2)) + trajectory_sterr_4D_ehvi(1:size(t2,2)))];
h(4) = patch(tt2, yy, dark_2(4,:), 'FaceAlpha', .4, 'EdgeColor', 'none');

hh = plot([0 max_time], [.5 .5], 'k--', 'LineWidth',3);
xlim([1 max_time])
ylim([0 1])
box off
legend([h hh], {'Rand+Model','','EHVI+Hyperprior','','50% Chance'}, 'box' ,'off')
set(gca, 'FontSize', 14)
xlabel('Seconds')
ylabel('AUC')
end

