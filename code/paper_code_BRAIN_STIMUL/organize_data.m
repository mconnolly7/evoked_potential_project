function [monopolar_data_table, bipolar_data_table] = organize_data(subject_ids)

data_path = '/Users/mconn24/Box Sync/EP_data_for_Mark/EPdata_forMarkJan2020.mat';
load(data_path)

if ~exist('subject_ids', 'var') || isempty(subject_ids)
    subject_ids             = [2 5 7 9 10 12 14 15 17 18]';
end

monopolar_data_table    = [];
bipolar_data_table      = [];

for c1 = 1:length(subject_ids)
    EP_data         = EP(subject_ids(c1));
    
    n_stimulations  = size(EP_data.stimSettings,2);
    ep1_data        = max(EP_data.EP1ramp,[],2);
    emg_data        = max(squeeze(max(EP_data.relativeAmplitudesEMG)),[],2);
    
    parameter_table = [];

    for c2 = 1:n_stimulations
        parameter_string    = EP_data.stimSettings{c2};
        parameter_row       = process_stim_parameter_string(parameter_string);
        parameter_table     = [parameter_table; parameter_row];
    end
    
    
    % Remove the invalid stimulations
    valid_idx               = parameter_table.frequency == 10;
    valid_idx               = valid_idx & parameter_table.pulse_width_a == 60;
    
    parameter_table         = parameter_table(valid_idx,:);
    ep1_data                = ep1_data(valid_idx,:);
    emg_data                = emg_data(valid_idx,:);
    
    % Collect and model the monopolar data
    monopolar_idx           = ~isnan(parameter_table.monopolar_coordinates);
    monopolar_coordinates   = parameter_table.monopolar_coordinates(monopolar_idx);
    monopolar_amplitude     = parameter_table.amplitude(monopolar_idx);
    
    ep1_monopolar          	= ep1_data(monopolar_idx,:);
    emg_monopolar        	= emg_data(monopolar_idx,:);
    
    monopolar_subject       = repmat(subject_ids(c1),size(ep1_monopolar));
    monopolar_table_subject = table(monopolar_subject, monopolar_coordinates, monopolar_amplitude, ep1_monopolar, emg_monopolar);
    monopolar_data_table    = [monopolar_data_table; monopolar_table_subject];
    
    % Collect and model the bipolar data
    bipolar_idx             = all(~isnan(parameter_table.bipolar_coordinates),2);
    if all(bipolar_idx)
        bipolar_coordinates     = parameter_table.bipolar_coordinates(bipolar_idx,:);
        bipolar_amplitude       = parameter_table.amplitude(bipolar_idx);

        ep1_bipolar          	= ep1_data(bipolar_idx,:);
        emg_bipolar             = emg_data(bipolar_idx,:);

        bipolar_subject         = repmat(subject_ids(c1),size(ep1_bipolar));

        bipolar_table_subject   = table(bipolar_subject, bipolar_coordinates, bipolar_amplitude, ep1_bipolar, emg_bipolar);
        bipolar_data_table      = [bipolar_data_table; bipolar_table_subject];
    end
end

end


