function [ep1_models, emg_models] = create_bipolar_gt_models(subject_ids)

hyperprior_data_path    = 'evoked_potential_project/data/meta_modeling_data/hyperprior_data_3D.xlsx';
hyperprior_table        = readtable(hyperprior_data_path);

augmenting_data_path    = 'evoked_potential_project/data/meta_modeling_data/augmenting_data_3D.xlsx';
augmenting_table        = readtable(augmenting_data_path);

if ~exist('subject_ids', 'var')
    subject_ids             = [2 5 7 9 10 12];
end

[~, data_table]         = organize_data(subject_ids);


subject_ids             = unique(data_table.bipolar_subject);

for c1 = 1:length(subject_ids)
    subject_idx         = data_table.bipolar_subject == subject_ids(c1);
    subject_table       = data_table(subject_idx,:);
    
    % Get subject specific augement data
    augment_idx         = augmenting_table.subject_id == subject_ids(c1);
    augment_anode       = augmenting_table.Anode(augment_idx);
    augment_cathode     = augmenting_table.Cathode(augment_idx);
    augment_amplitude   = augmenting_table.Amplitude(augment_idx);
    augment_x           = [augment_anode augment_cathode augment_amplitude];
    
    augment_ep1         = augmenting_table.EP1(augment_idx);
    augment_emg         = augmenting_table.EMG(augment_idx);
    
    % Get subject specific hyperprior data
    hyperprior_idx      = hyperprior_table.subject_id == subject_ids(c1);
    hyperprior_ep1_idx  = strcmp(hyperprior_table.objective,'EP1');
    hyperprior_emg_idx  = strcmp(hyperprior_table.objective,'EMG');
   
    hyperprior_ep1      = hyperprior_table(hyperprior_idx & hyperprior_ep1_idx,:);
    hyperprior_emg      = hyperprior_table(hyperprior_idx & hyperprior_emg_idx,:);
    
    coordinates         = subject_table.bipolar_coordinates; 
    amplitude           = subject_table.bipolar_amplitude;
    x_data              = [coordinates amplitude; augment_x];
    
    ep1                 = [subject_table.ep1_bipolar; augment_ep1];
    emg                 = [subject_table.emg_bipolar; augment_emg];
    
    % If all the outputs are the same value, the GP will not work
    % Covariance matrix will not be positive semidefinite
    rng(subject_ids(c1))
    if all(emg == 0) 
       emg = emg + randn(size(emg))/4; 
    end
    
    % Set up ground truth models
    ep1_model                       = gp_object();
    ep1_model.initialize_data(x_data, ep1);
    ep1_model.hyperparameters.mean  = hyperprior_ep1.mean;
    ep1_model.hyperparameters.lik   = hyperprior_ep1.lik;
    ep1_model.hyperparameters.cov   = [hyperprior_ep1.cov1; hyperprior_ep1.cov2; hyperprior_ep1.cov3; hyperprior_ep1.cov4];

    emg_model                       = gp_object();
    emg_model.initialize_data(x_data, emg);
    emg_model.hyperparameters.mean  = hyperprior_emg.mean;
    emg_model.hyperparameters.lik   = hyperprior_emg.lik;
    emg_model.hyperparameters.cov   = [hyperprior_emg.cov1; hyperprior_emg.cov2; hyperprior_emg.cov3; hyperprior_emg.cov4];
    
    ep1_models(c1) = ep1_model;
    emg_models(c1) = emg_model;
end



