%% % 
% 
% Process 2D data
%
% %%
% results_path            = 'evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/';
results_path            = '';

model_free_EP002        = load([results_path '2D_rand_model_free_EP002.mat']);
model_free_EP005        = load([results_path '2D_rand_model_free_EP005.mat']);
model_free_EP007        = load([results_path '2D_rand_model_free_EP007.mat']);
model_free_EP009        = load([results_path '2D_rand_model_free_EP009.mat']);
model_free_EP010        = load([results_path '2D_rand_model_free_EP010.mat']);
model_free_EP012        = load([results_path '2D_rand_model_free_EP012.mat']);

model_free_data         = [model_free_EP002.data_struct; model_free_EP005.data_struct; model_free_EP007.data_struct;
                        model_free_EP009.data_struct; model_free_EP010.data_struct;model_free_EP012.data_struct];

%
grid_model_EP002        = load([results_path '2D_grid_model_EP002.mat']);
grid_model_EP005        = load([results_path '2D_grid_model_EP005.mat']);
grid_model_EP007        = load([results_path '2D_grid_model_EP007.mat']);
grid_model_EP009        = load([results_path '2D_grid_model_EP009.mat']);
grid_model_EP010        = load([results_path '2D_grid_model_EP010.mat']);
grid_model_EP012        = load([results_path '2D_grid_model_EP012.mat']);

grid_model_data         = [grid_model_EP002.data_struct; grid_model_EP005.data_struct; grid_model_EP007.data_struct;
                        grid_model_EP009.data_struct; grid_model_EP010.data_struct; grid_model_EP012.data_struct];

                    
%
rand_model_EP002        = load([results_path '2D_rand_model_EP002.mat']);
rand_model_EP005        = load([results_path '2D_rand_model_EP005.mat']);
rand_model_EP007        = load([results_path '2D_rand_model_EP007.mat']);
rand_model_EP009        = load([results_path '2D_rand_model_EP009.mat']);
rand_model_EP010        = load([results_path '2D_rand_model_EP010.mat']);
rand_model_EP012        = load([results_path '2D_rand_model_EP012.mat']);

rand_model_data         = [rand_model_EP002.data_struct; rand_model_EP005.data_struct; rand_model_EP007.data_struct;
                        rand_model_EP009.data_struct; rand_model_EP010.data_struct; rand_model_EP012.data_struct];

%
ehvi_hyper_EP002        = load([results_path '2D_EHVI_EP002.mat']);
ehvi_hyper_EP005        = load([results_path '2D_EHVI_EP005.mat']);
ehvi_hyper_EP007        = load([results_path '2D_EHVI_EP007.mat']);
ehvi_hyper_EP009        = load([results_path '2D_EHVI_EP009.mat']);
ehvi_hyper_EP010        = load([results_path '2D_EHVI_EP010.mat']);
ehvi_hyper_EP012        = load([results_path '2D_EHVI_EP012.mat']);

ehvi_hyper_data         = [ehvi_hyper_EP002.data_struct; ehvi_hyper_EP005.data_struct; ehvi_hyper_EP007.data_struct;
                        ehvi_hyper_EP009.data_struct; ehvi_hyper_EP010.data_struct; ehvi_hyper_EP012.data_struct];
%%                    
input_cathode                       = 0:3;
input_amplitudes                    = .1:.25:5;

input_space                         = combvec(input_cathode, input_amplitudes)';

data_struct                         = [model_free_data grid_model_data rand_model_data ehvi_hyper_data];
[auc_trajectory_all, e_rate_auc]    = calculate_trajectory(data_struct, input_space);

%% % 
% 
% Process 3D data
%
% %%
% full_model_data     = load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/component_test_2020_04_19.mat');
% model_free_data     = load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/model_free_test_2020_04_20.mat');
% grid_search_data    = load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/grid_search_component_test_2020_04_20.mat');
load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/3D_all_components.mat')
%%

for c1 = 1:size(data_struct_3D,1)
    for c2 = 1:size(data_struct_3D,2) 
        
        data_struct_lite.dom_order_est  = data_struct_3D(c1,c2).dom_order_est;
        data_struct_lite.Y_est          = data_struct_3D(c1,c2).Y_est;
        data_struct_lite.Y_sample       = data_struct_3D(c1,c2).Y_sample;
        data_struct_lite.ep1_model      = data_struct_3D(c1,c2).ep1_model;
        data_struct_lite.emg_model      = data_struct_3D(c1,c2).emg_model;
        
        data_struct(c1,c2) = data_struct_lite;
    end
end

input_cathode               = 0:3;
input_anode                 = 0:3;
input_amplitudes            = .1:.25:5;

input_space                 = combvec(input_cathode, input_anode, input_amplitudes)';

[auc_trajectory_all, e_rate_auc] = calculate_trajectory(data_struct, input_space);


%% % 
% 
% Process 4D data
%
% %%

EHVI_data           = load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/4D_EHVI_all.mat');  
grid_data           = load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/4D_grid_all.mat');  
random_model_data  	= load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/4D_rand_model_all.mat');  
random_free_data   	= load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/4D_rand_model_free_all.mat');  

data_struct_4D      = [random_free_data.data_struct grid_data.data_struct random_model_data.data_struct EHVI_data.data_struct_4D];

for c1 = 1:size(data_struct_4D,1)
    for c2 = 1:size(data_struct_4D,2) 
        
        data_struct_lite.dom_order_est  = data_struct_4D(c1,c2).dom_order_est;
        data_struct_lite.Y_est          = data_struct_4D(c1,c2).Y_est;
        data_struct_lite.Y_sample       = data_struct_4D(c1,c2).Y_sample;
        data_struct_lite.ep1_model      = data_struct_4D(c1,c2).ep1_model;
        data_struct_lite.emg_model      = data_struct_4D(c1,c2).emg_model;
        
        data_struct(c1,c2) = data_struct_lite;
    end
end

input_cathode               = 0:3;
input_anode                 = 0:3;
input_amplitudes            = .1:.25:5;
input_pulse_width           = 20:20:120;

input_space                 = combvec(input_cathode, input_anode, input_amplitudes)';
input_space                 = combvec(input_space', input_pulse_width)';

[auc_trajectory_all, e_rate_auc] = calculate_trajectory(data_struct, input_space);



