clear; clc; 
% close all;

f                           = figure( 'Position',  [100, 100, 1500, 500]);
x_grid                      = [0.125    0.385    0.685];
y_grid_1                    = .1;

width                       = .25;
height                      = .8;

ax(1)                       = axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(2)                       = axes('Position', [x_grid(2)  y_grid_1(1) width(1) height(1)]);
ax(3)                       = axes('Position', [x_grid(3)  y_grid_1(1) width(1) height(1)]);

results_2D                	= load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_2D_all.mat');
results_3D                 	= load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_3D_all.mat');
results_4D                 	= load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_4D_all.mat');

optimization_results_2D    	= results_2D.optimization_results;
optimization_results_3D   	= results_3D.optimization_results;
optimization_results_4D  	= results_4D.optimization_results;

n_trials                  	= optimization_results_2D(1).optimization_config.n_trials;
n_samples                   = optimization_results_2D(1).optimization_config.n_samples;

for c1 = 1:10;size(optimization_results_2D,2);
    auc_trajectory_all_2D{c1} = [];
    auc_trajectory_all_3D{c1} = [];
    auc_trajectory_all_4D{c1} = [];
end

for c1 = 1:size(optimization_results_2D,2)
    algorithm_name      = optimization_results_2D(c1).algorithm_config.name;
    auc_trajectory_2D   = optimization_results_2D(c1).auc_trajectory;
    auc_trajectory_3D   = optimization_results_3D(c1).auc_trajectory;
    auc_trajectory_4D   = optimization_results_4D(c1).auc_trajectory;
    
    if isempty(algorithm_name)
        algorithm_name  = 'evolution_model_hyperprior';
    end
    
    switch algorithm_name
        case 'grid'
            algorithm_code(c1) = 10;
        case 'grid_model'
         	algorithm_code(c1) = 9;
        case 'grid_model_hyperprior'
            algorithm_code(c1) = 8;
        
        case 'evolution_model'
            algorithm_code(c1) = 7;
        case 'evolution_model_hyperprior'
            algorithm_code(c1) = 6;
        
        case 'random'
            algorithm_code(c1) = 5;
        case 'random_model'
            algorithm_code(c1) = 4;
        case 'random_model_hyperprior'
            algorithm_code(c1) = 3;      
        
        case 'ehvi_model'
            algorithm_code(c1) = 2;
        case 'ehvi_model_hyperprior'
            algorithm_code(c1) = 1;
    end
    
    auc_trajectory_all_2D{algorithm_code(c1)}  = [auc_trajectory_all_2D{algorithm_code(c1)} auc_trajectory_2D];
    auc_trajectory_all_3D{algorithm_code(c1)}  = [auc_trajectory_all_3D{algorithm_code(c1)} auc_trajectory_3D];
    auc_trajectory_all_4D{algorithm_code(c1)}  = [auc_trajectory_all_4D{algorithm_code(c1)} auc_trajectory_4D];
end

%
%
%
set(f, 'currentaxes', ax(1));

algorithm_plot = [1 3 6 8];

auc_trajectory_all(1:4)     = auc_trajectory_all_2D(algorithm_plot);
auc_trajectory_all(5:8)     = auc_trajectory_all_3D(algorithm_plot);
auc_trajectory_all(9:12)    = auc_trajectory_all_4D(algorithm_plot);


sample_number               = (1:n_samples-1)';
auc_final                   = nan(10,120);


for c1 = 1:size(auc_trajectory_all,2)
    [e_mean(c1), e_ci(:,c1)]   	= error_convergence_rate_2(sample_number, 1-auc_trajectory_all{c1});
    auc_final(c1,1:size(auc_trajectory_all{c1}(end,:),2))          	= auc_trajectory_all{c1}(end,:);
end

auc_mean        = nanmean(auc_final,2);
auc_stdv        = std(auc_final,[],2);
auc_ster        = auc_stdv / sqrt(size(auc_final,2));
auc_ci95        = auc_ster * 1.96;

x               = [0 .5 1 1.5,   2.5 3 3.5 4,  5 5.5 6 6.5];
color_idx       = [1 2 3 4,      1 2 3 4,      1 2 3 4];
face_alpha      = [1 1 1 1,      1 1 1 1,      1 1 1 1];
y               = auc_mean;
grey_idx        = [.3 1 1 1,    .5 1 1 1,      .7 1 1 1];


hold on 
for c1 = 1:length(x)
    if mod(c1,4) == 1
        plot_handle(c1) = bar(x(c1),y(c1), 'BarWidth', .4, 'FaceColor', grey_idx(c1)*ones(1,3), 'FaceAlpha', face_alpha(c1), 'linewidth', 1);
        plot([x(c1) x(c1)], auc_mean(c1) + auc_ci95(c1)*[1 -1], 'color', 'k', 'lineWidth', 2);
    else
        plot_handle(c1) = bar(x(c1),y(c1), 'BarWidth', .4, 'FaceColor', dark2(color_idx(c1)), 'FaceAlpha', face_alpha(c1), 'linewidth', 1);
        plot([x(c1) x(c1)], auc_mean(c1) + auc_ci95(c1)*[1 -1], 'color', 'k', 'lineWidth', 2);
    end
end

plot([-.5 100], [.5 .5], 'k--', 'LineWidth',3)
xlim([-.5 7]);
ylim([0.4 0.95])
view(90, 90)
ylabel('Final AUC')
xticks([.75 3.25 5.75])
xticklabels({'Monopolar (2D)' ,'Bipolar (3D)','Bipolar/Pulse Width (4D)'});
% set(gca,'TickLabelInterpreter', 'latex');
set(gca, 'FontSize', 14)

%
%
%
set(f, 'currentaxes', ax(2));
hold on 
for c1 = 1:length(x)
    if mod(c1,4) == 1
        plot_handle(c1) = bar(x(c1),e_mean(c1), 'BarWidth', .4, 'FaceColor', grey_idx(c1)*ones(1,3), 'FaceAlpha', face_alpha(c1), 'linewidth', 1);
        plot([x(c1) x(c1)], e_ci(:,c1), 'color', 'k', 'lineWidth', 2);
    else
        plot_handle(c1) = bar(x(c1),e_mean(c1), 'BarWidth', .4, 'FaceColor', dark2(color_idx(c1)), 'FaceAlpha', face_alpha(c1), 'linewidth', 1);
        plot([x(c1) x(c1)], e_ci(:,c1), 'color', 'k', 'lineWidth', 2);
    end
end

view(-90, -90)
xticks(x)
xticklabels([])
ylabel('Convergence')
set(gca, 'FontSize', 14)
ylim([-0.025 0])
xlim([-.5 7]);

l = legend(plot_handle([1 2 3 4]), ...
    {'EHVI+Hyperprior','Random+Hyperprior','Evolution+Hyperprior','Grid+Hyperprior'},...
    'box', 'off', 'Position', [0.5337 0.12 0.1360 0.1310]);

%
%
%
set(f, 'currentaxes', ax(3));

algorithm_plot = [1 5 9];

hold on

t       = 1:99;
tt      = [t flip(t)];

line_style = {'-', ':', '-.', '--'};
grey_idx = [.3 .5 .7 1];
for c1 = 1:size(algorithm_plot,2)
    algorithm_trajectory    = auc_trajectory_all{algorithm_plot(c1)};
    trajectory_mean         = mean(algorithm_trajectory,2);
    trajectory_stdv         = std(algorithm_trajectory,[],2);
    trajectory_sterr        = trajectory_stdv / sqrt(size(algorithm_trajectory,2));
    trajectory_ci95         = trajectory_sterr *1.96;
    
    yy = [trajectory_mean + trajectory_ci95; flip(trajectory_mean - trajectory_ci95)];
    patch(tt,yy, grey_idx(c1)*ones(1,3), 'FaceAlpha', 0.5, 'EdgeColor', 'none')
    plot(trajectory_mean, 'Color', grey_idx(c1)*ones(1,3), 'linewidth', 3, 'LineStyle', line_style{c1})
    
end

plot([0 99], [.5 .5], 'k--', 'LineWidth',3)
xlim([1 99])
set(gca, 'FontSize', 14)
xlabel('Samples Collected')
ylabel('AUC')

x_legend    = [40 50];
y_legend    = [.40 .36 .32 .28];
height      = 0.015;
x_text      = 51;
xx          = [x_legend flip(x_legend)];
legend_labels = {'Monopolar (2D)', 'Bipolar (3D)', 'Bipolar/Pulse Width (4D)', '0.50 AUC'};

for c1 = 1:3
    yy = [y_legend(c1)-height y_legend(c1)-height y_legend(c1)+height y_legend(c1)+height];
    patch(xx,yy,grey_idx(c1)*ones(1,3), 'FaceAlpha', 0.5, 'EdgeColor', 'none')
    plot(x_legend, [y_legend(c1) y_legend(c1)], 'Color', grey_idx(c1)*ones(1,3), 'linewidth', 3, 'LineStyle', line_style{c1})
    text(x_text, y_legend(c1), legend_labels{c1}, 'FontSize', 14)

end
plot(x_legend, [y_legend(4) y_legend(4)], 'Color', 'k', 'linewidth', 3, 'LineStyle', line_style{4})
text(x_text, y_legend(4), legend_labels{4}, 'FontSize', 14)

% l = legend({'2 Dimensions', '', '3 Dimensions', '', '4 Dimensions', '', '0.50 AUC'}, 'box', 'off', 'Position',[0.8487 0.1200 0.0787 0.2240])

y_coords        = [.86];
x_coords        = [.1 .64];
sublabel_size   = 28;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','a','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','b','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);


