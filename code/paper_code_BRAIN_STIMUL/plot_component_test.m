%%
% load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/component_test_2020_04_19.mat')
%%
% full_model_data     = data_struct;

%%
% load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/model_free_test_2020_04_20.mat')
% model_free_data     = data_struct;
% 
% load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/grid_search_component_test_2020_04_20.mat')
% grid_search_data    = data_struct;
% 

%%
% data_struct = [model_free_data grid_search_data full_model_data(:,[1 4])];
%%

%%
load('evoked_potential_project/data/simulation_data/brain_stimul_data_2020_04_20/3D_all_components.mat')

%%
% Construct the input space
data_struct = data_struct_3D;

input_cathode               = 0:3;
input_anode                 = 0:3;
input_amplitudes            = .1:.25:5;
input_pulse_width           = 20:20:120;
input_space                 = combvec(input_cathode, input_anode, input_amplitudes)';

sample_number               = 1:99;
sample_compare              = 99;

for c1 = 1:size(data_struct,1) % Subject
    ep1_model       = data_struct(c1,1).ep1_model;
    emg_model       = data_struct(c1,1).emg_model;
    
    y_gt(:,1)       = ep1_model.predict(input_space);
    y_gt(:,2)       = emg_model.predict(input_space);
            
    [pareto_gt, dom_order_gt]	= get_pareto_2(1, -1, y_gt);

    for c2 = 1:size(data_struct,2) % Configuration
        
        experiment_data     = data_struct(c1,c2);
        dom_order_est       = experiment_data.dom_order_est;
        costs_est           = experiment_data.Y_est;
        Y_sample            = experiment_data.Y_sample;
        
        for c3 = 1:size(dom_order_est,3) % Trial
%             auc_final(c1,c2,c3)     = pareto_roc_2(dom_order_est(:,sample_compare,c3), dom_order_gt);   
            
            if isnan(costs_est)
                cost_trial          = costs_est(:,:,sample_compare,c3);
            else
                cost_trial          = Y_sample;
            end
            
%             pareto_est              = dom_order_est == 0;
%             pareto_est              = get_pareto_2(1,-1,cost_trial);          
%             pareto_dist             = pdist2(cost_trial(pareto_est,:), y_gt(pareto_gt,:));
%             dist_final(c1,c2,c3)    = mean(min(pareto_dist,[],2));
            
            for c4 = 1:size(dom_order_est,2)
                auc_trajectory(c4)      = pareto_roc_2(dom_order_est(:,c4,c3), dom_order_gt);
                
%                 cost_trial              = costs_est(:,:,c4,c3);
%                 pareto_est              = get_pareto_2(1,-1,cost_trial);          
%                 pareto_dist             = pdist2(cost_trial(pareto_est,:), y_gt(pareto_gt,:));
%                 dist_trajectory(c4)     = mean(min(pareto_dist,[],2));
            end
            
            auc_trajectory_all(c1,c2,c3,:)      = auc_trajectory;
%             dist_trajectory_all(c1,c2,c3,:)     = dist_trajectory;
            
            fit_model               = fit(sample_number', 1-auc_trajectory', 'exp1', 'Startpoint',[0 0]);
            e_rate_auc(c1,c2,c3)  	= fit_model.b;  
%             
%             fit_model               = fit(sample_number', dist_trajectory', 'exp1', 'Startpoint',[0 0]);
%             e_rate_dist(c1,c2,c3)  	= fit_model.b;
        end
        
    end
    
end


%%
dark_2 = [27,158,119
217,95,2
117,112,179
231,41,138]/255;

close all
sample_compare = 99;
mean_auc    = mean(auc_trajectory_all(:,:,:,sample_compare), 3);
stdv_auc    = std(auc_trajectory_all(:,:,:,sample_compare),[], 3);
stdr_auc    = stdv_auc / sqrt(30);
ci95_auc    = stdr_auc * 1.96;

mean_conv   = mean(e_rate_auc, 3);
stdv_conv   = std(e_rate_auc,[], 3);
stdr_conv   = stdv_conv / sqrt(30);
ci95_conv   = stdr_conv * 1.96;

subplot(1,3,1)
h =  barwitherr(ci95_auc, mean_auc);
set(gca,'Box', 'off');
set(gca, 'FontSize', 14)

for c1 = 1:length(h)
    set(h(c1), 'FaceColor', dark_2(c1,:))
    set(h(c1), 'EdgeColor', dark_2(c1,:))
    set(h(c1), 'FaceAlpha', 0.4)
    set(h(c1), 'LineWidth', 2)
end


hold on
hh = plot([0 7], [.5 .5], 'k--', 'LineWidth',3)

legend([h hh],{'Random', 'Grid + Model', 'Random + Model', 'EHVI+Hyperprior', '50% Chance'}, 'box', 'off')

xticklabels({'EP002','EP005','EP007','EP009','EP010','EP012'})


ylim([0 1])
xlim([.5 6.5])
ylabel('AUC Final')
view(90,90)

subplot(1,3,2)
h =  barwitherr(ci95_conv, mean_conv);
set(gca,'Box', 'off');

for c1 = 1:length(h)
    set(h(c1), 'FaceColor', dark_2(c1,:))
    set(h(c1), 'EdgeColor', dark_2(c1,:))
    set(h(c1), 'FaceAlpha', 0.4)
    set(h(c1), 'LineWidth', 2)
end

xlim([.5 6.5])


set(gca, 'FontSize', 14)
xticklabels({})
ylabel('Convergence Rate')
view(-90,-90)
%
subplot(1,3,3)
subject_id          = 1;
sample_number      	= 1:sample_compare;

for c1 = 1:size(auc_trajectory_all,2)
    hold on
    auc_trajectory_config   = squeeze(auc_trajectory_all(subject_id,c1,:,1:sample_compare));
    trajectory_mean         = mean(auc_trajectory_config);
    trajectory_stdv         = std(auc_trajectory_config);
    trajectory_serr         = trajectory_stdv / sqrt(30);
    trajectory_95ci         = trajectory_serr;
    
    tt = [sample_number flip(sample_number)];
    yy = [trajectory_mean + trajectory_95ci flip(trajectory_mean - trajectory_95ci)];
    patch(tt,yy, dark_2(c1,:), 'FaceAlpha', 0.4, 'EdgeColor', dark_2(c1,:))
    plot(trajectory_mean, 'color', dark_2(c1,:), 'LineWidth', 3)
    
    
end

plot([0 100], [.5 .5], 'k--', 'LineWidth',3)
xlim([1 sample_compare])
set(gca,'Box', 'off');
set(gca, 'FontSize', 14)
xlabel('Sample #')
ylabel('AUC')

%%
figure
mean_dist    = mean(dist_trajectory_all(:,:,:,sample_compare), 3);
stdv_dist    = std(dist_trajectory_all(:,:,:,sample_compare),[], 3);
stdr_dist    = stdv_auc / sqrt(30);
ci95_dist    = stdr_auc * 1.96;

mean_conv   = mean(e_rate_dist, 3);
stdv_conv   = std(e_rate_dist,[], 3);
stdr_conv   = stdv_conv / sqrt(30);
ci95_conv   = stdr_conv * 1.96;

subplot(2,2,[1])
h =  barwitherr(ci95_dist, mean_dist);
set(gca,'Box', 'off');
set(gca, 'FontSize', 14)

for c1 = 1:4
    set(h(c1), 'FaceColor', dark_2(c1,:))
    set(h(c1), 'EdgeColor', dark_2(c1,:))
    set(h(c1), 'FaceAlpha', 0.4)
    set(h(c1), 'LineWidth', 2)
end



legend({'Random', 'EHVI', 'Hyperprior', 'EHVI+Hyperprior'}, 'box', 'off')

hold on

% ylim([0 1])
xlim([.5 6.5])
ylabel('Distance Final')
xticklabels([])

subplot(2,2,[3])
h =  barwitherr(ci95_conv, -1*mean_conv);
set(gca,'Box', 'off');

for c1 = 1:4
    set(h(c1), 'FaceColor', dark_2(c1,:))
    set(h(c1), 'EdgeColor', dark_2(c1,:))
    set(h(c1), 'FaceAlpha', 0.4)
    set(h(c1), 'LineWidth', 2)
end

xlim([.5 6.5])


set(gca, 'FontSize', 14)
xticklabels({'EP002','EP005','EP007','EP009','EP010','EP012'})
ylabel('Convergence Rate')

%
subplot(2,2,[2 4])
subject_id          = 3;
sample_number      	= 1:sample_compare;

for c1 = 1:size(dist_trajectory_all,2)
    hold on
    dist_trajectory_config      = squeeze(dist_trajectory_all(subject_id,c1,:,1:50));
    trajectory_mean             = mean(dist_trajectory_config);
    trajectory_stdv             = std(dist_trajectory_config);
    trajectory_serr             = trajectory_stdv / sqrt(30);
    trajectory_95ci             = trajectory_serr;
    
    tt = [sample_number flip(sample_number)];
    yy = [trajectory_mean + trajectory_95ci flip(trajectory_mean - trajectory_95ci)];
    patch(tt,yy, dark_2(c1,:), 'FaceAlpha', 0.4, 'EdgeColor', dark_2(c1,:))
    plot(trajectory_mean, 'color', dark_2(c1,:), 'LineWidth', 3)
    
    
end

xlim([1 50])
set(gca,'Box', 'off');
set(gca, 'FontSize', 14)
xlabel('Sample #')
ylabel('Distance')