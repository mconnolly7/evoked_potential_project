function plot_dimension
close all; figure('Position',[-100 -100 1500 500])
results_2D                  = load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_2D_all.mat');
results_3D                  = load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_3D_all.mat');
results_4D                  = load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_3D_all.mat');

[auc_ci95_2D, auc_mean_2D, conv_ci95_2D, conv_mean_2D]  = dimension_summary_stats(results_2D.optimization_results);
[auc_ci95_3D, auc_mean_3D, conv_ci95_3D, conv_mean_3D]  = dimension_summary_stats(results_3D.optimization_results);
[auc_ci95_4D, auc_mean_4D, conv_ci95_4D, conv_mean_4D]  = dimension_summary_stats(results_4D.optimization_results);

dark_2 = [27,158,119
217,95,2
117,112,179
231,41,138]/255;

auc_mean                    = [auc_mean_2D; auc_mean_3D; auc_mean_4D];
auc_ci95                    = [auc_ci95_2D; auc_ci95_3D; auc_ci95_4D];

conv_mean                   = [conv_mean_2D; conv_mean_3D; conv_mean_4D];
conv_ci95                   = [conv_ci95_2D; conv_ci95_3D; conv_ci95_4D];

%%%%
% Plot AUC Final
%%%%
subplot(1,3,1)
h                           =  barwitherr(auc_ci95, auc_mean);

set(gca,'Box', 'off');
set(gca, 'FontSize', 14)

for c1 = 1:length(h)
    set(h(c1), 'FaceColor', dark_2(c1,:))
    set(h(c1), 'EdgeColor', dark_2(c1,:))
    set(h(c1), 'FaceAlpha', 0.4)
    set(h(c1), 'LineWidth', 2)
end


hold on
hh = plot([0 7], [.5 .5], 'k--', 'LineWidth',3);

legend([h hh],{'Random', 'Grid + Model', 'Random + Model', 'EHVI + Hyperprior', '50% Chance'}, 'box', 'off','Position', [.3 .8 .1 .1])

ylim([0 1])
xlim([.5 3.5])
ylabel('AUC Final')
xticklabels({'2 Dimensions', '3 Dimensions',sprintf('4 Dimensions')})
view(90,90)

%%%%
% Plot Convergence Rate
%%%%
subplot(1,3,2)

h                           =  barwitherr(conv_ci95, conv_mean);

set(gca,'Box', 'off');
set(gca, 'FontSize', 14)

for c1 = 1:length(h)
    set(h(c1), 'FaceColor', dark_2(c1,:))
    set(h(c1), 'EdgeColor', dark_2(c1,:))
    set(h(c1), 'FaceAlpha', 0.4)
    set(h(c1), 'LineWidth', 2)
end
view(-90,-90)
xticklabels({})
ylabel('Convergence Rate (a.u.)')

%%%%
% Plot Trajectory
%%%%
set_1 = [228,26,28
55,126,184
77,175,74]/255;

subplot(1,3,3)
hold on

t   = 1:99;
tt  = [t flip(t)];

auc_trajectory_2D   = results_2D.auc_trajectory_all;
auc_trajectory_3D   = results_3D.auc_trajectory_all;
auc_trajectory_4D   = results_4D.auc_trajectory_all;

[trajectory_mean_2D, trajectory_sterr_2D] = dimension_trajectory(auc_trajectory_2D,4);
[trajectory_mean_3D, trajectory_sterr_3D] = dimension_trajectory(auc_trajectory_3D,4);
[trajectory_mean_4D, trajectory_sterr_4D] = dimension_trajectory(auc_trajectory_4D,4);

h(1) = plot(trajectory_mean_2D, 'color', set_1(1,:), 'LineWidth', 3);
yy = [trajectory_mean_2D - trajectory_sterr_2D flip(trajectory_mean_2D + trajectory_sterr_2D)];
h(2) = patch(tt, yy, set_1(1,:), 'FaceAlpha', .4, 'EdgeColor', 'none');

h(3) = plot(trajectory_mean_3D, 'color', set_1(2,:), 'LineWidth', 3);
yy = [trajectory_mean_3D - trajectory_sterr_3D flip(trajectory_mean_3D + trajectory_sterr_3D)];
h(4) = patch(tt, yy, set_1(2,:), 'FaceAlpha', .4, 'EdgeColor', 'none');

h(5) = plot(trajectory_mean_4D, 'color', set_1(3,:), 'LineWidth', 3);
yy = [trajectory_mean_4D - trajectory_sterr_4D flip(trajectory_mean_4D + trajectory_sterr_4D)];
h(6) = patch(tt, yy, set_1(3,:), 'FaceAlpha', .4, 'EdgeColor', 'none');

hh = plot([0 99], [.5 .5], 'k--', 'LineWidth',3);
xlim([1 99])
ylim([0.4 .85])
box off
% legend([h hh], {'2 Dimension','','3 Dimension','','4 Dimension','','50% Chance'}, 'box' ,'off')
set(gca, 'FontSize', 14)
xlabel('Sample #')
ylabel('AUC')

x_bar       = [8 16];
x_patch     = [x_bar flip(x_bar)];
x_text      = 17;
plot(x_bar, [0.82 0.82], 'LineWidth', 3, 'color', set_1(1,:))
patch(x_patch, [0.815 0.815 .825 .825], set_1(1,:), 'FaceAlpha', 0.4, 'EdgeColor', 'none')
text(x_text, .82, '1 Dimension', 'FontSize', 14)

plot(x_bar, [0.8 0.8], 'LineWidth', 3, 'color', set_1(2,:))
patch(x_patch, [0.795 0.795 .805 .805], set_1(2,:), 'FaceAlpha', 0.4, 'EdgeColor', 'none')
text(x_text, .80, '2 Dimension', 'FontSize', 14)

plot(x_bar, [0.78 0.78], 'LineWidth', 3, 'color', set_1(3,:))
patch(x_patch, [0.775 0.775 .785 .785], set_1(3,:), 'FaceAlpha', 0.4, 'EdgeColor', 'none')
text(x_text, .78, '3 Dimension', 'FontSize', 14)
end



function [auc_ci95, auc_mean, conv_ci95, conv_mean] = dimension_summary_stats(results_)

sample_compare         	= 99;

% AUC
auc_results_compare 	= results_.auc_trajectory(:,:,:,sample_compare);

for c1 = 1:size(auc_results_compare,2)
    auc_component_vector(:,c1)  = reshape(auc_results_compare(:,c1,:), [], 1);
end

auc_mean   	= mean(auc_component_vector, 1);
auc_stdv    = std(auc_component_vector,[], 1);
sterr_auc   = auc_stdv / sqrt(size(auc_component_vector,1));
auc_ci95    = sterr_auc * 1.96;

% Convergence Rate
e_rate_auc              = results_.e_rate_auc;

for c1 = 1:size(auc_results_compare,2)
    conv_component_vector(:,c1)  = reshape(e_rate_auc(:,c1,:), [], 1);
end

conv_mean               = mean(conv_component_vector, 1);
conv_stdv               = std(conv_component_vector,[], 1);
conv_sterr              = conv_stdv / sqrt(30);
conv_ci95               = conv_sterr * 1.96;
end
