clear; clc; 
close all;

f               = figure( 'Position',  [100, 100, 1500, 500]);
x_grid          = [0.125    0.385    0.685];
y_grid_1        = .1;

width           = .25;
height          = .8;

ax(1)        	= axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid(2)  y_grid_1(1) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid(3)  y_grid_1(1) width(1) height(1)]);

set(f, 'currentaxes', ax(1));

results_3D                  = load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_2D_all.mat');
optimization_results        = results_3D.optimization_results;

n_trials                    = optimization_results(1).optimization_config.n_trials;
n_samples                   = optimization_results(1).optimization_config.n_samples;

for c1 = 1:size(optimization_results,2)
    auc_trajectory_all{c1} = [];
end

for c1 = 1:size(optimization_results,2)
    algorithm_name  = optimization_results(c1).algorithm_config.name;
    auc_trajectory  = optimization_results(c1).auc_trajectory;
    
    if isempty(algorithm_name)
        algorithm_name = 'evolution_model_hyperprior';
    end
    switch algorithm_name
        case 'grid'
            algorithm_code(c1) = 10;
        case 'grid_model'
         	algorithm_code(c1) = 9;
        case 'grid_model_hyperprior'
            algorithm_code(c1) = 8;
        
        case 'evolution_model'
            algorithm_code(c1) = 7;
        case 'evolution_model_hyperprior'
            algorithm_code(c1) = 6;
        
        case 'random'
            algorithm_code(c1) = 5;
        case 'random_model'
            algorithm_code(c1) = 4;
        case 'random_model_hyperprior'
            algorithm_code(c1) = 3;      
        
        case 'ehvi_model'
            algorithm_code(c1) = 2;
        case 'ehvi_model_hyperprior'
            algorithm_code(c1) = 1;
    end
    
    auc_trajectory_all{algorithm_code(c1)}  = [auc_trajectory_all{algorithm_code(c1)} auc_trajectory];
end

sample_number          	= (1:n_samples-1)';
auc_final               = nan(10,120);

for c1 = 1:10;size(auc_trajectory_all,2)
    [e_mean(c1), e_ci(:,c1)]   	= error_convergence_rate_2(sample_number, 1-auc_trajectory_all{c1});
    auc_final(c1,1:size(auc_trajectory_all{c1}(end,:),2))          	= auc_trajectory_all{c1}(end,:);
end

auc_mean        = nanmean(auc_final,2);
auc_stdv        = std(auc_final,[],2);
auc_ster        = auc_stdv / sqrt(size(auc_final,2));
auc_ci95        = auc_ster * 1.96;

x               = [.5 1,   2 2.5 3,  4 4.5,   5.5 6 6.5];
color_idx       = [1  1    2 2 2     3 3      4 4 4];
face_alpha      = [1 .5    1 .5 .3   1 .5     1 .5 .3];
y               = auc_mean;


hold on 
for c1 = 1:length(x)
    bar(x(c1),y(c1), 'BarWidth', .4, 'FaceColor', dark2(color_idx(c1)), 'FaceAlpha', face_alpha(c1), 'linewidth', 1)
    plot([x(c1) x(c1)], auc_mean(c1) + auc_ci95(c1)*[1 -1], 'color', 'k', 'lineWidth', 2);
end

plot([0 100], [.5 .5], 'k--', 'LineWidth',3)
xlim([0 7]);
ylim([0.45 0.95])
view(90, 90)
ylabel('Final AUC')
xticks(x)
xticklabels({'EHVI+Hyperprior','EHVI+Model',...
     'Random+Hyperprior', 'Random+Model','Random',  ...
    'Evolution+Hyperprior', 'Evolution+Model', ...
    'Grid+Hyperprior','Grid+Model','Grid',...
    });
set(gca, 'FontSize', 16)


set(f, 'currentaxes', ax(2));
hold on 
for c1 = 1:length(x)
    bar(x(c1),e_mean(c1), 'BarWidth', .4, 'FaceColor', dark2(color_idx(c1)), 'FaceAlpha', face_alpha(c1), 'linewidth', 1)
    plot([x(c1) x(c1)], e_ci(:,c1), 'color', 'k', 'lineWidth', 2);
end

view(-90, -90)
xticks(x)
xticklabels([])
ylabel('Convergence')
set(gca, 'FontSize', 16)
ylim([-0.025 0])
xlim([0 7]);

%
%
%
set(f, 'currentaxes', ax(3));

algorithm_plot = [1 3 6 8];

hold on

t       = 1:99;
tt      = [t flip(t)];

line_style = {'-', ':', '-.', '--', '--'};
for c1 = 1:size(algorithm_plot,2)
    algorithm_trajectory    = auc_trajectory_all{algorithm_plot(c1)};
    trajectory_mean         = mean(algorithm_trajectory,2);
    trajectory_stdv         = std(algorithm_trajectory,[],2);
    trajectory_sterr        = trajectory_stdv / sqrt(size(algorithm_trajectory,2));
    trajectory_ci95         = trajectory_sterr *1.96;
    
    yy = [trajectory_mean + trajectory_ci95; flip(trajectory_mean - trajectory_ci95)];
    patch(tt,yy, dark2(c1), 'FaceAlpha', 0.2, 'EdgeColor', 'none')
    plot_idx(c1) = plot(trajectory_mean, 'Color', dark2(c1), 'linewidth', 3, 'LineStyle', line_style{c1});
    
end
plot_idx(c1+1) = plot([0 99], [.5 .5], 'k--', 'LineWidth',3);
xlim([1 99])
ylim([.4 .95])
set(gca, 'FontSize', 16)
xlabel('Samples Collected')
ylabel('AUC')
y_coords        = [.86];
x_coords        = [.1 .64];
sublabel_size   = 28;

%
%
%
legend_colors = [dark2(1); dark2(2); dark2(3); dark2(4); 1 1 1];

x_legend    = [45 55];
y_legend    = [.65 .62 .59 .56 .53] -.015;
height      = 0.01;
x_text      = 56;
xx          = [x_legend flip(x_legend)];
legend_labels = {'EHVI+Hyperprior','Random+Hyperprior','Evolution+Hyperprior','Grid+Hyperprior','0.50 AUC'};

for c1 = 1:4
    yy = [y_legend(c1)-height y_legend(c1)-height y_legend(c1)+height y_legend(c1)+height];
    patch(xx,yy,legend_colors(c1,:), 'FaceAlpha', 0.5, 'EdgeColor', 'none')
    plot(x_legend, [y_legend(c1) y_legend(c1)], 'Color', legend_colors(c1,:), 'linewidth', 3, 'LineStyle', line_style{c1})
    text(x_text, y_legend(c1), legend_labels{c1}, 'FontSize', 14)

end
plot(x_legend, [y_legend(5) y_legend(5)], 'Color', 'k', 'linewidth', 3, 'LineStyle', line_style{5})
text(x_text, y_legend(5), legend_labels{5}, 'FontSize', 14)


l = legend(plot_idx, {'EHVI+Hyperprior','Random+Hyperprior','Evolution+Hyperprior','Grid+Hyperprior','0.50 AUC'},...
    'box', 'off', 'Position',[0.8487 0.1200 0.0787 0.2240]);

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','a','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','b','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);


