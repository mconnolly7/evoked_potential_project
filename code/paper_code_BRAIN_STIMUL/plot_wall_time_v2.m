clear; clc; 
% close all;

f               = figure( 'Position',  [100, 100, 1500, 500]);
x_grid          = [0.125    0.425   0.725];
y_grid_1        = .1;

width           = .25;
height          = .8;

ax(1)        	= axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid(2)  y_grid_1(1) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid(3)  y_grid_1(1) width(1) height(1)]);

% compute_time    = [.005 .5 7.05,    .006 0.98 9.7,   0 6.7 13.6];
compute_time    = [7.05 .5 .005,     9.7 0.98 .006,  13.6  6.7 0 ];
auc             = [0.87 0.86 0.88,  0.84 0.87 0.88,  0.72 0.75 0.78];

for c1 = 1:size(compute_time,2)
    overflow_time(c1)           = max(compute_time(c1)-3, 0);
    sample_time(c1)             = 15+overflow_time(c1);
    n_search(c1)             	= floor(1350/sample_time(c1)); 
    n_samples(c1)               = n_search(c1)+9;
end



results_2D                	= load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_2D_all.mat');
results_3D                 	= load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_3D_all.mat');
results_4D                 	= load('evoked_potential_project/results/jne_simulation_results_2020_06_29/AUC_trajectory_results_4D_all.mat');

optimization_results_2D    	= results_2D.optimization_results;
optimization_results_3D   	= results_3D.optimization_results;
optimization_results_4D  	= results_4D.optimization_results;

n_trials                  	= optimization_results_2D(1).optimization_config.n_trials;
n_overflow                  = optimization_results_2D(1).optimization_config.n_samples;

for c1 = 1:10;size(optimization_results_2D,2);
    auc_trajectory_all_2D{c1} = [];
    auc_trajectory_all_3D{c1} = [];
    auc_trajectory_all_4D{c1} = [];
end

for c1 = 1:size(optimization_results_2D,2)
    algorithm_name      = optimization_results_2D(c1).algorithm_config.name;
    auc_trajectory_2D   = optimization_results_2D(c1).auc_trajectory;
    auc_trajectory_3D   = optimization_results_3D(c1).auc_trajectory;
    auc_trajectory_4D   = optimization_results_4D(c1).auc_trajectory;
    
    if isempty(algorithm_name)
        algorithm_name = 'evolution_model_hyperprior';
    end
    switch algorithm_name
        case 'grid'
            algorithm_code(c1) = 10;
        case 'grid_model'
         	algorithm_code(c1) = 9;
        case 'grid_model_hyperprior'
            algorithm_code(c1) = 8;
        
        case 'evolution_model'
            algorithm_code(c1) = 7;
        case 'evolution_model_hyperprior'
            algorithm_code(c1) = 6;
        
        case 'random'
            algorithm_code(c1) = 5;
        case 'random_model'
            algorithm_code(c1) = 4;
        case 'random_model_hyperprior'
            algorithm_code(c1) = 3;      
        
        case 'ehvi_model'
            algorithm_code(c1) = 2;
        case 'ehvi_model_hyperprior'
            algorithm_code(c1) = 1;
    end
    
    auc_trajectory_all_2D{algorithm_code(c1)}  = [auc_trajectory_all_2D{algorithm_code(c1)} auc_trajectory_2D];
    auc_trajectory_all_3D{algorithm_code(c1)}  = [auc_trajectory_all_3D{algorithm_code(c1)} auc_trajectory_3D];
    auc_trajectory_all_4D{algorithm_code(c1)}  = [auc_trajectory_all_4D{algorithm_code(c1)} auc_trajectory_4D];
end

algorithm_plot = [1 2 3];

auc_trajectory_all(1:3)     = auc_trajectory_all_2D(algorithm_plot);
auc_trajectory_all(4:6)     = auc_trajectory_all_3D(algorithm_plot);
auc_trajectory_all(7:9)     = auc_trajectory_all_4D(algorithm_plot);

sample_number               = (1:n_overflow-1)';
% auc_final                   = nan(10,120);

for c1 = 1:size(auc_trajectory_all,2)
    [e_mean(c1), e_ci(:,c1)]   	= error_convergence_rate_2(sample_number, 1-auc_trajectory_all{c1});
    auc_final_timed(c1,1:size(auc_trajectory_all{c1}(end,:),2))          	= auc_trajectory_all{c1}(n_samples(c1),:);
    auc_final_sample(c1,1:size(auc_trajectory_all{c1}(end,:),2))          	= auc_trajectory_all{c1}(end,:);
end

auc_mean_timed  = nanmean(auc_final_timed,2);
auc_mean_sample  = nanmean(auc_final_sample,2);
auc_stdv        = std(auc_final_timed,[],2);
auc_ster        = auc_stdv / sqrt(size(auc_final_timed,2));
auc_ci95        = auc_ster * 1.96;

x               = [0 .5 1,   2 2.5 3,  4 4.5 5];
color_idx       = [1 1 2,    1 1 2,    1 1 2];
grey_idx        = [1 1 1,    1 1 1,    .3 .5 .7];
face_alpha      = [1 .5 1,   1 .5 1,   1 1 1];
y               = auc_mean_timed;

algorithm_plot  = [7 8 9];

%
%
%

set(f, 'currentaxes', ax(1));

hold on 
for c1 = 1:length(x)
    
    bar(x(c1), auc_mean_sample(c1), 'BarWidth', .4, 'LineWidth', 1, 'EdgeColor', 'k', 'FaceColor', 'none');
    if ~any(c1 == algorithm_plot)
        plot_handle(c1) = bar(x(c1),y(c1), 'BarWidth', .4, 'FaceColor', dark2(color_idx(c1)), 'FaceAlpha', face_alpha(c1), 'linewidth', 1);
        plot([x(c1) x(c1)], auc_mean_timed(c1) + auc_ci95(c1)*[1 -1], 'color', 'k', 'lineWidth', 2);
    else
        plot_handle(c1) = bar(x(c1),y(c1), 'BarWidth', .4, 'FaceColor', grey_idx(c1)*ones(1,3), 'FaceAlpha', face_alpha(c1), 'linewidth', 1);
        plot([x(c1) x(c1)], auc_mean_timed(c1) + auc_ci95(c1)*[1 -1], 'color', 'k', 'lineWidth', 2);
    end
    
end

plot_handle(4) = plot([-.5 100], [.5 .5], 'k--', 'LineWidth',3);
xlim([-.5 5.5]);
ylim([0.45 .95])
view(90, 90)
ylabel('Time-Limited AUC')
xticks([.5 2.5 4.5])
xticklabels({'Monopolar (2D)','Bipolar (3D)','Bipolar/Pulse Width (4D)'});
set(gca, 'FontSize', 14)

l = legend(plot_handle(1:4), {'EHVI+Hyperprior', 'EHVI', 'Random+Hyperprior','0.50 AUC'}, ...
    'box', 'off','Position', [0.2763 0.1170 0.1327 0.1310]);

%
%
%

% set(f, 'currentaxes', ax(2));
% hold on 
% for c1 = 1:length(x)
%     bar(x(c1),e_mean(c1), 'BarWidth', .4, 'FaceColor', dark2(color_idx(c1)), 'FaceAlpha', face_alpha(c1), 'linewidth', 1)
%     plot([x(c1) x(c1)], e_ci(:,c1), 'color', 'k', 'lineWidth', 2);
% end
% 
% view(-90, -90)
% xticks(x)
% xticklabels([])
% ylabel('Convergence')
% set(gca, 'FontSize', 14)
% ylim([-0.018 0])
% xlim([-.5 7]);

%
%
%
set(f, 'currentaxes', ax(2));


hold on


line_style = {'-', ':', '-.', '--'};
color_idx  = [1 1 2];
alpha_idx   = [.7 .3 .7];
grey_idx = [.3 .5 .7];
color_bupu = [140,150,198
            136,86,167
            129,15,124]/255;
        
for c1 = 1:size(algorithm_plot,2)
    
    plot_idx                = algorithm_plot(c1);
    t                       = [(2:10)*15 150+(1:n_search(plot_idx))*sample_time(plot_idx)];
    tt                      = [t  flip(t)];
    
    n_samples               = size(t,2);
    algorithm_trajectory    = auc_trajectory_all{algorithm_plot(c1)}(1:n_samples,:);
    
    trajectory_mean         = mean(algorithm_trajectory,2);
    trajectory_stdv         = std(algorithm_trajectory,[],2);
    trajectory_sterr        = trajectory_stdv / sqrt(size(algorithm_trajectory,2));
    trajectory_ci95         = trajectory_sterr *1.96;
    
    yy = [trajectory_mean + trajectory_ci95; flip(trajectory_mean - trajectory_ci95)];
%     patch(tt, yy, set1(color_idx(c1)), 'FaceAlpha', alpha_idx(c1), 'EdgeColor', 'none')
%     plot(t, trajectory_mean, 'Color', set1(color_idx(c1)), 'linewidth', 3, 'LineStyle', line_style{c1})
    patch(tt,yy, grey_idx(c1)*ones(1,3), 'FaceAlpha', 0.5, 'EdgeColor', 'none')
    plot(t, trajectory_mean, 'Color', grey_idx(c1)*ones(1,3), 'linewidth', 3, 'LineStyle', line_style{c1})
    
end
plot([0 1500], [.5 .5], 'k--', 'LineWidth',3)
xlim([30 1500])
ylim([.15 .8])
set(gca, 'FontSize', 14)
xlabel('Time Elapsed (seconds)')
ylabel('AUC')
% l=legend({'EHVI+Hyperprior', '', 'EHVI', '', 'Random+Hyperprior', '', '0.50 AUC'}, 'box', 'off', 'Position', [0.4927 0.1100 0.1327 0.2240]);

x_legend    = [800 900];
y_legend    = [.40 .36 .32 .28];
height      = 0.015;
x_text      = 910;
xx          = [x_legend flip(x_legend)];
legend_labels = {'EHVI+Hyperprior', 'EHVI', 'Random+Hyperprior','0.50 AUC'};

for c1 = 1:3
    yy = [y_legend(c1)-height y_legend(c1)-height y_legend(c1)+height y_legend(c1)+height];
    patch(xx,yy,grey_idx(c1)*ones(1,3), 'FaceAlpha', 0.5, 'EdgeColor', 'none')
    plot(x_legend, [y_legend(c1) y_legend(c1)], 'Color', grey_idx(c1)*ones(1,3), 'linewidth', 3, 'LineStyle', line_style{c1})
    text(x_text, y_legend(c1), legend_labels{c1}, 'FontSize', 14)

end
plot(x_legend, [y_legend(4) y_legend(4)], 'Color', 'k', 'linewidth', 3, 'LineStyle', line_style{4})
text(x_text, y_legend(4), legend_labels{4}, 'FontSize', 14)


%
%
%
set(f, 'currentaxes', ax(3));

hold on
scatter(compute_time(1), auc(1), 200, dark2(2), 'filled', 'Marker', 'o')
scatter(compute_time(2), auc(2), 200, dark2(1), 'filled', 'Marker', 'o', 'MarkerFaceAlpha', .7, 'MarkerEdgeColor', 'k')
scatter(compute_time(3), auc(3), 200, dark2(1), 'filled', 'Marker', 'o')
scatter(compute_time(4), auc(4), 200, dark2(2), 'filled', 'Marker', '^')
scatter(compute_time(5), auc(5), 200, dark2(1), 'filled', 'Marker', '^', 'MarkerFaceAlpha', .7)
scatter(compute_time(6), auc(6), 200, dark2(1), 'filled', 'Marker', '^')
scatter(compute_time(7), auc(7), 200, dark2(2), 'filled', 'Marker', 's')
scatter(compute_time(8), auc(8), 200, dark2(1), 'filled', 'Marker', 's', 'MarkerFaceAlpha', .7)
scatter(compute_time(9), auc(9), 200, dark2(1), 'filled', 'Marker', 's')

xlabel('Calculation Time per Sample')
ylabel('AUC')
set(gca, 'FontSize', 16)


y_coords        = [.86];
x_coords        = [.09 0.39 .69];
sublabel_size   = 28;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','a','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','b','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(3) y_coords(1) .1 .1],'String','c','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);

print('/Users/mconn24/Box Sync/papers/2019_07_01_paper_evoked_potential_optimization/figures/plot_wall_time/plot_wall_time', '-dpng', '-r500')
