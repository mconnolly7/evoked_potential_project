% function plot_models
close all
monopolar_model      = load('evoked_potential_project/data/subject_models/2D/EP002_model.mat');
bipolar_model       = load('evoked_potential_project/data/subject_models/3D/EP002_model.mat');
pulse_width_model   = load('evoked_potential_project/data/subject_models/4D/EP002_model.mat');


%%

ppt_colors = [197 90, 17;
    84, 130, 54;
    46, 117, 182;
    191, 144, 0]/255;

input_cathode       = [0 1 2 3];
input_amplitude     = 0.1:.1:5;
input_space         = combvec(input_cathode, input_amplitude)';


close all
f               = figure( 'Position',  [100, 100, 800, 1000]);
x_grid_1        = [0.075  0.575];
x_grid_2        = [0.34 .56  0.775];
y_grid_1        = [0.0500    0.2750    0.52500    0.775];
y_grid_2        = [0.0500    0.2750    0.48000    0.775];

width           = [.35 .15];
height          = [.2 .15];

ax(1)        	= axes('Position', [x_grid_1(1)  y_grid_1(4) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid_1(2)  y_grid_1(4) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid_1(1)  y_grid_2(3) width(1) height(1)]);
ax(4)        	= axes('Position', [x_grid_1(2)  y_grid_1(3) width(1) height(1)]);
% ax(5)        	= axes('Position', [x_grid_1(1)  y_grid_1(2) width(1) height(1)]);
ax(6)        	= axes('Position', [x_grid_1(2)  y_grid_1(2) width(1) height(1)]);
ax(7)        	= axes('Position', [x_grid_2(1)  y_grid_1(1) width(2) height(2)]);
ax(8)        	= axes('Position', [x_grid_2(2)  y_grid_1(1) width(2) height(2)]);
ax(9)        	= axes('Position', [x_grid_2(3)  y_grid_1(1) width(2) height(2)]);

set(f, 'currentaxes', ax(3));
% monopolar_model.emg_model.minimize(5)
% monopolar_model.emg_model.hyperparameters.mean = 0;
plot_monopolar(monopolar_model.emg_model)
colormap(inferno)
% monopolar_model.emg_model.plot_data
zlabel('mEP (µV)')
set(gca,'FontSize', 16);
ylabel('Amp. (mA)')

set(f, 'currentaxes', ax(1));
for c3 = 1:200
        input_d = input_space(c3,:);
        xx = [input_d(1) - 0.4,input_d(1) + 0.4,input_d(1) + 0.4,input_d(1) - 0.4];
        yy = [input_d(2) - .04, input_d(2) - .04, input_d(2) + .04, input_d(2) + .04];

%         patch(xx,yy,[1 1 1], 'EdgeColor', [.5 .5 .5])     
        patch(xx,yy,ppt_colors(input_d(1)+1, :), 'EdgeColor', 0*[.5 .5 .5], 'facealpha', input_d(2)/5)     
        hold on

end
ylim([0 5.05])
xticks([0 1 2 3])
xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})
    
set(gca, 'FontSize', 15)
ylabel('Amplitude (mA)')


set(f, 'currentaxes', ax(2));
monopolar_model.ep1_model.minimize(1)
plot_monopolar(monopolar_model.ep1_model)
colormap(inferno)
% monopolar_model.ep1_model.plot_data
zlabel('cEP (µV)')
set(gca,'FontSize', 15);
ylabel('Amp. (mA)')


set(f, 'currentaxes', ax(6));
plot_bipolar(bipolar_model.ep1_model)
colormap(inferno)
set(gca,'FontSize', 15);

plot_4D_model(pulse_width_model.ep1_model, f, ax(7:9))
set(f, 'currentaxes', ax(8));
xlabel('');
ylabel('');
set(f, 'currentaxes', ax(9));
xlabel('');
ylabel('');
% set(gca,'FontSize', 14);
colormap(inferno)

%%%%%%
set(f, 'currentaxes', ax(4));

cep_gt              = monopolar_model.ep1_model.predict(input_space);
mep_gt              = monopolar_model.emg_model.predict(input_space);

[pareto_idx, dom_order] = get_pareto_2(1, -1, [cep_gt mep_gt]);

max_dom_order   = 2;
    
dom_color_map   = [255,255,178;
                254,204,92;
                253,141,60;
                240,59,32;
                189,0,38]/255;

dom_color_map = flip(dom_color_map);

hold on
% for c2 = 0:max_dom_order
%     plot_idx = dom_order == c2;
%     scatter(cep_gt(plot_idx,1),mep_gt(plot_idx,1),100,dom_color_map(c2+1,:),'filled', 'MarkerFaceAlpha',.7)
% end
% 
% plot_idx = dom_order > c2;
% scatter(cep_gt(plot_idx,1),mep_gt(plot_idx,1),100,[.5 .5 .5])

for c1 = 1:size(input_space,1)
    
    scatter(cep_gt(c1,1),mep_gt(c1,1),100, ppt_colors(input_space(c1,1)+1,:), ...
        'filled', 'MarkerFaceAlpha', input_space(c1,2)/5, 'MarkerEdgeColor', 0*[.5 .5 .5])
    
end
set(gca,'FontSize', 15);
xlabel('cEP (µV)')
ylabel('mEP (µV)')

%
y_coords        = [.9 .65 .4 .15];
x_coords        = [.01 0.5];
sublabel_size   = 28;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','a','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','b','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(2)-.045 .1 .1],'String','c','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(2) .1 .1],'String','d','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(3)-.05 .1 .1],'String','e','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(3) .1 .1],'String','f','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(4) .1 .1],'String','g','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2)-.2 y_coords(4) .1 .1],'String','h','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);




