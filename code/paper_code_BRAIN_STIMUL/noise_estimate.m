load('/Users/mconn24/Box Sync/EP_data_for_Mark/EPdata_4-20-2020.mat');

A = [7, 8, 12, 13, 17, 18, 22, 23, 32, 33, 37, 38, 42, 43, 53, 54];

EP_data = EP(25);

y = [EP(25).ephysEP1_B(A(1:2:end)) EP(25).ephysEP1_B(A(2:2:end))]';

x = [0, 1; 1 0; 1 1; 1 2; 2 0; 2 1; 2 2; 3 1];
x = [x;x];
close all

subplot(2,2,1)
gp_model = gp_object();
gp_model.initialize_data(x,y);
gp_model.minimize(1)
gp_model.hyperparameters.lik = 1;
gp_model.plot_mean

for c1 = 1:8
    scatter3(x(c1,1), x(c1,2), y(c1), 50, set1(c1), 'filled')
    a= scatter3(x(c1+8,1), x(c1+8,2), y(c1+8), 50, set1(c1), 'filled')
end
    
[y_mean, y_fs] = gp_model.predict(x);
cnr_1 = (max(y_mean) - min(y_mean))/mean(y_fs);
colormap(viridis)

xlabel('Y Coordinate')
ylabel('X Coordinate')
zlabel('cEP (µV)')

view([30,25])
font_size(20)

subplot(2,2,[2 4])
hold on
for c1 = 1:8
    scatter(y(c1), y(c1+8), 100, set1(c1), 'filled')
%     scatter3(x(c1+8,1), x(c1+8,2), y(c1+8), 50, set1(c1), 'filled')
end

plot([min(y) max(y)], [min(y), max(y)], 'r--', 'LineWidth', 2)

xlabel('cEP_1 (µV)')
ylabel('cEP_2 (µV)')
font_size(20)

subplot(2,2,3)
gp_model.plot_mean

for c1 = 1:8
    scatter3(x(c1,1), x(c1,2), y(c1), 50, set1(c1), 'filled')
    scatter3(x(c1+8,1), x(c1+8,2), y(c1+8), 50, set1(c1), 'filled')
end

xlabel('Y Coordinate')
ylabel('X Coordinate')
zlabel('ephysEP1_B')
view([90,-90])
font_size(20)

%%
subplot(1,2,2)


load('/Users/mconn24/repositories/evoked_potential_project/data/subject_models/2D/EP002_model.mat')

[y_mean, y_fs] = ep1_model.predict(x);
cnr_2 = (max(y_mean) - min(y_mean))/mean(y_fs)
ep1_model.noise_sigma = 1/8;
[y_mean] = ep1_model.sample(x);
% cnr_3 = (max(y_mean) - min(y_mean))/mean(y_fs)


%%
% ep1_model.hyperparameters.lik = gp_model.hyperparameters.lik;
ep1_model.hyperparameters.cov(1) = gp_model.hyperparameters.cov(1);

ep1_model.plot_mean
ep1_model.plot_data
ep1_model.plot_standard_deviation

%%
clc
load('/Users/mconn24/Box Sync/EP_data_for_Mark/EPdata_forMarkJan2020.mat')
% squeeze(EP(2).amplitudes(3:4,1,:))
% squeeze(EP(2).relativeAmplitudes(1,1,:))'

sum(EP(2).EP1ramp(1,:))
% EP(2);
% EP(2).EP1rlat(1,:)
% squeeze(EP(2).latencies(3,1,:))'

%
load('/Users/mconn24/Box Sync/EP_data_for_Mark/EPdata_4-20-2020.mat')
EP(2).ephysEP1_B()







