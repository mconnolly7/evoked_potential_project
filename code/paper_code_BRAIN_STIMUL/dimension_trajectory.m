function [trajectory_mean, trajectory_sterr] = dimension_trajectory(auc_trajectory,row)

ehvi_2D             = squeeze(auc_trajectory(:,row,:,:));
ehvi_vec            = [];

for c1 = 1:6
    ehvi_vec = [ehvi_vec; squeeze(ehvi_2D(c1,:,:))];
end

trajectory_mean     = mean(ehvi_vec,1);
trajectory_stdv     = std(ehvi_vec,[],1);
trajectory_sterr    = trajectory_stdv / sqrt(size(ehvi_vec,1));

end


