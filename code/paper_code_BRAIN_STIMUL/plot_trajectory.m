% load('/Users/mconn24/repositories/ehvi_model_2D_EP002.mat')
load('ehvi_model_2D_EP002.mat')
%%

close all
% trials = 1:30;
% trials = [6 13 19 24 25 29];
% trials = [13 25 29];
trials = 13;
for trial_idx = 1:size(trials,2)
    % trial_idx = trials(trial_idx);
    
    cep_model       = optimization_config.objective_1;
    mep_model       = optimization_config.objective_2;
    input_space     = optimization_config.input_space;
    
    costs_est(:,1)  = cep_model.predict(input_space);
    costs_est(:,2)  = mep_model.predict(input_space);
    
    [pareto_idx, dom_order_gt] = get_pareto_2(1, -1, costs_est);

    trial_results   = optimization_results(trials(trial_idx));
    X_sample        = trial_results.X_sample;
    objective_est   = trial_results.objective_est;
    dom_order       = trial_results.dom_order_est;
    Y_est           = trial_results.Y_est;
            
    figure('Position', [721 1 1100 1100])
    t_points        = [5 15 99];
    
    for c1 = 1:size(t_points,2)
        
        t               = t_points(c1);
        
        auc = pareto_roc_2(dom_order(:,t),dom_order_gt);

        subplot(4,3,c1)
        objective_est(t,1).lower_bound = [0 0];
        objective_est(t,1).upper_bound = [3 5];
        objective_est(t,1).update_plot_inputs
        objective_est(t,1).plot_mean
        objective_est(t,1).plot_data
        colormap(viridis)
        
        ylabel('Amplitude (mA)');
        zlabel('cEP (µV)')
        xlim([0 3])
        ylim([0 5])
        zlim([0 6.5])
        
        xticks([0 1 2 3])
        xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})
        title(sprintf('%d Samples; AUC=%.2f', t, auc))
        
        set(gca,'FontSize', 14)
        
        
        subplot(4,3,c1+3)
              
        objective_est(t,2).lower_bound = [0 0];
        objective_est(t,2).upper_bound = [3 5];
        objective_est(t,2).update_plot_inputs
        objective_est(t,2).plot_mean
        objective_est(t,2).plot_data
        colormap(inferno)
        ylabel('Amplitude (mA)');
        zlabel('mEP (µV)')
        xlim([0 3])
        ylim([0 5])
        zlim([-2 14])
        xticks([0 1 2 3])
        xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})
        set(gca,'FontSize', 14)
        
        
        subplot(4,3,c1+6)
        Y_est_t         = Y_est(:,:,t);
        dom_order_t     = dom_order(:,t);
        max_dom_order   = 2;
        
        dom_color_map   = [255,255,178
            254,204,92
            253,141,60
            240,59,32
            189,0,38]/255;
        
        dom_color_map   = [
         0   104    55
       120   198   121
       217   240   163  
                    ]/255;
%         dom_color_map = flip(dom_color_map);
        
        hold on
        for c2 = 0:max_dom_order
            plot_idx = dom_order_t == c2;
            scatter(Y_est_t(plot_idx,1),Y_est_t(plot_idx,2),100,dom_color_map(c2+1,:),'filled', 'MarkerFaceAlpha',.7, 'MarkerEdgeColor', 'k')
        end
        
        plot_idx = dom_order_t > c2;
        scatter(Y_est_t(plot_idx,1),Y_est_t(plot_idx,2),100,[.5 .5 .5])
        
        scatter(costs_est(dom_order_gt == 0,1), costs_est(dom_order_gt == 0,2), 30, 'k','filled')
        
        xlabel('cEP (µV)')
        ylabel('mEP (µV)')
        xlim([1 6])
        ylim([0 10])
        set(gca,'FontSize', 14)
        
        
        
        subplot(4,3,c1+9)
        hold on
        
        for c2 = 0:max(dom_order_t)
            dom_idx = find(dom_order_t == c2);
            for c3 = 1:size(dom_idx,1)
                input_d = input_space(dom_idx(c3),:);
                xx = [input_d(1) - 0.4,input_d(1) + 0.4,input_d(1) + 0.4,input_d(1) - 0.4];
                yy = [input_d(2) - .04, input_d(2) - .04, input_d(2) + .04, input_d(2) + .04];
                
                if c2 > max_dom_order
                    patch(xx,yy,[1 1 1], 'EdgeColor', [.5 .5 .5])
                else
%                     patch(xx,yy,dom_color_map(c2+1,:), 'FaceAlpha', 1, 'edgecolor', dom_color_map(c2+1,:))
                    patch(xx,yy,dom_color_map(c2+1,:), 'FaceAlpha', 1, 'edgecolor', 'k')
                end
            end
        end
        
        scatter(input_space(dom_order_gt == 0,1), input_space(dom_order_gt == 0,2), 20, [0 0 0],'filled');
        ylim([0 5])
        xlim([-.8 3.8])
        xticks([0 1 2 3])
        xticklabels({'C+/0-','C+/1-','C+/2-','C+/3-'})
        ylabel('Amplitude (mA)')
        set(gca,'FontSize',14)        
       
    end
    
end

print('/Users/mconn24/Box Sync/papers/2019_07_01_paper_evoked_potential_optimization/figures/06_trajectory/trajectory', '-dpng', '-r500')