data_dir = 'evoked_potential_project/data/simulation_data/jne_data_2020_06_29/4D/';


d = dir([data_dir '*.mat']);

for c1 = 1:size(d,1)
    
    fprintf('Results %d processed\n', c1);
    
    file_name                       = d(c1).name;
    file_path                       = [data_dir file_name];

    data_struct                     = load(file_path);
    [auc_trajectory, e_rate_auc]    = calculate_auc_trajectory(data_struct);
    
    optimization_config             = data_struct.optimization_config;
    algorithm_config                = data_struct.algorithm_config;
    
    optimization_results(c1).subject_id             = file_name(end-8:end-4);
    optimization_results(c1).optimization_config    = optimization_config;
    optimization_results(c1).algorithm_config       = algorithm_config;
    optimization_results(c1).auc_trajectory         = auc_trajectory;
    optimization_results(c1).e_rate_auc             = e_rate_auc;
end