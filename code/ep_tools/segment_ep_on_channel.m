function pulse_data = segment_ep_on_channel(ep_data, pulse_onset_times, sampling_rate, segment_length_time)
%
% pulse_data: <channel, pulse, time>
%

n_pulses                    = size(pulse_onset_times, 2);

ep_window_idx               = floor(segment_length_time * sampling_rate);

pulse_onset_idx             = floor(pulse_onset_times * sampling_rate);
pulse_offset_idx            = pulse_onset_idx + ep_window_idx;
    
for c2 = 1:n_pulses
    pulse_idx               = pulse_onset_idx(c2):pulse_offset_idx(c2);
    pulse_data(:,c2,:)      = ep_data(:,pulse_idx);
end

end
