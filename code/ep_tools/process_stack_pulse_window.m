function stacked_pulses = process_stack_pulse_window(data_neuro_sig, pulse_onset_idx, ...
        sampling_rate, pulse_window_duration)
%%%%%%%%%
%
% 9/27/22 Need to figure out/catch the extra pulse problem
%
%%%%%%%%%

pulse_window_samples = pulse_window_duration * sampling_rate;

for c1 = 1:size(pulse_onset_idx, 2)-1
    
    pulse_idx = pulse_onset_idx(c1):(pulse_onset_idx(c1) + pulse_window_samples - 1);
    stacked_pulses(c1,:) = data_neuro_sig(pulse_idx);

end

end