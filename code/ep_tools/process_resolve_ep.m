function [ep_mean, ep_std, ep_se, ep_ci] = process_resolve_ep(sync_data, ctx_data, pulse_window_duration, post_artifact_idx, sampling_rate)

[~, pulse_onset_idx] = process_detect_pulse_onset(sync_data, 0, 0, sampling_rate);

data_neuro_sig = ctx_data(2,:) - ctx_data(4,:);
stacked_pulses = process_stack_pulse_window(data_neuro_sig, pulse_onset_idx, ...
        sampling_rate, pulse_window_duration);

ep_mean = mean(stacked_pulses);

% De-trend
ep_post = ep_mean(post_artifact_idx);
ep_mean = ep_mean - mean(ep_post);

ep_std = std(stacked_pulses);
ep_se = ep_std / sqrt(size(stacked_pulses,1));
ep_ci = ep_se * 1.96;

end

