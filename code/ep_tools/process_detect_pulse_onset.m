function [pulse_onset_times, pulse_onset_idx]  = process_detect_pulse_onset( ...
    pulse_data, t_start, frequency, sampling_rate, threshold)
% DETECT_PULSE_ONSET Summary of this function goes here

d_pulse_data                = diff(pulse_data);

if nargin < 6
    threshold = std(d_pulse_data);
end

ttl_start_idx               = floor((t_start - .001) * sampling_rate);
ttl_start_idx               = max(ttl_start_idx, 1);

d_pulse_segment             = d_pulse_data(ttl_start_idx:end);

onset_idx                   = find(d_pulse_segment > threshold, 1);

index                       = 1;

while ~isempty(onset_idx)
    pulse_onset_idx(index)      = ttl_start_idx + onset_idx - 1;
    pulse_onset_times(index)    = pulse_onset_idx(index)/sampling_rate;
    
    ttl_start_idx               = pulse_onset_idx(index) + 100;

    d_pulse_segment             = d_pulse_data(ttl_start_idx:end);

    onset_idx                   = find(d_pulse_segment > threshold, 1);
    index                       = index + 1;
    
    if index == 1000
       index; 
    end
end
end

