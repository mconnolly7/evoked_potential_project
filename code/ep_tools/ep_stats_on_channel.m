function [ep_mean, ep_std, ep_ci] = ep_stats_on_channel(ep_data, pulse_onset_times, sampling_rate)
%EP_STATS_ON_CHANNEL Summary of this function goes here
%   Detailed explanation goes here

n_pulses            = size(pulse_onset_times, 2);
ep_window_time      = 0.2;
ep_window_idx       = floor(ep_window_time * sampling_rate);

pulse_onset_idx     = floor(pulse_onset_times * sampling_rate);
pulse_offset_idx    = pulse_onset_idx + ep_window_idx;

for c1 = 1:n_pulses-1
    pulse_idx           = pulse_onset_idx(c1):pulse_offset_idx(c1);
    pulse_data(c1,:)    = ep_data(pulse_idx);
end

ep_mean = mean(pulse_data);
ep_std  = std(pulse_data, [], 1);
ep_ci   = ep_std / sqrt(n_pulses);
end

