% function [outputArg1,outputArg2] = generate_stimulation_setting(inputArg1,inputArg2)
% %GENERATE_STIMULATION_SETTING Summary of this function goes here
% %   Detailed explanation goes here
% outputArg1 = inputArg1;
% outputArg2 = inputArg2;
% end
% 

close all
sampling_rate = 24414.062; % Hz
pulse_frequency = 10; % Hz
pulse_amplitude = 100; % uA
train_duration = 10; % s
pulse_width = 100; % us

pulse_period_seconds = 1/pulse_frequency; % Hz
pulse_period_sample = floor(pulse_period_seconds * sampling_rate);

pulse_width_seconds = pulse_width / 1e6;
pulse_width_sample = floor(pulse_width_seconds * sampling_rate); % us

n_pulses = train_duration * pulse_frequency;

pulse_amplitude = [0 pulse_amplitude*ones(1,pulse_width_sample) -1*pulse_amplitude*ones(1,pulse_width_sample) 0];
pulse_waveform = zeros(1, pulse_period_sample);
pulse_waveform(1:size(pulse_amplitude,2)) = pulse_amplitude;

pulse_train = repmat(pulse_waveform, 1, n_pulses);
plot(pulse_train)
% pulse_index = mod(t_sample, pulse_period_sample) == 0;
% 
% plot(t(pulse_index), zeros(1,sum(pulse_index)), '*')

