clear;  
% close all; 
% clc
project_path  = '/Users/mconn24/OneDrive - Emory University/projects/';
experiment_path = [project_path 'thalamic_dbs_effect_on_cortex/nhp/RTv18/2022_09_21_corticothalamic_bipolar_probe/'];

processed_dir   = [experiment_path 'segmented_data/'];

experiment_data_path = [processed_dir 'experiment_data' '.mat'];

sampling_rate = 24414.065;
sampling_period = 1/sampling_rate;

segment_length_time = 0.01;
segment_length_idx = segment_length_time * sampling_rate;

artifact_offset_pre = -.001;
artifact_offset_post = .001;
pulse_window_duration = .01;

t_start = artifact_offset_pre + sampling_period;
t_end = artifact_offset_pre + segment_length_time;

t = (t_start:sampling_period:t_end)*1000;

post_artifact_idx = floor(artifact_offset_post-artifact_offset_pre*sampling_rate):size(t,2);

load(experiment_data_path)

valid_idx = experiment_table.train_duration == 1;
valid_idx = valid_idx & experiment_table.cathode == 1;
valid_idx = valid_idx & experiment_table.anode == 0;
valid_idx = valid_idx & experiment_table.amplitude == 300;

unique_depths = unique(experiment_table.depth);
valid_table = experiment_table(valid_idx,:);

f = figure('Position', [680 1 547 1096]);
% ax = axes('Position', [.05 .1 .8 .9]);  
hold on

n_plots = sum(valid_idx);

viridis_map = flip(viridis(n_plots));

for c1 = 1:size(unique_depths, 1)
    
    file_idx = valid_table.depth == unique_depths(c1);
    stimulation_file = valid_table.file_name{file_idx}
valid_table.depth(file_idx)
    load([processed_dir stimulation_file '.mat'])

    [pulse_onset_times, pulse_onset_idx] = process_detect_pulse_onset(sync_data, 0, 0, sampling_rate);

%     data_neuro_sig = ctx_data(2,:) - ctx_data(4,:);
    data_neuro_sig = zscore(ctx_data(3,:)') - zscore(ctx_data(1,:)');
    stacked_pulses = process_stack_pulse_window(data_neuro_sig, pulse_onset_idx, ...
            sampling_rate, pulse_window_duration);
    
    ep_mean = mean(stacked_pulses);

    % De-trend
    ep_post = ep_mean(post_artifact_idx);
    ep_mean = ep_mean - mean(ep_post);
    
    ep_std = std(stacked_pulses);
    ep_se = ep_std / sqrt(size(stacked_pulses,1));
    ep_ci = ep_se * 1.96;

    hold on

    h(c1) = plot(t, ep_mean - 1*10^-1*c1, '-', 'LineWidth', 3, 'color', viridis_map(c1,:));

%     title_string = sprintf('%d-/%d+', cathode, anode );
%     title(title_string)

end
    
xlim([-1 9])
ylim([-1.7 0])

set(gca, 'FontSize', 14)