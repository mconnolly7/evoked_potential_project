clear;  
close all; 
clc
% f = figure('Position', [-1137 495 918 420]);
project_dir             = '/Users/mconn24/OneDrive/projects/STN_EP_project/';
experiment_date         = '2021_12_17';
experiment_dir          = [project_dir 'data/Yerkes/Fluffy/' experiment_date '/'];
processed_dir           = [experiment_dir 'segmented_data/'];
experiment_data_path    = [processed_dir 'experiment_data_' experiment_date '.mat'];

sampling_rate           = 30000;

segment_length_time     = 0.049;
segment_length_idx      = segment_length_time * sampling_rate;

artifact_offset_pre     = -.001;
artifact_offset_post    = .001;

t                       = (artifact_offset_pre:1/30000:(artifact_offset_pre + segment_length_time))*1000;

post_artifact_idx       = (artifact_offset_post-artifact_offset_pre)*sampling_rate:size(t,2) ;

load(experiment_data_path)

unique_depths           = unique(experiment_table.depth);
n_depths                = size(unique_depths,1);

amplitude               = [200 ];
phases                  = 0;
depths                  = unique_depths'; [30];
contacts                = [0 1 2 3];
   
filter_vec              = combvec(amplitude, depths, phases, contacts)';
n_plots                 = size(filter_vec,1);

viridis_map             = flip(viridis(n_plots));

for c1 = 1:n_plots
    figure
    amplitude                   = filter_vec(c1,1);
    depth                       = filter_vec(c1,2);
    phase                       = filter_vec(c1,3);
    contact                     = filter_vec(c1,4);
    
    amplitude_idx               = experiment_table.amplitude_a == amplitude;
    depth_idx                   = experiment_table.depth == depth;
    phase_idx                   = experiment_table.phase == phase;
    contact_idx                 = experiment_table.contact == contact;
    
    stimulation_idx             = amplitude_idx & depth_idx & phase_idx & contact_idx;
    if(all(~stimulation_idx))
        continue
    end
    stimulation_file            = experiment_table.file_name{stimulation_idx};
    
    load([processed_dir stimulation_file '.mat'])

    pulse_onset_times           = detect_pulse_onset(sync_data, 0, 17, 30000);

    pulse_data                  = segment_ep_on_channel(emg_data, pulse_onset_times, sampling_rate, segment_length_time);

    % Center each pulse by subtracting average
    pulse_mean                  = mean(pulse_data(:,:,post_artifact_idx),3);
    pulse_data                  = pulse_data - pulse_mean;

    % Scale by standard deviation
    pulse_std                   = std(pulse_data(:,:,post_artifact_idx),[],3);
    pulse_data                  = pulse_data ./ pulse_std;
    
    % Re-reference pulses between channels
    pulse_segment_channel       = squeeze(pulse_data(1,:,:) - pulse_data(2,:,:));
    hold on 
%     plot(t,mean(squeeze(pulse_data(3,:,:))))
%     plot(t,mean(squeeze(pulse_data(4,:,:))))
    
%     a = mean(squeeze(pulse_data(2,:,:)));
%     b = mean(squeeze(pulse_data(4,:,:)));

%     a = squeeze(pulse_data(4,:,:));
%     [coeff,score,latent,tsquared, explained, mu]  = pca(a);
    
%     aa = coeff(:,2:end) * score(:,2:end)' ;
%     p = polyfit(a(post_artifact_idx), b(post_artifact_idx), 1);
%     a_scale = polyval(p, a);
%     plot(mean(aa,2))
%     plot(b)
%     plot(a_scale)
%     plot(b-a_scale)
%     scatter(a,b);
%     continue
    pulse_segment_channel       = squeeze(pulse_data(1,:,:));
%     pulse_segment_channel       = squeeze(pulse_data(1,:,:) - pulse_data(2,:,:));

    ep_mean(c1,:)               = mean(pulse_segment_channel);
%         ep_mean(c2,:)               = ep_mean(c2,:) / std(ep_mean(c2,post_artifact_idx));
%         params.Fs = 30000;
%         params.fpass = [5 200];
%         [S, t, f] = mtspecgramc(lfp_data(2,:) - lfp_data(4,:), [1 .1], params);
%         plot_matrix(S,t, f)
%     [~, pulse_dc_offset_idx]   	= min(abs(t - 2.5));
%     ep_mean(c1,:)               = ep_mean(c1,:) - ep_mean(c1,pulse_dc_offset_idx) - c1*5;

    ep_mean(c1,:)             = movmean(ep_mean(c1,:), 20); 

    ep_std      = std(pulse_segment_channel);
    ep_se       = ep_std / sqrt(size(pulse_segment_channel,1));
    ep_ci       = ep_se * 1.96;

    hold on

    plot(t, ep_mean(c1,:) + ep_ci, 'k')
    plot(t, ep_mean(c1,:) - ep_ci, 'k')
    h(c1)       = plot(t, ep_mean(c1,:), '-', 'LineWidth', 3, 'color', viridis_map(c1,:));
    labels{c1}  = sprintf('%.1f mm', depth);
        

    %     figure
    % plot(lfp_data(2,:))
    % hold on
    % plot(lfp_data(4,:))
    % plot(lfp_data(2,:) - lfp_data(4,:))
   
%     ylim([-70 5])
%     xlim([min(t), 10])
%     
%     title(sprintf('Amplitude: %d µA', amplitude))
    title(sprintf('Depth: %d mm, Contact C+/%d-', depth, contact))
    
%     yticks([])
%     yticklabels([])
%     ylim([-.5 .5])
    set(gca, 'FontSize', 14)
%     set(gca,'visible','off')
end
return
% subplot(1,3,1)
xlabel('Milliseconds')
% ylabel('µV')

% plot([40 50], [-425 -425], 'k','LineWidth', 3)
% plot([50 50], [-425 -400], 'k','LineWidth', 3)
% l = legend(h(1:end),labels(1:end), 'box', 'off', 'Position', [0.9003 0.2929 0.0991 0.5619]);

% print('cep_figure', '-dpng', '-r300')
%%
depth_up_idx    = 1:n_depths;
t_up_idx        = t>2.5 & t <= 20;
t_up            = t(t_up_idx);
depth_up        = unique_depths(depth_up_idx);
ep_mean_up      = ep_mean(depth_up_idx,t_up_idx);

% plot_matrix(ep_mean_up', t_up, depth_up, 'n') 

x_1             = repmat(t_up, size(depth_up_idx,2),1);
x_2             = repmat(depth_up',1,size(t_up,2));

x_data          = [reshape(x_1,[],1) reshape(x_2,[],1)];
y_data          = reshape(ep_mean_up,[],1);
ep_gp           = gpr_model();

rand_idx        = randperm(size(x_data,1),5000);
ep_gp.initialize_data(x_data(rand_idx,:), y_data(rand_idx,1))
ep_gp.minimize(1)

%
figure
h = ep_gp.plot_mean;
colormap(magma)

%
xlabel('Time (ms)')
% ylabel('Depth (mm)')
yticklabels
% xlim([1 48]) 
% ylim([36 40.1])

set(gca,'FontSize', 15)

view(0,90)
set(gca, 'YDir','reverse')
%%

for c1 = 1:1000
    x1 = rand(1000,1)*10-5;
    x2 = rand(1000,1)*10-5;

    y = x1.^2 + x2.^2;

    [~, min_idx] = min(abs(y-2.5^2));
    x_test(c1,:) = [x1(min_idx), x2(min_idx)];
end
scatter(x_test(:,1), x_test(:,2))






