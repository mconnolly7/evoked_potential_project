clear;  
close all; 
clc
f = figure('Position', [396 677 918 420]);
project_dir             = '/Users/mconn24/OneDrive/projects/STN_EP_project/';
experiment_date         = '2021_06_28';
experiment_dir          = [project_dir 'data/Yerkes/Fluffy/' experiment_date '/'];
processed_dir           = [experiment_dir 'segmented_data/'];
experiment_data_path    = [processed_dir 'experiment_data'];

sampling_rate           = 30000;
artifact_offset_pre     = -.001;
artifact_offset_post    = .001;

segment_length_time     = 0.049;
segment_length_idx      = segment_length_time * sampling_rate;

t                       = (artifact_offset_pre:1/30000:(artifact_offset_pre + segment_length_time))*1000;

load(experiment_data_path)
amp     = [200 ];
phase 	= 2;

unique_depths           = [36; 42];unique(experiment_table.depth);
% unique_depths(unique_depths < 38) = [];
n_depths                = size(unique_depths,1);
viridis_map             = flip(viridis(n_depths));
    
ax = axes('Position', [.05 .1 .8 .9]);  
hold on

for c1 = 1:size(amp,2)
%     subplot(1,3,c1)
    
    for c2 = 1:n_depths
        subplot(2,1,c2)
        depth                       = unique_depths(c2);
        depth_idx                   = experiment_table.depth == depth;
        polarity_idx                = experiment_table.phase == phase;
        amplitude_idx               = experiment_table.amplitude_a == amp(c1);
        stimulation_idx             = depth_idx & polarity_idx & amplitude_idx;

        stimulation_file            = experiment_table.file_name{stimulation_idx};
        load([processed_dir stimulation_file '.mat'])

        pulse_onset_times           = detect_pulse_onset(sync_data, 0, 20, 30000);
        
%         lfp_data(1,:)  = bandstop(lfp_data(2,:), [65 70], 30000);
%         lfp_data(2,:)  = bandstop(lfp_data(4,:), [65 70], 30000);
        
%         lfp_data(1,:)  = medfilt1(lfp_data(2,:), 70);
%         lfp_data(2,:)  = medfilt1(lfp_data(4,:), 70);
        % Collect each segment
        for c3 = 1:size(pulse_onset_times,2)
            segment_start_idx       = floor((pulse_onset_times(c3) + artifact_offset_pre) * sampling_rate) ;
            segment_end_idx         = segment_start_idx + segment_length_idx;

            pulse_segment(c3,:,:)   = lfp_data(:,segment_start_idx:segment_end_idx); 
        end

        % Center each pulse by subtracting average
        post_artifact_idx           = (artifact_offset_post-artifact_offset_pre)*sampling_rate:size(pulse_segment,3) ;
        pulse_dc_offset             = mean(pulse_segment(:,:,post_artifact_idx),3);
        pulse_segment               = pulse_segment - pulse_dc_offset;

        % Re-reference pulses between channels
        pulse_segment_channel       = squeeze(pulse_segment(:,4,:) - pulse_segment(:,2,:));

        %     pulse_segment_channel       = squeeze(pulse_segment(:,4,:));
        ep_mean(c2,:)               = mean(pulse_segment_channel);
%         ep_mean(c2,:)               = ep_mean(c2,:) / std(ep_mean(c2,post_artifact_idx));
%         params.Fs = 30000;
%         params.fpass = [5 200];
%         [S, t, f] = mtspecgramc(lfp_data(2,:) - lfp_data(4,:), [1 .1], params);
%         plot_matrix(S,t, f)
        [~, pulse_dc_offset_idx]   	= min(abs(t - 2.5));
        ep_mean(c2,:)               = ep_mean(c2,:) - ep_mean(c2,pulse_dc_offset_idx) - c2*5;
        
        ep_smooth(c2,:) = movmean(ep_mean(c2,:), 20); 
        
        ep_std      = std(pulse_segment_channel);
        ep_se       = ep_std / sqrt(size(pulse_segment_channel,1));
        ep_ci       = ep_se * 1.96;

        hold on

%         h(c2)       = plot(t, ep_smooth(c2,:), '-', 'LineWidth', 3, 'color', viridis_map(c2,:));
        h(c2)       = plot(t, ep_smooth(c2,:), '-', 'LineWidth', 5, 'color', [51 113 181]/255);
        labels{c2}  = sprintf('%.1f mm', depth);
        

    %     figure
    % plot(lfp_data(2,:))
    % hold on
    % plot(lfp_data(4,:))
    % plot(lfp_data(2,:) - lfp_data(4,:))
    
    ylim([-30 5])
%     xlim([min(t), 10])
    xlim([.5, 25])
    
%     title(sprintf('Amplitude: %d µA', amp(c1)))
    
%     yticks([])
%     yticklabels([])
    
    set(gca, 'FontSize', 28)
%     set(gca,'visible','off')
    end
end

% subplot(1,3,1)
% xlabel('Milliseconds')
% ylabel('µV')

% plot([40 50], [-425 -425], 'k','LineWidth', 3)
% plot([50 50], [-425 -400], 'k','LineWidth', 3)
% l = legend(h(1:end),labels(1:end), 'box', 'off', 'Position', [0.9003 0.2929 0.0991 0.5619]);
print('cep_figure', '-dpng', '-r300')
%%
depth_up_idx    = 1:n_depths;
t_up_idx        = t>2.5 & t <= 20;
t_up            = t(t_up_idx);
depth_up        = unique_depths(depth_up_idx);
ep_mean_up      = ep_mean(depth_up_idx,t_up_idx);

% plot_matrix(ep_mean_up', t_up, depth_up, 'n') 

x_1             = repmat(t_up, size(depth_up_idx,2),1);
x_2             = repmat(depth_up',1,size(t_up,2));

x_data          = [reshape(x_1,[],1) reshape(x_2,[],1)];
y_data          = reshape(ep_mean_up,[],1);
ep_gp           = gpr_model();

rand_idx        = randperm(size(x_data,1),5000);
ep_gp.initialize_data(x_data(rand_idx,:), y_data(rand_idx,1))
ep_gp.minimize(1)

%
figure
h = ep_gp.plot_mean;
colormap(magma)

%
xlabel('Time (ms)')
% ylabel('Depth (mm)')
yticklabels
% xlim([1 48]) 
% ylim([36 40.1])

set(gca,'FontSize', 15)

view(0,90)
set(gca, 'YDir','reverse')
%%
x1 = rand(1000,1);
x2 = rand(1000,1);

x = [x1 x2];
for c1 = 1:1000
    x(c1,:);
end



%%


for c1 = 1:11
    
    [x(c1,:), y(c1,:)] = ginput(2);
    squeeze(x(c1,:,:)) 
    squeeze(y(c1,:,:))
    scatter(x(c1,1), y(c1,1))
    scatter(x(c1,2), y(c1,2))

end

%%
load('meta_bayesian_optimization/data/cep_300uA.mat')
for c1 = 1:11
     d(c1,3) = y(c1,1) - y(c1,2);
     disp(d)
end
% csvwrite('300uA', d)


%%
[x,y] = meshgrid(1:11,100:100:300);
dd = d';
dd(3,8) = mean(dd(3,[7 9]));

x1 = reshape(x,[],1);
x2 = reshape(y,[],1);
y_data = reshape(dd,[],1);

data = [x1 x2 y_data]

%%
[~, idx]= min(abs(t-2))
