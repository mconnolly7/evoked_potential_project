clear;  
close all; 
clc
f = figure('Position', [829 541 718 342]);
project_dir = '/Users/mconn24/OneDrive/projects/STN_EP_project/';
experiment_date = '2022_08_26';

sampling_rate = 24414.065;
sampling_period = 1/sampling_rate;

segment_length_time = 0.01;
segment_length_idx = segment_length_time * sampling_rate;

artifact_offset_pre = -.001;
artifact_offset_post = .001;
pulse_window_duration = .01;

t_start = artifact_offset_pre + sampling_period;
t_end = artifact_offset_pre + segment_length_time;

t = (t_start:sampling_period:t_end)*1000;

post_artifact_idx = floor(artifact_offset_post-artifact_offset_pre*sampling_rate):size(t,2);

cathode = 1;
anode = 3;
amplitude = -300;

% 35 mm 
experiment_name = 'bipolar_ring_grid_35mm';
experiment_dir = [project_dir 'data/Yerkes/Fluffy/' experiment_date '/' experiment_date '_' experiment_name '/'];
processed_dir = [experiment_dir 'segmented_data/'];
experiment_data_path = [processed_dir 'experiment_data_' experiment_date '.mat'];

load(experiment_data_path)

cathode_idx = experiment_table.cathode == cathode;
anode_idx = experiment_table.anode == anode;
amplitude_idx = experiment_table.amplitude == amplitude;
plot_idx = cathode_idx & anode_idx & amplitude_idx;

stimulation_file = experiment_table.file_name{plot_idx};
load([processed_dir stimulation_file '.mat'])

ep_mean_36 = process_resolve_ep(sync_data, ctx_data, pulse_window_duration, post_artifact_idx, sampling_rate);

% 42 mm 
experiment_name = 'bipolar_ring_grid_42mm';
experiment_dir = [project_dir 'data/Yerkes/Fluffy/' experiment_date '/' experiment_date '_' experiment_name '/'];
processed_dir = [experiment_dir 'segmented_data/'];
experiment_data_path = [processed_dir 'experiment_data_' experiment_date '.mat'];

load(experiment_data_path)

cathode_idx = experiment_table.cathode == cathode;
anode_idx = experiment_table.anode == anode;
amplitude_idx = experiment_table.amplitude == amplitude;
plot_idx = cathode_idx & anode_idx & amplitude_idx;

stimulation_file = experiment_table.file_name{plot_idx};
load([processed_dir stimulation_file '.mat'])

ep_mean_42 = process_resolve_ep(sync_data, ctx_data, pulse_window_duration, post_artifact_idx, sampling_rate);


subplot(2,1,1)
plot(t, ep_mean_36*1e6, 'LineWidth', 3)
xlim([-0.5,9])
ylim([-6 6]*10)
set(gca, 'FontSize', 20)
box off

subplot(2,1,2)
plot(t, ep_mean_42*1e6, 'LineWidth', 3)
xlim([-0.5,9])
ylim([-6 6]*10)
set(gca, 'FontSize', 20)
box off

