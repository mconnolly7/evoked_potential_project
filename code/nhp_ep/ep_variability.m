%%
data_dir = '/Users/mconn24/OneDrive/grants/R21/2021_10_29_R21_dbs_optimization_A0/data/';
load([data_dir 'f18_RT4D-5.501F0005_R_STN_evoked_part1_preprocessed.mat'])

%%
data_dir = '/Users/mconn24/OneDrive/grants/R21/2021_10_29_R21_dbs_optimization_A0/data/';
load([data_dir 'f19_RT4D-5.501F0006_R_STN_evoked_part2_preprocessed.mat'])
%%
stim_idx                = 6;
artifact_offset_pre     = -.001;
sampling_rate           = 22000;
t2_window               = ([.040 .090] - artifact_offset_pre) * sampling_rate;
stim_start_idx          = (stim_start_times(stim_idx) -2.5 ) * sampling_rate;
stim_stop_idx           = stim_start_idx + 15.5 * sampling_rate;

d                       = ecog(5,stim_start_idx:stim_stop_idx) - ecog(6,stim_start_idx:stim_stop_idx);
d_med                   = medfilt1(d,50);
%%
bands = [1 5; 
    5 10; 
    10 15;
    15 20;
    20 25;
    25 30;
    30 35;
    35 40];

for c1 = 1:3%size(bands,1)
    d_band(c1,:)          = bandpass(d_med, bands(c1,:), 22000);
    d_hil(c1,:)           = hilbert(d_band(c1,:));
    d_phase(c1,:)         = angle(d_hil(c1,:));
    d_mag(c1,:)           = abs(d_hil(c1,:));
    c1
end

%%
segment_length_time     = 0.099;
segment_length_idx      = segment_length_time * sampling_rate;

clear phase_state

clear pulse_ecog pulse_phase pulse_alpha pulse_band pulse_mag band_peak

pulse_onset_times           = stim_pulse_times{stim_idx} - stim_start_times(stim_idx);

for c1 = 1:size(pulse_onset_times,2)
    segment_start_idx       = floor((pulse_onset_times(c1) + artifact_offset_pre + 2.5) * sampling_rate) ;
    segment_end_idx         = segment_start_idx + segment_length_idx;

    pulse_ecog(c1,:)        = d(segment_start_idx:segment_end_idx); 
    pulse_phase(c1,:,:)     = d_phase(:, segment_start_idx:segment_end_idx);
    pulse_band(c1,:,:)      = d_band(:, segment_start_idx:segment_end_idx);
    pulse_mag(c1,:,:)      	= d_mag(:, segment_start_idx:segment_end_idx);
end

[pulse_peak(stim_idx,:), peak_idx]               = max(abs(pulse_ecog),[],2);

[pulse_t2(stim_idx,:), peak_idx]               = min(pulse_ecog(:,t2_window(1):t2_window(2)),[],2);
% for c1 = 1:120
%     pulse_peak(stim_idx,c1)  = pulse_ecog(c1,peak_idx(c1));
% end

band_mean(stim_idx,:,:)   	= mean(pulse_band(1:120,:,:),3);
band_power(stim_idx,:,:)   	= sum(pulse_band(1:120,:,:).^2,3);
band_mag_mean(stim_idx,:,:) = mean(pulse_mag(1:120,:,:),3);
band_mag_end(stim_idx,:,:)  = pulse_mag(1:120,:,end);
phase_state(stim_idx,:,:)   = pulse_phase(1:120,:,23);
mag_state(stim_idx,:,:)     = pulse_mag(1:120,:,1);
    

%%
%
% Plots mag_b = GP(phase_b, mag_b) 
%

close all
clc
hold on
clear x_data
clear gp_model
v = viridis(8);
band_idx = [1 2 3];
for c1 = 1:size(band_idx,2)
        subplot(1,3,c1)

    x_data(:,1) = squeeze(phase_state(stim_idx,:,c1))' * 180/pi;
    x_data(:,2) = squeeze(mag_state(stim_idx,:,c1))';
    x_test = (-180:180)';
%     y_data = squeeze(band_mag_end(stim_idx,:,c1))' - squeeze(mag_state(stim_idx,:,c1))';
    y_data = pulse_t2(stim_idx,:)';
    
    gp_model(c1) = gpr_model;
    gp_model(c1).initialize_data(x_data, y_data);
    gp_model(c1).minimize(5)
    hold on
    gp_model(c1).plot_mean
    gp_model(c1).plot_data

    xlabel('Phase at Pulse Onset (degrees)');
    ylabel('Mean Band');
    title(sprintf('Band [%d - %d] Hz', bands(band_idx(c1),1), bands(band_idx(c1),2)))
    
    set(gca, 'Fontsize', 14)
    xlim([-180 180])
    ylim([min(x_data(:,2)) max(x_data(:,2))])
    colormap(viridis)
    view(30,20)
end
% legend(h,{'1-5 Hz', '5-10 Hz','10-15 Hz','15-20 Hz', '20-25 Hz', ...
%     '25-30 Hz', '30-35 Hz', '35-40 Hz'}, 'Box', 'Off')



%%
%
% Plot the pulse magnitude over time, sorted by phase
%
c_max = 0;
c_min = 0;

% close all
for c1 = 1:8
    t   = (1:2179)';
    tt  = repmat(t,1, 120);
    ttt = reshape(tt,[],1);
        
    z   = squeeze(pulse_mag(:,c1,t))';
    zz  = reshape(z,[],1);
    
%     p   = repmat(squeeze(phase_state(stim_idx,:,c1))*180/pi,size(t,1),1);
    p   = repmat(squeeze(phase_state(stim_idx,:,c1)),size(t,1),1);
    pp  = reshape(p,[],1);
    
    rand_idx = randperm(size(ttt,1),2000);
    gp_band(c1) = gpr_model();
    gp_band(c1).initialize_data([ttt(rand_idx)/22 pp(rand_idx)], zz(rand_idx))
%     gp_band(c1).minimize(1);

%     zzz = gp_band(c1).predict([ttt/22, pp]);
%     c_max = max(c_max, max(zzz));
%     c_min = min(c_min, min(zzz));

    subplot(2,4,c1)
    
    gp_band(c1).plot_mean
    gp_band(c1).plot_data
    view(0,90)
    
    xlabel('Milliseconds');
    ylabel('Phase (degrees)')
    set(gca, 'FontSize', 14);
    title(sprintf('%d-%d Hz', bands(c1,1), bands(c1,2)))
    
%     ylim([-180 180])
    xlim([min(ttt/22) max(ttt/22)])
%     caxis([-30.0055 35.4236])
    
    colormap(viridis)
 
end

%%
% pulse_peak = GP(phase_state)
%

%%
%
% RSVM to predict line length 
%
band = 1
close all
X1  = squeeze(phase_state(stim_idx,:,:));
X2  = squeeze(mag_state(stim_idx,:,:));
X   = zscore([X1(:,1:3) X2(:,1:3)]);
Y   = pulse_t2(stim_idx,:)';
% for band = 1:8
%     subplot(2,4,band)
%     Y   = squeeze(band_mag_mean(stim_idx,:,band))' - squeeze(mag_state(stim_idx,:,band))';
    cv_model = cvpartition(120,'KFold', 12);


    y_est_  = []; 
    y_test_ = []; 
    for c1 = 1:12
        x_train         = X(training(cv_model,c1),:);
        y_train         = Y(training(cv_model,c1));

        x_test          = X(test(cv_model,c1),:);
        y_test_(test(cv_model,c1))   = Y(test(cv_model,c1));
        gp_model        = gp_object();
        
        gp_model.initialize_data(x_train, y_train)
        gp_model.minimize(10)
        svm_model       = fitrsvm(x_train, y_train, 'Standardize', true, 'KernelFunction','linear', 'KernelScale', 'auto');

        y_est_(test(cv_model,c1))    = svm_model.predict(x_test);
        y_est_(test(cv_model,c1))    = gp_model.predict(x_test);
    end
    %
    % close all
    % figure
    y_est   = reshape(y_est_,[],1);
    y_test  = reshape(y_test_,[],1);
    scatter(y_est,y_test)
    [a(band), b(band)] = corr(y_est,y_test, 'type','Spearman');

    p = polyfit(y_est,y_test,1);
    x_lin = linspace(min(y_est), max(y_est), 100);
    y_lin = polyval(p, x_lin);
    hold on
    plot(x_lin, y_lin)

    set(gca, 'Fontsize', 14)
    legend({'Line Length','Linear Regression'}, 'Box', 'On')
    xlabel('Estimated');
    ylabel('Actual');
    title(sprintf('T2 Amplitude - Corr = %.2f, P = %.2e', a(band), b(band)))
    
gp_model.initialize_data(X, Y)
gp_model.minimize(10)
% end
%%
close all
threshold = [-90 90]*pi/180;

phase_t = phase_state(stim_idx,:,4) > threshold(1) & phase_state(stim_idx,:,4) < threshold(2);
% phase_t = phase_t | (phase_state(stim_idx,:,4) > threshold(1) & phase_state(stim_idx,:,4) < threshold(2))
hold on
% phase_t = true(1,120);
plot((1:length(d_mag))/22000, d_med,'LineWidth', 2)
% plot((1:length(d_mag))/22000, d_phase(4,:)','LineWidth', 2)
scatter((pulse_onset_times(phase_t) + 2.5), zeros(sum(phase_t),1)+5,36,'r', 'filled')
scatter((pulse_onset_times(~phase_t) + 2.5), zeros(sum(~phase_t),1)+5,36,'k')




%%
% close all
t = (1:size(d_med,2))/22000;
hold on
plot(t,d)
% plot(t,d_med)
plot(resample(t,1000,22000), resample(d_med,1000,22000))
sum(abs(pulse_ecog),2)

%%
close all
t = (1:size(d_med,2))/22000;
hold on
plot(t,d, 'Color', .5*[1 1 1])
plot(t,d_med, 'Color', [.9 0 0], 'LineWidth',2)
xlabel('Seconds')
set(gca, 'Fontsize', 14)
set(gcf,'Color', 'w')


%%
nhp_dir = '/Users/mconn24/OneDrive - Emory University/STN_EP_project/data/Fluffy/2021_09_13/segmented_data/';

load([nhp_dir 'experiment_data.mat'])

load([nhp_dir 'segmented_data_0007.mat'])
