clear;  
close all; 
clc
% f = figure('Position', [-1137 495 918 420]);
project_dir = '/Users/mconn24/OneDrive/projects/STN_EP_project/';

experiment_name = 'bipolar_ring_grid_34mm';
% experiment_name = 'bipolar_ring_grid_36mm';
% experiment_name = 'bipolar_ring_grid_37mm';
% experiment_name = 'bipolar_ring_grid_38mm';
% experiment_name = 'bipolar_ring_grid_39mm';
% experiment_name = 'bipolar_ring_grid_40mm';
% experiment_name = 'bipolar_ring_grid_41mm';
% experiment_name = 'bipolar_ring_grid_42mm';
% experiment_name = 'bipolar_ring_grid_43mm';
% experiment_name = 'bipolar_ring_grid_44mm';
% experiment_name = 'bipolar_ring_grid_45mm';

% experiment_name = 'monopolar_segmented_grid_40mm';
% experiment_name = 'monopolar_segmented_grid_41mm';
% experiment_name = 'monopolar_segmented_grid_42mm';
% experiment_name = 'monopolar_segmented_grid_43mm';
% experiment_name = 'monopolar_segmented_grid_44mm';

experiment_date = '2022_08_26';
experiment_dir = [project_dir 'data/Yerkes/Fluffy/' experiment_date '/' experiment_date '_' experiment_name '/'];
processed_dir = [experiment_dir 'segmented_data/'];
experiment_data_path = [processed_dir 'experiment_data_' experiment_date '.mat'];

sampling_rate = 24414.065;
sampling_period = 1/sampling_rate;

segment_length_time = 0.01;
segment_length_idx = segment_length_time * sampling_rate;

artifact_offset_pre = -.001;
artifact_offset_post = .001;
pulse_window_duration = .01;

t_start = artifact_offset_pre + sampling_period;
t_end = artifact_offset_pre + segment_length_time;

t = (t_start:sampling_period:t_end)*1000;

post_artifact_idx = floor(artifact_offset_post-artifact_offset_pre*sampling_rate):size(t,2);

load(experiment_data_path)

cathode = [0 1 2 3];
anode = [0 1 2 3];
amplitude = [100 200 300 ];

ax = axes('Position', [.05 .1 .8 .9]);  
hold on

filter_vec = combvec(cathode, anode, amplitude)';
n_plots = size(experiment_table,1);

viridis_map = flip(viridis(3));

for c1 = 1:n_plots

    stimulation_file = experiment_table.file_name{c1};

    cathode = experiment_table.cathode(c1);
    anode = experiment_table.anode(c1);
    amplitude = experiment_table.amplitude(c1);

    sub_plot_idx = cathode*4 + anode + 1;
    subplot(4,4,sub_plot_idx)

    load([processed_dir stimulation_file '.mat'])

    [pulse_onset_times, pulse_onset_idx] = process_detect_pulse_onset(sync_data, 0, 0, sampling_rate);

    data_neuro_sig = ctx_data(2,:) - ctx_data(4,:);
    stacked_pulses = process_stack_pulse_window(data_neuro_sig, pulse_onset_idx, ...
            sampling_rate, pulse_window_duration);
    
    ep_mean = mean(stacked_pulses);

    % De-trend
    ep_post = ep_mean(post_artifact_idx);
    ep_mean = ep_mean - mean(ep_post);
    
    ep_std = std(stacked_pulses);
    ep_se = ep_std / sqrt(size(stacked_pulses,1));
    ep_ci = ep_se * 1.96;

    hold on

    h(c1) = plot(t, ep_mean, '-', 'LineWidth', 3, 'color', viridis_map(amplitude/-100,:));

    ylim([-6 6]*10^-5)
    title_string = sprintf('%d-/%d+', cathode, anode );
    title(title_string)
    set(gca, 'FontSize', 14)
end
