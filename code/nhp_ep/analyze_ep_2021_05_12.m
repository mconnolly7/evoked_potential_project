clear; close all
project_dir     = '/Users/mconn24/OneDrive - Emory University/STN_EP_project/';
experiment_date = '2021_05_12';
experiment_dir  = [project_dir 'data/Fluffy/' experiment_date '/'];

sampling_rate           = 30000;
artifact_offset         = -.001;
segment_length          = 0.049;
segment_length_idx      = segment_length * sampling_rate;

t                       = (artifact_offset:1/30000:(artifact_offset + segment_length))*1000;

depth = [37 38 39 40 41 42 43]; 

% pairs = [10 14; 15 16; 17 18; 19 20; 21 22; 23 24; 26 27];
% experiment_ids = [10 14; 15 16; 17 18; 19 20; 21 22; 23 24; 26 27];
experiment_id = 6;
% for c1 = 33

file_base           = sprintf(['Fluffy_' experiment_date '_%03d'], experiment_id);

% Load stimulation_table
stim_table_path     = [experiment_dir file_base '_stimulation_table.mat'];
load(stim_table_path);

% Load raw LFP data
raw_data_path       = [experiment_dir file_base '.ns6'];
raw_data_struct   	= openNSx(raw_data_path);
sync_data           = double(raw_data_struct.Data(16,:));
depth               = 41;

%%
data_1              = double(raw_data_struct.Data(1,:))';
data_2              = double(raw_data_struct.Data(2,:))';

data                = data_1 - data_2;
% data              	= data_1;

n_trials            = size(stimulation_table,1);
%%
    
close all

for c1 = 1:n_trials

    t_start             = stimulation_table.t_start(c1);
    pulse_amplitude     = stimulation_table.amplitude_a(c1);
    pulse_frequency     = stimulation_table.frequency(c1);
    pulse_width         = stimulation_table.pulse_width_a(c1);
    
    if pulse_width == 150 
        continue
    end
    
    
    % Get TTL onset times
    pulse_onset_times   = detect_pulse_onset(sync_data, t_start, pulse_frequency, sampling_rate);

    for c2 = 1:size(pulse_onset_times,2)
        segment_start_idx       = floor((pulse_onset_times(c2) + artifact_offset) * sampling_rate) ;
        segment_end_idx         = segment_start_idx + segment_length_idx;

        pulse_segment(c2,:,:)   = data(segment_start_idx:segment_end_idx,:); 

    end

    pulse_segment = pulse_segment - mean(pulse_segment(:,(.001-artifact_offset)*sampling_rate:end,:),2);
    %         title(sprintf('Trial: %d, Polarity: %s, Depth: %d', c1, stimulation_table.phase{1}, depth(c1)))

    ep_mean     = mean(pulse_segment);

    ep_std      = std(pulse_segment);
    ep_se       = ep_std / sqrt(size(pulse_segment,1));
    ep_ci       = ep_se * 1.96;

  
     
    if pulse_amplitude == 100 && pulse_frequency == 1 && pulse_width == 50
        subplot(3,4,1)
    elseif pulse_amplitude == 100 && pulse_frequency == 1 && pulse_width == 100
        subplot(3,4,2)
    elseif pulse_amplitude == 100 && pulse_frequency == 10 && pulse_width == 50
        subplot(3,4,3)
    elseif pulse_amplitude == 100 && pulse_frequency == 10 && pulse_width == 100
        subplot(3,4,4)
    elseif pulse_amplitude == 200 && pulse_frequency == 1 && pulse_width == 50   
        subplot(3,4,5)
    elseif pulse_amplitude == 200 && pulse_frequency == 1 && pulse_width == 100
        subplot(3,4,6)
    elseif pulse_amplitude == 200 && pulse_frequency == 10 && pulse_width == 50
        subplot(3,4,7)
    elseif pulse_amplitude == 200 && pulse_frequency == 10 && pulse_width == 100
        subplot(3,4,8)
    elseif pulse_amplitude == 300 && pulse_frequency == 1 && pulse_width == 50
        subplot(3,4,9)
    elseif pulse_amplitude == 300 && pulse_frequency == 1 && pulse_width == 100
        subplot(3,4,10)
    elseif pulse_amplitude == 300 && pulse_frequency == 10 && pulse_width == 50
        subplot(3,4,11)
    elseif pulse_amplitude == 300 && pulse_frequency == 10 && pulse_width == 100
        subplot(3,4,12)
    end
    hold on

    plot(t, ep_mean/4, 'k-', 'LineWidth', 2)

    %         plot(t, ep_mean + ep_ci, 'k--')
    %         plot(t, ep_mean - ep_ci, 'k--')
    xlabel('Milliseconds')
    ylabel('µV')
    set(gca, 'FontSize', 14)
    
    title(sprintf('%d uA, %d Hz, %dus', pulse_amplitude, pulse_frequency, pulse_width))
    ylim([-15 10])
    
    file_name = sprintf('Depth_%d-Amplitude_%d-Frequency_%d-Pulse Width_%d.png', depth, pulse_amplitude, pulse_frequency, pulse_width);
    drawnow
%     print(file_name, '-dpng')
end
%     legend({'Inward', 'Outward'}, 'box', 'off')    

