%%%%%%%%
% Prototype code for segmenting a parameter sweep into separate files
%
% Created for 6/10/21, running with 6/28/21 experiment of same format
%%%%%%%%
clear; close all
project_dir     = '/Users/mconn24/OneDrive/projects/STN_EP_project/';
experiment_date = '2021_11_22';
experiment_dir  = [project_dir 'data/Yerkes/Fluffy/' experiment_date '/'];
processed_dir   = [experiment_dir 'segmented_data/'];
sampling_rate   = 30000;

d = dir([experiment_dir '*.ns6']);

file_index              = 1;
experiment_table        = [];
path_table              = [];

for c1 = 1:size(d, 1)
    
    file_name           = d(c1).name;
    data_path          	= [experiment_dir file_name];
    stim_table_path     = strrep(data_path, '.ns6', '_stimulation_table.mat');
    
    % Load stimulation_table
    load(stim_table_path);

    % Load raw LFP data
    raw_data_struct   	= openNSx(data_path);
    
    sync_data_raw       = double(raw_data_struct.Data(17,:));
    ctx_data_raw        = double(raw_data_struct.Data([1 2 3 4],:));
    stn_data_raw        = double(raw_data_struct.Data(5:16,:));
         
    for c2 = 1:size(stimulation_table,1)
        segment_start_time          = stimulation_table.t_start (c2);
        segment_start_sample        = floor(segment_start_time * sampling_rate);
        
        segment_duration_time       = stimulation_table.duration(c2);
        segment_duration_sample     = segment_duration_time  * sampling_rate;
        segment_end_sample          = floor(segment_start_sample + segment_duration_sample);
        
        sync_data                   = sync_data_raw(segment_start_sample:segment_end_sample);
        ctx_data                    = ctx_data_raw(:,segment_start_sample:segment_end_sample);
        stn_data                    = stn_data_raw(:,segment_start_sample:segment_end_sample);
        pulse_onset_times           = detect_pulse_onset(sync_data, 0, 17, 30000);
        
        file_name                   = {sprintf('%s_segmented_data_%04d', experiment_date, file_index)};
        file_index                  = file_index + 1;
        
        path_table_row              = table(file_name);
        path_table                  = [path_table; path_table_row];
        save_path                   = [processed_dir file_name{1}];
        
        save(save_path, 'sync_data', 'stn_data', 'ctx_data', 'pulse_onset_times', 'sampling_rate');
    end
    
    experiment_table = [experiment_table; stimulation_table];
    
end

experiment_table            = [experiment_table path_table];
experiment_table.t_start    = [];

save([processed_dir 'experiment_data_' experiment_date], 'experiment_table')