%%%%%%%%
% Prototype code for segmenting a parameter sweep into separate files
%
% Created for 6/10/21, adapting for experiment with Heraeus electrode + EMG
% 
% 8/10/22: adapting for TDT 
%%%%%%%%
clear; close all
project_dir     = '/Users/mconn24/OneDrive/projects/STN_EP_project/';
experiment_date = '2022_08_10';

experiment_name = 'bipolar_ring_grid_37mm';
% experiment_name = 'bipolar_ring_grid_38mm';
% experiment_name = 'bipolar_ring_grid_39mm';
% experiment_name = 'bipolar_ring_grid_40mm';
% experiment_name = 'bipolar_ring_grid_41mm';
% experiment_name = 'bipolar_ring_grid_42mm';
% experiment_name = 'bipolar_ring_grid_43mm';
% experiment_name = 'bipolar_ring_grid_44mm';
% experiment_name = 'bipolar_ring_grid_45mm';

% experiment_name = 'monopolar_segmented_grid_40mm';
% experiment_name = 'monopolar_segmented_grid_41mm';
% experiment_name = 'monopolar_segmented_grid_42mm';
% experiment_name = 'monopolar_segmented_grid_43mm';
% experiment_name = 'monopolar_segmented_grid_44mm';

experiment_dir  = [project_dir 'data/Yerkes/Fluffy/' experiment_date '/' experiment_date '_' experiment_name '/'];
processed_dir   = [experiment_dir 'segmented_data/'];

if ~exist('processed_dir', 'dir')
    mkdir(processed_dir)
end

d = dir([experiment_dir '/*_T_*']);

file_index              = 1;
experiment_table        = [];
path_table              = [];

for c1 = 1:size(d, 1)
    
    file_name           = d(c1).name;
    data_path          	= [experiment_dir file_name];
    stim_table_path     = [data_path '/' file_name '.csv'];
    
    % Load stimulation_table
    stimulation_table = readtable(stim_table_path);

    % Load raw LFP data
    raw_data_struct = TDTbin2mat(data_path, 'STORE', 'cMER');
    
    ctx_data_raw        = double(raw_data_struct.streams.cMER.data(1:4,:));
    sync_data_raw       = double(raw_data_struct.streams.cMER.data(5,:));
%     stn_data_raw        = double(raw_data_struct.Data(5:16,:));
%     emg_data_raw       = double(raw_data_struct.Data([1 2],:));

    sampling_rate = raw_data_struct.streams.cMER.fs;

    for c2 = 1:size(stimulation_table,1)
        segment_start_time          = stimulation_table.stimulation_onset_time(c2);
        segment_start_sample        = floor(segment_start_time * sampling_rate);
        
        segment_duration_time       = 0.5; %stimulation_table.duration(c2);
        segment_duration_sample     = segment_duration_time  * sampling_rate;
        segment_end_sample          = floor(segment_start_sample + segment_duration_sample);
        
        sync_data                   = sync_data_raw(segment_start_sample:segment_end_sample);
        ctx_data                    = ctx_data_raw(:,segment_start_sample:segment_end_sample);
%         stn_data                    = stn_data_raw(:,segment_start_sample:segment_end_sample);
%         emg_data                    = emg_data_raw(:,segment_start_sample:segment_end_sample);
        pulse_onset_times           = detect_pulse_onset(sync_data, 0, 17, sampling_rate);
        
        file_name                   = {sprintf('%s_segmented_data_%04d', experiment_date, file_index)};
        file_index                  = file_index + 1;
        
        path_table_row              = table(file_name);
        path_table                  = [path_table; path_table_row];
        save_path                   = [processed_dir file_name{1}];
        
%         save(save_path, 'sync_data', 'emg_data', 'stn_data', 'ctx_data', 'pulse_onset_times', 'sampling_rate');
        save(save_path, 'sync_data', 'ctx_data', 'pulse_onset_times', 'sampling_rate');
    end
    
    experiment_table = [experiment_table; stimulation_table];
    
end

experiment_table            = [experiment_table path_table];
% experiment_table.t_start    = [];

save([processed_dir 'experiment_data_' experiment_date], 'experiment_table')