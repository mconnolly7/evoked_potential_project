close all
project_dir     = '/Users/mconn24/OneDrive - Emory University/STN_EP_project/';
experiment_date = '2021_06_03';
experiment_dir  = [project_dir 'data/Fluffy/' experiment_date '/'];

sampling_rate           = 30000;
artifact_offset         = -.001;
segment_length          = 0.049;
segment_length_idx      = segment_length * sampling_rate;

t                       = (artifact_offset:1/30000:(artifact_offset + segment_length))*1000;

depth = [   39 39.1 39.2 39.3 39.3 39.3 39.4 39.5 39.6 39.7 39.8 39.9 40 40.1]; 
depth = [depth 40.1 40.0 39.9 39.8 39.7 39.6 39.5 39.4 39.3 39.2 39.1];
depth = [depth 39.0 38.9 38.8 38.7 38.6 38.5 38.4 38.3 38.2 38.1];
depth = [depth 38.0 37.9 37.8 37.7 37.6 37.4 37.2];
depth = [depth 37.0 36.8 36.6 36.4 36.2 36];

for c1 = 32:size(depth,2)
    
    file_base           = sprintf(['Fluffy_' experiment_date '_%03d'], c1);

    % Load stimulation_table
    stim_table_path     = [experiment_dir file_base '_stimulation_table.mat'];
    load(stim_table_path);

    % Load raw LFP data
    raw_data_path       = [experiment_dir file_base '.ns6'];
    raw_data_struct   	= openNSx(raw_data_path);
    sync_data           = double(raw_data_struct.Data(16,:));

    %
    data_1              = double(raw_data_struct.Data(3,:))'/4;
    data_2              = double(raw_data_struct.Data(1,:))'/4;

    data                = data_1 - data_2;
%     data                = data_2;

    n_trials            = size(stimulation_table,1);
    
    t_start             = stimulation_table.t_start;
    pulse_amplitude     = stimulation_table.amplitude_a;
    pulse_frequency     = stimulation_table.frequency;
    pulse_width         = stimulation_table.pulse_width_a;

    % Get TTL onset times
    pulse_onset_times   = detect_pulse_onset(sync_data, t_start, pulse_frequency, sampling_rate);

    % Organize data by pulses [pulse,
    for c2 = 1:size(pulse_onset_times,2)
        segment_start_idx       = floor((pulse_onset_times(c2) + artifact_offset) * sampling_rate) ;
        segment_end_idx         = segment_start_idx + segment_length_idx;

        pulse_segment(c2,:,:)   = data(segment_start_idx:segment_end_idx,:); 
    end

    pulse_segment = pulse_segment - mean(pulse_segment(:,(.001-artifact_offset)*sampling_rate:end,:),2);
    %         title(sprintf('Trial: %d, Polarity: %s, Depth: %d', c1, stimulation_table.phase{1}, depth(c1)))

    ep_mean(c1,:)     = mean(pulse_segment);

    ep_std      = std(pulse_segment);
    ep_se       = ep_std / sqrt(size(pulse_segment,1));
    ep_ci       = ep_se * 1.96;

    hold on
% 
    plot(t, ep_mean(c1,:), 'k-', 'LineWidth', 2)

    plot(t, ep_mean(c1,:) + ep_ci, 'r')
    plot(t, ep_mean(c1,:) - ep_ci, 'r')
    xlabel('Milliseconds')
    ylabel('µV')
    set(gca, 'FontSize', 14)

    title(sprintf('%.2f mm', depth(c1)))
    ylim([-10 10])
    xlim([-1 48])
    drawnow
%     print(file_name, '-dpng')
end
%     legend({'Inward', 'Outward'}, 'box', 'off')    



%%
% cla
depth_up_idx    = 15:48;
t_up_idx        = t>1 & t <= 15;
t_up            = t(t_up_idx);
depth_up        = depth(depth_up_idx);
ep_mean_up      = ep_mean(depth_up_idx,t_up_idx);

% plot_matrix(ep_mean_up', t_up, depth_up, 'n') 

x_1             = repmat(t_up, size(depth_up_idx,2),1);
x_2             = repmat(depth_up',1,size(t_up,2));

x_data          = [reshape(x_1,[],1) reshape(x_2,[],1)];
y_data          = reshape(ep_mean_up,[],1);
ep_gp           = gpr_model();

rand_idx        = randperm(size(x_data,1),5000);
ep_gp.initialize_data(x_data(rand_idx,:), y_data(rand_idx,1))
% ep_gp.minimize(1)

%%
h = ep_gp.plot_mean;
colormap(magma)

%
xlabel('Time (ms)')
ylabel('Depth (mm)')

xlim([1 48])
ylim([36 40.1])

set(gca,'FontSize', 15)

view(0,90)
set(gca, 'YDir','reverse')
%%
[x,~,~,ci] = ep_gp.predict([t_up', 38*ones(size(t_up'))])
%%
















