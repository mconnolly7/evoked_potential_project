clear; close all
project_dir     = '/Users/mconn24/OneDrive - Emory University/STN_EP_project/';
experiment_date = '2021_05_12';
experiment_dir  = [project_dir 'data/Fluffy/' experiment_date '/'];

sampling_rate   = 30000;


% depth = [0 39 39 39 39 40 40 41 41 42 42 43 43 43 43 42 42 41 41 40 40 39 39 30 30]; 
depth = [37 37 38 38 39 39 40 40 41 41 42 42 43 43]; 

recordings = [10 14 15 16 17 18 19 20 21 22 23 24 26 27];
% for c1 = 33
for c1 = 1:size(recordings, 2)
    
    % figure
    file_base           = sprintf(['Fluffy_' experiment_date '_%03d'], recordings(c1));

    % Load stimulation_table
    stim_table_path     = [experiment_dir file_base '_stimulation_table.mat'];
    load(stim_table_path);

    % Load raw LFP data
    raw_data_path       = [experiment_dir file_base '.ns6'];
    raw_data_struct   	= openNSx(raw_data_path);
    sync_data           = double(raw_data_struct.Data(16,:));

    data                = double(raw_data_struct.Data([13 14],:))';

    % Get TTL onset times
    pulse_onset_times   = detect_pulse_onset(sync_data, ...
        stimulation_table.t_start(1), 1, sampling_rate);

    electrode_depth     = depth(c1);
    
    save_path = sprintf('RRM16_%s_depth_%dmm_phase_%s', experiment_date, depth(c1), stimulation_table.phase{1});
    save(save_path, 'data', 'pulse_onset_times', 'sampling_rate', 'electrode_depth', 'stimulation_table');
end
