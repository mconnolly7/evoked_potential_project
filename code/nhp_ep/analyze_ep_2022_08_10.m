clear;  
close all; 
clc
% f = figure('Position', [-1137 495 918 420]);
project_dir = '/Users/mconn24/OneDrive/projects/STN_EP_project/';

experiment_name = 'bipolar_ring_grid_37mm';
% experiment_name = 'bipolar_ring_grid_38mm';
% experiment_name = 'bipolar_ring_grid_39mm';
% experiment_name = 'bipolar_ring_grid_40mm';
% experiment_name = 'bipolar_ring_grid_41mm';
experiment_name = 'bipolar_ring_grid_42mm';
% experiment_name = 'bipolar_ring_grid_43mm';
experiment_name = 'bipolar_ring_grid_44mm';
% experiment_name = 'bipolar_ring_grid_45mm';

% experiment_name = 'monopolar_segmented_grid_40mm';
% experiment_name = 'monopolar_segmented_grid_41mm';
% experiment_name = 'monopolar_segmented_grid_42mm';
% % experiment_name = 'monopolar_segmented_grid_43mm';
% experiment_name = 'monopolar_segmented_grid_44mm';

experiment_date = '2022_08_26';
experiment_dir = [project_dir 'data/Yerkes/Fluffy/' experiment_date '_' experiment_name '/'];
processed_dir = [experiment_dir 'segmented_data/'];
experiment_data_path = [processed_dir 'experiment_data_' experiment_date '.mat'];

sampling_rate = 24414.065;

segment_length_time = 0.01;
segment_length_idx = segment_length_time * sampling_rate;

artifact_offset_pre = -.001;
artifact_offset_post = .001;

t = (artifact_offset_pre:1/sampling_rate:(artifact_offset_pre + segment_length_time))*1000;

post_artifact_idx = floor(artifact_offset_post-artifact_offset_pre*sampling_rate):size(t,2);

load(experiment_data_path)

cathode = [0 1 2 3];
anode = [0 1 2 3];
amplitude = [100 200 300 ];

ax = axes('Position', [.05 .1 .8 .9]);  
hold on

filter_vec = combvec(cathode, anode, amplitude)';
n_plots = size(experiment_table,1);

viridis_map = flip(viridis(3));

for c1 = 1:n_plots

    stimulation_file = experiment_table.file_name{c1};

    cathode = experiment_table.cathode(c1);
    anode = experiment_table.anode(c1);
    amplitude = experiment_table.amplitude(c1);

    sub_plot_idx = cathode*4 + anode + 1;
    subplot(4,4,sub_plot_idx)

    load([processed_dir stimulation_file '.mat'])

    [pulse_onset_times, pulse_onset_idx] = process_detect_pulse_onset(sync_data, 0, 0, sampling_rate);
%     pulse_onset_times = detect_pulse_onset(sync_data, 0, 100, sampling_rate);

    data_neuro_sig = ctx_data(2,:) - ctx_data(4,:);
    stacked_pulses = process_stack_pulse_window(data_neuro_sig, pulse_onset_idx, ...
            sampling_rate, .01);

%     pulse_data = segment_ep_on_channel(ctx_data, pulse_onset_times, sampling_rate, segment_length_time);

    % Center each pulse by subtracting average
%     pulse_mean = mean(pulse_data(:,:,post_artifact_idx),3);
%     pulse_data = pulse_data - pulse_mean;
% 
%     % Scale by standard deviation
%     pulse_std = std(pulse_data(:,:,post_artifact_idx),[],3);
% %     pulse_data = pulse_data ./ pulse_std;
%     
%     % Re-reference pulses between channels
%     pulse_segment_channel = squeeze(pulse_data(1,:,:) - pulse_data(3,:,:));
%     hold on 

    
    ep_mean = mean(stacked_pulses);
%     ep_mean = movmean(ep_mean, 10); 

    ep_std = std(stacked_pulses);
    ep_se = ep_std / sqrt(size(stacked_pulses,1));
    ep_ci = ep_se * 1.96;

    hold on

%     plot(t(1:end-1), ep_mean + ep_ci, 'k')
%     plot(t(1:end-1), ep_mean - ep_ci, 'k')
    h(c1) = plot(t(1:end-1), ep_mean, '-', 'LineWidth', 3, 'color', viridis_map(amplitude/100,:));

    ylim([-5 5]*10^-5)
    title_string = sprintf('Cathode: %.1f, Anode: %.1f, Amplitude: %.1f', cathode, anode, amplitude);
    title(title_string)
    set(gca, 'FontSize', 14)
end
